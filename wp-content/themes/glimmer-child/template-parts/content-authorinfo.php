<?php
/**
 * The template for displaying author info 
 *
 * Used for both single and author page.
 *
 * @package Glimmer
 */
global $softhopper_glimmer;
?>
<!-- author-info -->
<div class="author-info clearfix">
    <div id="author-img">
        <figure class="at-img">
            <?php echo get_avatar( get_the_author_meta('email') , 110 ); ?>
        </figure>
        <h3 class="author-name"><?php the_author(); ?></h3> 
    </div> <!-- /#author-img -->
    
    <div id="author-details">
        <div class="authors-bio">
            <p><?php the_author_meta('description'); ?></p>
        </div>
        <footer id="author-meta" class="clearfix">
            <div class="post-count">
                <b><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
                <?php 
                    the_author_posts();
                    echo ( get_the_author_posts() > 1 ) ? esc_html__( ' Total Posts', 'glimmer' ) : esc_html__( ' Total Post', 'glimmer' );
                ?> </a></b>
            </div> <!-- /.post-count -->

            <div class="authors-social">
                <?php 
                    $social_links = get_the_author_meta('_glimmer_user_social_link'); 
                    if ( !empty ( $social_links ) ) : 
                        echo "<b>".esc_html__( 'Follow Me', 'glimmer' )."</b>"; 
                        foreach ($social_links as $key => $value) {
                            echo "<a href=\"{$value['social_url']}\"><i class=\"fa {$value['social_icon']}\"></i></a>";
                        } // end foreach
                    endif;
                ?>
            </div> 
        </footer>  <!-- /#author-meta --> 
    </div> <!-- /#author-details -->               
</div> <!-- /.author-info -->