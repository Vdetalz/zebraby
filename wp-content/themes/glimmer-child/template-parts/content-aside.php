<?php
/**
 * The template for displaying aside post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>
<div class="<?php echo glimmer_theme_check_list_grid(); ?>">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	    <div class="entry-content">
	        <?php 
                if ( has_excerpt() ) {
                    the_excerpt();
                } else {
                    the_content();
                } 
            ?>    
	        <?php mb_glimmer_entry_header(); ?>
	    </div> <!-- .entry-content -->
	</article> <!-- /.post-->
</div> <!-- /.col-md-12 -->