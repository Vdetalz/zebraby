<?php
/**
 * The template for displaying quote post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>
<?php             
    global $softhopper_glimmer; 
    $meta = get_post_meta( $post->ID );
    // check list or grid post layout
?>
<div class="<?php echo esc_attr( glimmer_theme_check_list_grid() ); ?>">
    <article id="post-<?php the_ID(); ?>" <?php post_class('format-quote'); ?>>
        <h2 class="entry-title"><?php echo esc_html__('Some hide content', 'glimmer'); ?></h2>
        <div class="quote-content">
            <div class="quote-icon">
                <?php
                    if ( is_single() ) {
                        ?>
                        <div><span><i class="fa fa-quote-left"></i></span></div>
                        <?php
                    } else {
                        ?>
                        <a href="<?php the_permalink(); ?>"><span><i class="fa fa-quote-left"></i></span></a>
                        <?php
                    }
                ?>
            </div>
            <blockquote>
                <span class="screen-reader-text"></span>
                <p><?php if( isset ( $meta["_glimmer_format_quote"][0] ) ) echo wp_kses_post( $meta["_glimmer_format_quote"][0] ); ?></p>

                <footer class="author">
                    <?php
                    if ( isset ( $meta["_glimmer_format_quote_author"][0] ) ) :
                        if( isset ( $meta["_glimmer_format_quote_url"][0] ) ) { ?>
                            <a href="<?php if( isset ( $meta["_glimmer_format_quote_url"][0] ) ) echo esc_url( $meta["_glimmer_format_quote_url"][0]); ?>"><?php if( isset ( $meta["_glimmer_format_quote_author"][0] ) ) echo esc_html($meta["_glimmer_format_quote_author"][0] ); ?></a>
                            <?php
                            } else { ?>                        
                            <?php if( isset ( $meta["_glimmer_format_quote_author"][0] ) ) echo ( $meta["_glimmer_format_quote_author"][0] ); ?>
                            <?php
                            } 
                    endif;
                    ?>
                </footer> <!-- /.author -->
            </blockquote> <!-- /.quote-content -->
        </div> <!-- /.quote-content -->
    </article> <!-- /.post-->
</div> <!-- /.col-md-12 -->