<?php
/**
 * The template for displaying status post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
    global $softhopper_glimmer;
    // check list or grid post layout
    if ( !is_single() ) {
        if( is_archive() || is_search() ) {
            $post_layout = ( $softhopper_glimmer['post_layout_archive'] == 'list' ) ? 'col-md-12 full-width' : 'col-md-6 col-sm-6 grid';
        } else {
            $post_layout = ( $softhopper_glimmer['post_layout'] == 'list' ) ? 'col-md-12 full-width' : 'col-md-6 col-sm-6 grid';
        }
    } else {
        $post_layout = 'col-md-12 full-width';
    }

    if ( is_sticky() ) {
        if ( is_archive() || is_search() ) {
            $post_layout = ( $softhopper_glimmer['post_layout_archive'] == 'list' ) ? 'col-md-12 full-width' : 'col-md-6 col-sm-6 grid';
        } else {
            $post_layout = 'col-md-12 full-width';
        }
    }
?>
<div class="<?php echo esc_attr( $post_layout ); ?>">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php
            ob_start(); 
        ?>
        <div class="post-media">				
			<?php
		    $meta = get_post_meta( $post->ID );
			
			$status_facebook = ( isset ( $meta["_glimmer_format_status_fb"][0] ) ) ? $meta["_glimmer_format_status_fb"][0] : "";
			$status_twitter = ( isset ( $meta["_glimmer_format_status_twitter"][0] ) ) ? $meta["_glimmer_format_status_twitter"][0] : "";
			$status_gplus = ( isset ( $meta["_glimmer_format_status_gplus"][0] ) ) ? $meta["_glimmer_format_status_gplus"][0] : "";
			$status_instagram = ( isset ( $meta["_glimmer_format_status_instagram"][0] ) ) ? $meta["_glimmer_format_status_instagram"][0] : "";

			if( !empty( $status_facebook ) || !empty( $status_twitter ) || !empty( $status_gplus ) || !empty( $status_instagram )):	

                    $status_bg = "";
                    $image_src = "";
                    glimmer_cropping_image_size();
                    global $image_size;

                    if ( is_single() ) {
                        $meta = get_post_meta( $post->ID );
                        if ( isset($meta["_glimmer_custom_layout"][0]) ) {
                            ( $meta["_glimmer_layout"][0] == 'full-content' ) ? $image_size = "glimmer-single-full" : $image_size = "glimmer-single-list" ;
                        } else {
                            ( $softhopper_glimmer['sidebar_layout_single'] == 'full-width' ) ? $image_size = "glimmer-single-full" : $image_size = "glimmer-single-list" ;
                        }
                        if ( isset($meta["_glimmer_format_status_bg"][0]) ) {
                            $img_tag = wp_get_attachment_image_src( get_post_meta( get_the_ID(), '_glimmer_format_status_bg_id', 1 ), $image_size );
                            $image_src = $img_tag[0];
                        }
                    } else {
                        if ( isset($meta["_glimmer_format_status_bg"][0]) ) {
                            $img_tag = wp_get_attachment_image_src( get_post_meta( get_the_ID(), '_glimmer_format_status_bg_id', 1 ), $image_size );
                            $image_src = $img_tag[0];
                        } 
                    }

                    if ( $image_src ) {
                        $status_bg = $image_src;
                    }
                ?>
				<div class="post-status-wrapper" style="background: url(<?php echo esc_url( $status_bg ); ?>);">
				<?php if( !empty( $status_facebook ) ) : ?>
					<div id="fb-root"></div>
					<script>
						(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;  js = d.createElement(s);
						js.id = id;  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
						fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
					</script>
					<div class="fb-post" data-href="<?php echo esc_attr( $status_facebook ) ?>"></div>
				<?php elseif( !empty( $status_twitter ) ) : ?>
					<blockquote class="twitter-tweet"><a href="<?php echo esc_url($status_twitter); ?>"></a></blockquote>
					<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
				<?php elseif( !empty( $status_gplus ) ) : ?>
					<script type="text/javascript" src="//apis.google.com/js/plusone.js"></script>
					<div class="g-post" data-href="<?php echo esc_attr( $status_gplus ); ?>"></div>
				<?php elseif( !empty( $status_instagram ) ) : ?>
					<?php echo ( $status_instagram ); ?>
				<?php endif; ?>
				</div><!-- /.post-status-wrapper -->
			<?php endif; ?>
		</div> <!-- /.post-media -->
        <?php   
        $status_post = ob_get_clean();

        // if list layout show status or if grid layout show only image (but single post show full status)
        if (is_single()) {
            echo ( $status_post );
        } else {
            if (is_archive() || is_search()) {
                if ($softhopper_glimmer['post_layout_archive'] == 'list') {
                    echo ( $status_post );
                } else {
                    echo '<figure class="post-thumb">';
                    glimmer_cropping_image_size();
                    global $image_size;
                    if ( isset($meta["_glimmer_format_status_bg_id"][0]) ) {
                        echo wp_get_attachment_image( get_post_meta( get_the_ID(), '_glimmer_format_status_bg_id', 1 ), $image_size );
                    }
                    echo '</figure>';
                }
            } else {
                if ($softhopper_glimmer['post_layout'] == 'list') {
                    echo ( $status_post );
                } else {
                    echo '<figure class="post-thumb">';
                    glimmer_cropping_image_size();
                    global $image_size;
                    if ( isset($meta["_glimmer_format_status_bg_id"][0]) ) {
                        echo '<a href="'.get_the_permalink().'">';
                        echo wp_get_attachment_image( get_post_meta( get_the_ID(), '_glimmer_format_status_bg_id', 1 ), $image_size );
                        echo '</a>';
                    }
                    echo '</figure>';
                }
            }
        }
        ?>
                                    
        
        <header class="entry-header">            
            <?php mb_glimmer_entry_header(); ?>
        </header> <!-- /.entry-header -->

        <div class="entry-content">
        <?php 
            ob_start(); 
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
                    'after'  => '</div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                ) );
            $post_pagination = ob_get_clean();

            if ( is_single() ) {
                the_content(); 
                echo ( $post_pagination );
                edit_post_link( esc_html__( '(Edit Post)', 'glimmer' ), '<span class="edit-link">', '</span>' );
                glimmer_tag_list();
            } else {
                if ( has_excerpt() ) {
                    the_excerpt();
                } else {
                    the_content();
                }
                echo ( $post_pagination );
            }
        ?>  
        </div> <!-- .entry-content -->

        <footer class="entry-footer clearfix">                          
            <?php mb_glimmer_entry_footer(); ?>
        </footer> <!-- .entry-footer -->
    </article> <!-- /.post-->
</div> <!-- /.col-md-12 -->