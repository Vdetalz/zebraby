<?php
/**
 * Enqueue style of child theme
 */
function glimmer_child_theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', [ 'parent-style' ] );
}

add_action( 'wp_enqueue_scripts', 'glimmer_child_theme_enqueue_styles', 100000 );

if ( ! function_exists( 'mb_glimmer_entry_header' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function mb_glimmer_entry_header() {
		global $post;
		$meta = get_post_meta( $post->ID );
		glimmer_post_format_icon();
		if ( is_single() ) {
			if ( isset( $meta["_glimmer_post_title"][0] ) ) {
				$post_title = ( $meta["_glimmer_post_title"][0] == 'show' ) ? TRUE : FALSE;
			} elseif ( softhopper_glimmer( 'post_title' ) !== NULL ) {
				$post_title = ( softhopper_glimmer( 'post_title' ) == TRUE ) ? TRUE : FALSE;
			} else {
				$post_title = TRUE;
			}
			if ( $post_title == TRUE ) {
				the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' );
			}
		} else {
			the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
		}
		ob_start();
		?>
        <div class="entry-meta">
			<?php if ( is_archive() || is_search() ) {
				$header_layout_list = ( softhopper_glimmer( 'post_layout_archive' ) == 'list' ) ? TRUE : FALSE;
			} else {
				$header_layout_list = ( softhopper_glimmer( 'post_layout' ) == "list" ) ? TRUE : FALSE;
			} ?>

			<?php if ( $header_layout_list == TRUE ) { ?>
				<?php
				if ( 'post' == get_post_type() ) { ?>
                    <span class="cat-links">
                <?php esc_html_e( 'Опубликовано в : ', 'glimmer' ) . the_category( ', ' ); ?>
            </span>
                    <span class="devider">/</span>
				<?php } ?>
                <div class="entry-date">
					<?php the_time( get_option( 'date_format' ) ); ?>
                </div>
			<?php } else { ?>
				<?php
				if ( 'post' == get_post_type() ) { ?>
                    <span class="cat-links">
                    <?php esc_html_e( 'In: ', 'glimmer' ) . the_category( ', ' ); ?>
                </span>
				<?php } ?>
			<?php } ?>
        </div> <!-- .entry-meta -->

		<?php
		$entry_meta = ob_get_clean();
		if ( is_single() ) {
			if ( isset( $meta["_glimmer_post_cat_date_author_meta"][0] ) ) {
				$post_cat_date_author_meta = ( $meta["_glimmer_post_cat_date_author_meta"][0] == 'show' ) ? TRUE : FALSE;
			} else {
				$post_cat_date_author_meta = ( softhopper_glimmer( 'post_cat_date_author_meta' ) == TRUE ) ? TRUE : FALSE;
			}
			if ( $post_cat_date_author_meta == TRUE ) {
				echo wp_kses_post( $entry_meta );
			}
		} else {
			echo wp_kses_post( $entry_meta );
		}
	}
endif;

if ( ! function_exists( 'mb_glimmer_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function mb_glimmer_entry_footer() {
		global $post;
		$meta = get_post_meta( $post->ID ); ?>

		<?php if( is_archive() || is_search() ) {
			$footer_layout_list = ( softhopper_glimmer('post_layout_archive') == 'list' ) ? true : false;
		} else {
			$footer_layout_list = ( softhopper_glimmer('post_layout') == "list" || is_sticky() ) ? true : false;
		} ?>

		<?php if( $footer_layout_list == true ) { ?>
            <div class="footer-meta clearfix">
                <div class="post-comment">
                    <a href="<?php comments_link(); ?>" class="comments-link">
                        <span><?php comments_number( esc_html('0 Комментариев', 'glimmer-child'), esc_html('1 Комментарий', 'glimmer'), '% '.esc_html('Комментариев', 'glimmer') ); ?></span>
                    </a>
                </div>

				<?php if( function_exists('glimmer_social_share_link') ) { ?>
					<?php glimmer_social_share_link(); ?>
				<?php } ?>

                <div class="post-view">
                    <a href="<?php the_permalink(); ?>" class="view-link">
                        <span><?php echo softhopper_get_post_views( get_the_ID() ); ?></span>
                    </a>
                </div>
            </div>
		<?php } else { ?>
            <div class="more-wraper">
                <a href="<?php the_permalink(); ?>" class="more-link"><?php esc_html_e('Continue', 'glimmer'); ?></a>
            </div>
			<?php if( function_exists('glimmer_social_share_link') ) { ?>
                <div class="footer-meta">
					<?php glimmer_social_share_link(); ?>
                </div> <!-- /.footer-meta -->
			<?php }
		} ?>
		<?php
	}
endif;

if ( ! function_exists( 'mb_glimmer_entry_footer_single' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function mb_glimmer_entry_footer_single() {
		if(  function_exists('glimmer_social_share_link') ) {
			// Hide category and tag text for pages.
			if ( 'post' == get_post_type() ) { ?>
                <div class="footer-meta clearfix">
                    <div class="post-comment">
                        <a href="<?php comments_link(); ?>" class="comments-link">
                            <span><?php comments_number( esc_html('0 Комментариев', 'glimmer'), esc_html('1 Комментарий', 'glimmer'), '% '.esc_html('Комментариев', 'glimmer') ); ?></span>
                        </a>
                    </div>

					<?php if( function_exists('glimmer_social_share_link') ) { ?>
						<?php glimmer_social_share_link(); ?>
					<?php } ?>

                    <div class="post-view">
                        <a href="<?php the_permalink(); ?>" class="view-link">
                            <span><?php echo softhopper_get_post_views( get_the_ID() ); ?></span>
                        </a>
                    </div>
                </div>
				<?php
			} // end if post type
		}
	}
endif;

/**
 * Remove parent overrides.
 */
function mb_remove_parent_ovverrides() {
	remove_filter('comment_form_defaults', 'glimmer_comment_form');
	add_filter('comment_form_defaults', 'mb_glimmer_comment_form');
}
add_action('init', 'mb_remove_parent_ovverrides');

/**
 * Rewrite comment form.
 *
 * @param $args
 *
 * @return mixed
 */
function mb_glimmer_comment_form($args) {
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );

	$args['fields'] = array(
		'author' =>
			'<div class="col-md-6 pd-right"><p><input id="name" class="form-controller" name="author" required="required" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
			'" size="30"' . ( $req ? " aria-required='true'" : '' ) . ' placeholder="' . __( 'Your Name', 'glimmer' ) . ( $req ? '*' : '' ) . '" /></p></div>',

		'email' =>
			'<div class="col-md-6 pd-left"><p><input id="email" class="form-controller" name="email" required="required" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
			'" size="30"' . ( $req ? " aria-required='true'" : '' ) . ' placeholder="' . __( 'Your Email', 'glimmer' ) . ( $req ? '*' : '' ) . '" /></p></div>',
	);
	$args['id_form'] = "contact_form";
	//$args['class_form'] = "commentform";
	$args['id_submit'] = "submit";
	$args['class_submit'] = "submit";
	$args['name_submit'] = "submit";
	$args['title_reply'] = __( '<span>Leave a Reply</span>', 'glimmer' );

	$args['title_reply_to'] = __( 'Leave a Reply to %s', 'glimmer' );
	$args['cancel_reply_link'] = __( 'Cancel Reply', 'glimmer' );
	$args['comment_notes_before'] = "";
	$args['comment_notes_after'] = "";
	$args['label_submit'] = __( 'Submit', 'glimmer' );
	$args['comment_field'] = '<div class="col-md-12"><p><textarea id="message" class="form-controller" name="comment" aria-required="true" rows="8" cols="45" placeholder="'. __( 'Your Comment here...', 'glimmer' ) .'" ></textarea></p></div>';
	return $args;
}
