<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Glimmer
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js" >
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php endif; ?>
    
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

    <?php        
        // Site Preloader
        $preloader = "";
        ( isset($_GET["site_preloader"]) ) ? $site_preloader = $_GET["site_preloader"]  : $site_preloader = "" ;
        ( $site_preloader == "on" ) ? $preloader = true : $preloader = softhopper_glimmer('preloader');
        if ( $preloader ) { ?>
        <!-- Preloader -->
        <div class="preloader">
            <div class="preloader-logo">
                <img src="<?php if ( isset( softhopper_glimmer('preloader_logo')['url'] ) ) echo esc_url( softhopper_glimmer('preloader_logo')['url'] ); ?>" class="img-responsive" alt="<?php echo esc_attr('preloader', 'glimmer') ?>">
            </div> <!-- /.preloader-logo -->
            <div class="loader">
                <?php 
                    $animated_icon = "";
                    if ( softhopper_glimmer('preloader_animated_icon') == 1 ) {
                        $animated_icon = "fa-spinner fa-pulse";
                    } elseif ( softhopper_glimmer('preloader_animated_icon') == 2 ) {
                        $animated_icon = "fa-spinner fa-spin";
                    } elseif ( softhopper_glimmer('preloader_animated_icon') == 3 ) {
                        $animated_icon = "fa-circle-o-notch fa-spin";
                    } elseif ( softhopper_glimmer('preloader_animated_icon') == 4 ) {
                        $animated_icon = "fa-refresh fa-spin";
                    } elseif ( softhopper_glimmer('preloader_animated_icon') == 5 ) {
                        $animated_icon = "fa-cog fa-spin";
                    } 
                ?>
                <i class="fa <?php if ( isset( $animated_icon ) ) echo esc_attr( $animated_icon ); ?>"></i>
            </div> <!-- /.loader -->
        </div> <!-- /.preloader -->
        <?php }
    ?>
    
    <!-- Header
    ================================================== -->
    <header id="masthead" class="site-header">
       <!-- Header Top -->
       <?php 
            $header_menu_animation = ( softhopper_glimmer('header_menu_animation') ) ? softhopper_glimmer('header_menu_animation') : "slide";

            if (softhopper_glimmer('header_top_section')) :
        ?>
        <div id="header-top">
            <div class="container">
                <div class="row">
                    <div class="header-search">
                        <div class="search default">
                            <form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" class="searchform">
                                <div class="input-group">
                                    <input type="search" name="s" value="<?php esc_html_e( 'Search here &hellip;', 'glimmer' ); ?>" class="form-control" id="dsearch">
                                    <span class="input-group-btn">
                                        <button id="submit-btn" type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>
                        </div> <!-- /.search -->
                    </div> <!-- /.header-search -->

                    <div class="header-social">
                        <?php
                            // show header top right social link
                            if ( softhopper_glimmer('header_top_right_social_link') && !empty( softhopper_glimmer('header_top_right_social_link')) ) {
                                $social_link = softhopper_glimmer('header_top_right_social_link');                      
                                foreach ( $social_link as $key => $value ) {
                                if ( $value['title'] ) { ?>
                                <a target="_blank" href="<?php echo esc_url( $value['url'] ); ?>"><i class="fa <?php echo esc_attr( $value['title'] );?>"></i></a>
                                <?php }
                                }
                            }
                        ?>  
                    </div> <!-- /.top-social --> 
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /#header-top -->
        <?php endif; ?>
        
        <!-- Site Logo -->
        <div id="header-middle">
            <div class="container">
                <div class="row">
                    <?php if (softhopper_glimmer('header_menu_top_left_right')) : ?>
                    <div id="nav-left" class="site-navigation">
                        <div class="menu-wrapper">
                            <div class="menu-content">
                                <nav class="navigation main-menu-left animation-<?php echo esc_attr($header_menu_animation); ?>">
                                    <div class="navigation-inner">
                                        <div class="inner">
                                        <?php 
                                            wp_nav_menu ( array(
                                                'menu_class' => 'main-menu main-menu-left',
                                                'container'=> 'ul',
                                                'theme_location' => 'header-menu-left',
                                                'fallback_cb'       => 'Sh_Custom_Walker::fallback_main_menu',
                                                'walker' => new Sh_Custom_Walker()  
                                            )); 
                                        ?>
                                        </div>
                                    </div>
                                </nav> <!--Navigation-->  
                            </div>
                        </div> <!-- /.menu-wrapper --> 
                    </div> <!-- /#nav-left-->
                    <?php endif; ?>

                    <?php if( softhopper_glimmer('header_main_menu') !== '' && softhopper_glimmer('header_menu_top_left_right') !== 1 && softhopper_glimmer('header_menu_top_left_right') !== 1 ) {
                        $no_left_menu = 'no-left-right-menu';
                    } else {
                        $no_left_menu = '';
                    } ?>

                    <div id="site-logo" class="<?php echo esc_attr( $no_left_menu ); ?>">
                        <?php if ( softhopper_glimmer('header_logo', 'url') !== NULL ) { ?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-logo" rel="home"><img src="<?php echo esc_url( softhopper_glimmer('header_logo','url') ); ?>" alt="<?php echo bloginfo('name'); ?>"></a>
                        <?php } else { ?>                            
                            <div class="site-branding">
                                <div class="site-branding-text">
                                    <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                                    <?php $description = get_bloginfo( 'description', 'display' );
                                    if ( $description || is_customize_preview() ) : ?>
                                    <p class="site-description"><?php echo esc_html($description); ?></p>
                                    <?php endif; ?>
                                </div><!-- .site-branding-text -->
                            </div><!--  /.site-branding -->
                        <?php } ?>
                    </div> <!-- /#site-logo -->

                    <?php if (softhopper_glimmer('header_menu_top_left_right')) : ?>
                    <!--Navigation Right-->
                    <div id="nav-right" class="site-navigation menu-right">
                        <div class="menu-wrapper">
                            <div class="menu-content">
                                <nav class="navigation main-menu-right animation-<?php echo esc_attr($header_menu_animation); ?>">
                                    <div class="navigation-inner">
                                        <div class="inner">
                                        <?php 
                                            wp_nav_menu ( array(
                                                'menu_class' => 'main-menu main-menu-right',
                                                'container'=> 'ul',
                                                'theme_location' => 'header-menu-right',
                                                'fallback_cb'       => 'Sh_Custom_Walker::fallback_main_menu',
                                                'walker' => new Sh_Custom_Walker()  
                                            )); 
                                        ?>
                                        </div>
                                    </div>
                                </nav> <!--Navigation--> 
                            </div>
                        </div> <!-- /.menu-wrapper -->     
                    </div> <!-- /#nav-right -->    

                    <div class="menucontent overlapblackbg"></div>
                    <div class="menuexpandermain slideRight">
                        <a id="navToggle" class="animated-arrow slideLeft"><span></span></a>
                        <span class="menu-marker"></span>
                    </div>
                    <div id="mobile-menu">
                        <nav class="top-menu slideLeft clearfix">
                            <div class="menu-wrapper">
                                <div class="container">
                                    <div class="row" id="mobile-menu-wrap"></div>
                                </div>               
                            </div> 
                        </nav> 
                    </div> 
                    <?php endif; ?>
                </div> <!-- /.row -->
            </div> <!-- /.container --> 
        </div> <!-- /#header-middle -->
        
        <?php if (softhopper_glimmer('header_main_menu') !== '') : ?>
        <div class="main-menu-area">        
            <div class="container">   
                <div id="main-menu" class="site-navigation">
                    <div class="menu-wrapper">
                        <div class="menu-content">
                            <nav class="navigation main-menu-section animation-<?php echo esc_attr($header_menu_animation); ?>">
                                <div class="navigation-inner">
                                    <div class="inner">
                                    <?php 
                                        wp_nav_menu ( array(
                                            'menu_class' => 'main-menu',
                                            'container'=> 'ul',
                                            'theme_location' => 'main-menu',
                                            'fallback_cb'       => 'Sh_Custom_Walker::fallback_main_menu',
                                            'walker' => new Sh_Custom_Walker()  
                                        )); 
                                    ?>
                                    </div>
                                </div>
                            </nav> <!--Navigation-->  
                        </div>
                    </div> <!-- /.menu-wrapper --> 
                </div> <!-- /#nav-left-->

                <!-- Mobile Main Menu -->
                <div class="menucontent overlapblackbg"></div>
                <div class="menuexpandermain slideRight">
                    <a id="navToggleMain" class="animated-arrow slideLeft"><span></span></a>
                    <span class="menu-marker"></span>
                </div>
                <div id="mobile-min-menu">
                    <nav class="top-menu slideLeft clearfix">
                        <div class="menu-wrapper">
                            <div class="container">
                                <div class="row" id="mobile-menu-main-wrap"></div>
                            </div>               
                        </div> 
                    </nav> 
                </div> 
            </div><!-- /.container -->
        </div>
        <?php endif; ?>
    </header> <!-- /.site-header -->
