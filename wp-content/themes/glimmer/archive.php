<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Glimmer
 */
get_header();           
    get_template_part( 'blog-layout' );
get_footer(); 