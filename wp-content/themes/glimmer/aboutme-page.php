<?php
/**
 * Template Name: About Me
 */
?>
<?php get_header(); ?>
<!-- Content
================================================== -->
<div id="content" class="site-content about-me">
    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="header-title">
                    <h2 class="section-title"><span><?php the_title(); ?></span></h2>
                </div> <!-- /.header-title --> 
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <figure class="author-image">
                    <img class="img-responsive" src="<?php if ( softhopper_glimmer('author_img','url') !== '' ) echo esc_url( softhopper_glimmer('author_img','url') ); ?>" alt="<?php if (  softhopper_glimmer('author_name') !== '' ) echo esc_attr( softhopper_glimmer('author_name') ); ?>" >
                </figure> <!-- /.author-image -->
                <div class="clear"></div>
                <div class="about-details">
                    <div class="author-details">

                        <h2 class="author-name"> 
                        <?php 
                            if ( softhopper_glimmer('author_name') !== '' ) echo esc_html(softhopper_glimmer('author_name'));
                        ?>
                        </h2> <!-- /.author-name -->

                        <div class="entry-content">
                            <?php 
                                if ( softhopper_glimmer('author_description') !== '' ) echo wp_kses_post(softhopper_glimmer('author_description'));
                            ?>
                        </div> <!-- /.entry-content -->
                        <div class="clear"></div>
                        <footer class="entry-footer">
                           <div class="follow-link">
                                <span><?php esc_html_e('Follow me :', 'glimmer' ); ?></span>
                                <?php
                                    // show social link 
                                    if( softhopper_glimmer('author_social_link') !== '' ) {
                                        $social_link = softhopper_glimmer('author_social_link');                      
                                        foreach ( $social_link as $key => $value ) {
                                        if ( $value['title'] ) { ?>
                                        <a href="<?php echo esc_url($value['url']); ?>"><i class="fa <?php echo esc_attr($value['title']);?>"></i></a>
                                        <?php }
                                        }
                                    }
                                ?>       
                           </div> <!-- /.follow-link -->
                           <div class="author-sign">
                                <img src="<?php if ( softhopper_glimmer('author_sign','url') !== '' ) echo esc_url(softhopper_glimmer('author_sign','url')); ?>" alt="<?php esc_attr_e('Sign', 'glimmer') ?>" >
                                <h3><?php if ( softhopper_glimmer('author_name') !== '' ) echo esc_html( softhopper_glimmer('author_name') ); ?></h3>
                           </div>
                        </footer> <!-- /.entry-footer --> 
                    </div> <!-- /.author-details -->
                </div> <!-- /.contact-details -->
            </div>
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- #content -->
<?php get_footer(); ?>