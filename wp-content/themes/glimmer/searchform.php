<?php
/**
 * The template for displaying search form.
 *
 * @package Glimmer
 */
?>
<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="search-form" method="get">
    <div class="input-group">
        <input type="search" name="s" value="<?php esc_html_e( 'Search here &hellip;', 'glimmer' ); ?>" class="form-controller">
        <?php if ( softhopper_glimmer('search_only_form_post') == 1 ) : ?>
            <input type="hidden" value="post" name="post_type" id="post_type">
        <?php endif; ?>
        <span class="input-group-btn">
            <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
        </span>
    </div>
</form>