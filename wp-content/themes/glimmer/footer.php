<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Glimmer
 */
?>
    <!-- Footer
    ================================================== --> 
    <footer id="colophon" class="site-footer">
        <?php if ( class_exists('Glimmer_Instagram_Section') && softhopper_glimmer('footer_widgets_top') == 1 ) : ?>
        <div id="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?php new Glimmer_Instagram_Section; ?>
                    </div><!--  /.col-md-12 -->              
                </div> <!-- /.row -->
            </div>       
        </div> <!-- #footer-top" -->
        <?php endif; ?>
        
        <?php if ( softhopper_glimmer('footer_widgets_bottom') == 1 ) : ?>
        <div id="footer-middle">
            <div class="container">
                <div class="row">
                    <?php
                    if ( softhopper_glimmer('footer_widgets_bottom_columns') != 1 ) {
                        // show footer widget with condition
                        $columns = intval( softhopper_glimmer('footer_widgets_bottom_columns') );
                        $col_class = 12 / max( 1, $columns );
                        $col_class_sm = 12 / max( 1, $columns );
                        if ( $columns == 4 ) {
                            $col_class_sm = 6;
                        } 
                        $col_class = "col-sm-$col_class_sm col-md-$col_class";
                           for ( $i = 1; $i <= $columns ; $i++ ) {
                            if ( $columns == 3 ) :
                                if ( $i == 3 ) {
                                    $col_class = "col-sm-12 col-md-$col_class";
                                } else {
                                    $col_class = "col-sm-6 col-md-$col_class";
                                } 
                            endif; 
                           ?>
                            <div class="widget-area footer-bottom-sidebar-<?php echo esc_attr($i) ?> <?php echo esc_attr( $col_class ) ?>">
                                <?php dynamic_sidebar( esc_html__( 'Footer Bottom ', 'glimmer' ) . $i ) ?>
                            </div>
                        <?php }
                    } else { ?>
                        <div class="widget-area footer-bottom-sidebar col-sm-12 col-md-12">
                          <?php dynamic_sidebar('footer-bottom-sidebar') ?>
                        </div>
                    <?php } ?> 
                </div> <!-- /.row -->
            </div> <!-- /.container -->         
        </div> <!-- #footer-middle -->
        <?php endif; ?>

        <div id="footer-bottom">
            <div class="container">
                <div class="copyright">
                    <?php if( softhopper_glimmer('footer_copyright_info') !== null  ) {
                        echo wp_kses_post( softhopper_glimmer('footer_copyright_info') );
                    } else { ?>
                        <p><?php echo wp_kses_post( 'Copyright&copy; 2015 Softhopper. All right reserved.', 'glimmer'); ?></p>
                    <?php } ?>
                </div> <!-- /.copyright -->
            </div> <!-- /.container -->
        </div> <!-- /#footer-bottom -->
    </footer><!-- #colophon -->
    <?php wp_footer(); ?>
    </body>
</html>