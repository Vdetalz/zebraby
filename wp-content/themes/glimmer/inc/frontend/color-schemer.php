<?php
function glimmer_hex_2_rgba($color, $opacity = false) {
    $default = 'rgb(0,0,0)';
    //Return default if no color provided
    if(empty($color))
        return $default; 
 
    //Sanitize $color if "#" is provided 
    if ($color[0] == '#' ) {
        $color = substr( $color, 1 );
    }

    //Check if color has 6 or 3 characters and get values
    if (strlen($color) == 6) {
        $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
    } elseif ( strlen( $color ) == 3 ) {
        $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
    } else {
        return $default;
    }

    //Convert hexadec to rgb
    $rgb =  array_map('hexdec', $hex);

    //Check if opacity is set(rgba or rgb)
    if($opacity){
        if(abs($opacity) > 1)
            $opacity = 1.0;
        $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
    } else {
        $output = 'rgb('.implode(",",$rgb).')';
    }

    //Return rgb(a) color string
    return $output;
}

function glimmer_color_scheme() { 
    global $softhopper_glimmer;
    switch( softhopper_glimmer('glimmer_color_scheme') ) {
        case 1: //C69F73
            // add a condition to show demo color scheme by url
            ( isset($_GET["color_scheme_color"]) ) ? $color_scheme_color = sanitize_text_field( wp_unslash( $_GET["color_scheme_color"] ) )  : $color_scheme_color = "" ;
            if (preg_match('/^[A-Z0-9]{6}$/i', $color_scheme_color)) {
                $demo_color_scheme = sanitize_text_field( wp_unslash( $_GET["color_scheme_color"] ) );
            }
            else {
               $demo_color_scheme = "9272ce";
            }
            $softhopper_glimmer_color_scheme = "#".$demo_color_scheme;
            
            break;
        case 2: //1ABC9C
            $softhopper_glimmer_color_scheme = "#1ABC9C";
            break;
        case 3: //D2527F
            $softhopper_glimmer_color_scheme = "#D2527F";
            break;
        case 4: //F26D7E
            $softhopper_glimmer_color_scheme = "#F26D7E";
            break;
        case 5: //CC6054
            $softhopper_glimmer_color_scheme = "#CC6054";
            break;
        case 6: //667A61
            $softhopper_glimmer_color_scheme = "#667A61";
            break;
        case 7: //A74C5B
            $softhopper_glimmer_color_scheme = "#A74C5B";
            break;
        case 8: //95A5A6
            $softhopper_glimmer_color_scheme = "#95A5A6";
            break;
        case 9: //turquoise
            $softhopper_glimmer_color_scheme = $softhopper_glimmer['glimmer_custom_color'];
            break;
        default:
            $softhopper_glimmer_color_scheme = "#9272ce";
            break;
    }
    //rgba color
    $softhopper_glimmer_rgba = glimmer_hex_2_rgba($softhopper_glimmer_color_scheme, 0.8);    
?>
<style type="text/css">
::-moz-selection{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}::selection{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}a:hover,a:focus,a:active{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>} blockquote:before{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}blockquote cite,blockquote a,blockquote span{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.more-link.main-more-btn, .more-wraper .more-link {background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.more-link.main-more-btn:hover,.more-link.main-more-btn:focus,.more-link.main-more-btn:active:focus{background:<?php echo esc_attr($softhopper_glimmer_rgba);?>}.small-border{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.ex-small-border::after{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.comment-reply-link{border-color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.comment-reply-link:hover{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}button,input[type="button"],input[type="reset"],input[type="submit"]{border-color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}button:hover,input[type="button"]:hover,input[type="reset"]:hover,input[type="submit"]:hover{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.btn-search{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.btn-search:focus,.btn-search:active:focus{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.continue-read{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.continue-read:hover{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;border-color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.go-button:hover{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}#featured-slider .item .post-content .more-link-wrap .more-link{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;}#featured-slider .owl-controls .owl-nav div:hover i{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.main-menu-area {background: <?php echo glimmer_hex_2_rgba($softhopper_glimmer_color_scheme, 0.05);?>; border-top: 1px solid <?php echo glimmer_hex_2_rgba($softhopper_glimmer_color_scheme, 0.09);?>;}.menu-list > li a:hover{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.menu-submenu > li > a:hover,.menu-submenu .menu-submenu > li > a:hover,.menu-submenu .menu-submenu .menu-submenu > li > a:hover{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>!important}.menu-list > li.current-menu-item > a,.menu-list > li.current-menu-ancestor > a{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>!important}.menu-submenu .current-menu-item > a,.current-menu-ancestor .current-menu-ancestor > a{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>!important}@media (max-width: 992px){.menu-list > ul > li:hover > a,.menu-list > ul > li.active > a{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>!important}}.post-format{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.content-area .entry-header .entry-meta a:hover{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.content-area .entry-content .edit-link:before{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.content-area .entry-footer .button:hover{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.content-area .entry-footer .comments-link:hover,.content-area .entry-footer .view-link:hover{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.content-area .entry-footer .share-area a span:hover{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;border-color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.content-area .entry-footer .tags-links span:before{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.content-area .format-link .post-link .icon-area i{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.content-area .format-quote .quote-icon a span::before,.content-area .format-quote .quote-icon a span::after,.content-area .format-quote .quote-icon > div span::before,.content-area .format-quote .quote-icon >div span::after{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.content-area .grid .entry-footer .share-area a:hover span{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>!important}.single-post .entry-content .tag a:hover{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;border-color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.single-post .entry-footer .post-format{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.single-post .entry-footer .post-author a:hover{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.paging-navigation .nav-links > li.active a,.paging-navigation .nav-links > li.active span{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;border-color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.paging-navigation .nav-links > li a:hover{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;border-color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.paging-navigation .nav-links li:first-child.nav-previous a:hover,.paging-navigation .nav-links li:last-child.nav-next a:hover{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.paging-navigation > li.active a,.paging-navigation > li.active span{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;border-color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.paging-navigation .nav-previous > a::after{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.paging-navigation .nav-next > a::after{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.entry-content .page-links > span {border-color: <?php echo esc_attr( $softhopper_glimmer_color_scheme );?>; background: <?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;}.gallery-one .owl-controls .owl-nav div,.gallery-two .full-view .owl-controls .owl-nav div{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.gallery-one .owl-controls .owl-buttons div span,.gallery-two .owl-controls .owl-buttons div span{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.gallery-two .list-view .owl-controls .owl-buttons div{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.author-info .authors-social a:hover{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.related-post .related-post-title span::before,.related-post .related-post-title span::after,.comments-area .comments-title span::before,.comments-area .comments-title span::after,.comment-reply-title span::before,.comment-reply-title span::after{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.widget .widget-title::before,.widget .widget-title::after{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.about-widget .more-link::after{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.follow-us-area .follow-link .fa:hover{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;border-color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.item-meta a,.popular-item-meta a{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.widget_categories li:hover > a,.widget_archive li:hover > a{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.widget_categories li:hover > .count,.widget_archive li:hover > .count{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.widget_categories li.current-cat > a,.widget_categories li.current-cat-parent > a,.widget_archive li.current-cat > a,.widget_archive li.current-cat-parent > a{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.widget_categories li.current-cat > .count,.widget_categories li.current-cat-parent > .count,.widget_archive li.current-cat > .count,.widget_archive li.current-cat-parent > .count{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.widget_tag_cloud .tagcloud a:hover{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;border-color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.widget_calendar caption{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.widget_calendar tbody a{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.widget_links li a:hover,.widget_meta li a:hover,.widget_nav_menu li a:hover,.widget_pages li a:hover,.widget_recent_comments li a:hover,.widget_recent_entries li a:hover{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.widget_links li span:hover:before,.widget_meta li span:hover:before,.widget_nav_menu li span:hover:before,.widget_pages li span:hover:before,.widget_recent_comments li span:hover:before,.widget_recent_entries li span:hover:before{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.tp_recent_tweets li span a{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.tp_recent_tweets li .twitter_time:before{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.tp_recent_tweets li .twitter_time{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.widget_glimmer_aboutus_contact .about-contact-area h3 span{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.widget_glimmer_aboutus_contact .about-contact-area .about-mail i{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.newsletter-area .form-newsletter #mc-embedded-subscribe:after{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.newsletter-area .form-newsletter #mc-embedded-subscribe:hover,.newsletter-area .form-newsletter #mc-embedded-subscribe:focus,.newsletter-area .form-newsletter #mc-embedded-subscribe:active:focus{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}#footer-top .widget .widget-title span::before,#footer-top .widget .widget-title span::after{color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}#footer-middle .widget_tag_cloud .tagcloud a:hover{border-color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}#footer-middle .widget_categories ul li .count:hover,#footer-middle .widget_archive ul li .count:hover{background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;border:1px solid <?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.topbutton,.widget_glimmer_login_form .glimmer-login-form .glimmer-login-board .right-details > p strong, .widget_glimmer_login_form .glimmer-login-form .glimmer-login-board .right-details i {color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;border-color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>}.content-area .nav-tabs li.active > a{border-top: 3px solid <?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;color:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>;}.search-form .btn.focus,.search-form .btn:focus,.search-form .btn:hover {background:<?php echo esc_attr( $softhopper_glimmer_color_scheme );?>} #header-middle, .widget, #footer-middle, .content-area .no-image .post-format {background:<?php echo glimmer_hex_2_rgba(esc_attr( $softhopper_glimmer_color_scheme ), 0.05);?>} .widget .widget-title:before {background:<?php echo glimmer_hex_2_rgba( $softhopper_glimmer_color_scheme, 0.1);?>}
</style>
<?php
    global $sh_glimmer_rgba_color_scheme;
    $sh_glimmer_rgba_color_scheme = glimmer_hex_2_rgba($softhopper_glimmer_color_scheme, 0.05);
} // end glimmer_color_scheme function
glimmer_color_scheme(); // here print the function

