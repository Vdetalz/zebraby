<?php
/**
 * Hooks for template footer
 *
 * @package Glimmer
 */

/**
 * Custom scripts  on footer
 *
 * @since  1.0
 */
function glimmer_footer_scripts() {
	global $softhopper_glimmer, $post;
	// Custom javascript
	if ( isset( $post->ID ) ) $meta = get_post_meta( $post->ID );
	$inline_js = '';
	isset( $meta["_glimmer_custom_js"][0] ) ? $softhopper_glimmer_custom_js = $meta["_glimmer_custom_js"][0] : $softhopper_glimmer_custom_js = '';
	isset( $softhopper_glimmer['custom_js'] ) ? $custom_js = $softhopper_glimmer['custom_js'] : $custom_js = '';
	
	$js_custom = $softhopper_glimmer_custom_js. $custom_js;

	$inline_js =  '<script>' . $js_custom . '	</script>' ;
	$inline_js2 =  "<script>function glimmerPopupWindow(url, title, w, h) {var left = (screen.width/2)-(w/2); var top = (screen.height/2)-(h/2); return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left); }</script>" ;
 
	if( $inline_js || $inline_js2 ) {
		echo ( $inline_js );
		echo ( $inline_js2 );
	} ?>  
	<?php // footer tracking code
	if( isset ( $softhopper_glimmer['tracking_code_for_footer'] )  && ! empty ( $softhopper_glimmer['tracking_code_for_footer']) ) {
        echo ( $softhopper_glimmer['tracking_code_for_footer'] );
    } 
}
add_action( 'wp_footer', 'glimmer_footer_scripts', 200 );
