<?php
/**
 * Hooks for template header
 *
 * @package Glimmer
 */

/**
 * Get favicon and home screen icons
 *
 * @since  1.0
 */
if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
	function glimmer_theme_header_icons() {
		global $softhopper_glimmer;
		$favicon = $softhopper_glimmer['favicon']['url'];
		$header_icons =  ( $favicon ) ? '<link rel="shortcut icon" type="image/x-ico" href="' . esc_url( $favicon ) . '" />' : '';

		$icon_iphone = $softhopper_glimmer['icon_iphone']['url'];
		$header_icons .= ( $icon_iphone ) ? '<link rel="apple-touch-icon" sizes="57x57"  href="' . esc_url( $icon_iphone ) . '" />' : '';

		$icon_iphone_retina = $softhopper_glimmer['icon_iphone_retina']['url'];
		$header_icons .= ( $icon_iphone_retina ) ? '<link rel="apple-touch-icon" sizes="114x114" href="' . esc_url( $icon_iphone_retina ). '" />' : '';

		$icon_ipad = $softhopper_glimmer['icon_ipad']['url'];
		$header_icons .= ( $icon_ipad ) ? '<link rel="apple-touch-icon" sizes="72x72" href="' . esc_url( $icon_ipad ) . '" />' : '';

		$icon_ipad_retina = $softhopper_glimmer['icon_ipad_retina']['url'];
		$header_icons .= ( $icon_ipad_retina ) ? '<link rel="apple-touch-icon" sizes="144x144" href="' . esc_url( $icon_ipad_retina ) . '" />' : '';

		$allowed_html_array = array(
	        'link' => array(
	            'rel' => array(),
	            'sizes' => array(),
	            'href' => array(),
	        ),
	    );
	    
	    if ( isset ( $header_icons ) ) echo wp_kses( $header_icons, $allowed_html_array );
	}
	add_action( 'wp_head', 'glimmer_theme_header_icons' );
}

/**
 * Custom scripts and styles on header
 *
 * @since  1.0
 */
function glimmer_header_scripts() {
	global $softhopper_glimmer, $post;
	// Include color schemer code
	require GLIMMER_TEMPLATE_DIR . '/inc/frontend/color-schemer.php'; 
	
	$custom_background = '';                          
	$custom_background = get_theme_mod( 'background_color' ); 
	if ( $custom_background == "" ) {
	    $custom_background = "#fff";
	}
	?>
	<style type="text/css"> 
		body {
			background: <?php echo esc_attr( $custom_background ); ?>;
		}		
	</style> 						
	<?php
	// Custom CSS
	if ( isset( $post->ID ) ) $meta = get_post_meta( $post->ID );
	$inline_css='';
	isset( $meta["_glimmer_custom_css"][0] ) ? $softhopper_glimmer_custom_css = $meta["_glimmer_custom_css"][0] : $softhopper_glimmer_custom_css = '';
	isset( $softhopper_glimmer['custom_css'] ) ? $custom_css = $softhopper_glimmer['custom_css'] : $custom_css = '';
	$inline_css = $softhopper_glimmer_custom_css . $custom_css;
	
	$inline_css = '<style type="text/css">' . $inline_css . '</style>';
	
	if( $inline_css ) {
		echo ( $inline_css );
	}	

	//header tracking code
	if( isset ( $softhopper_glimmer['tracking_code_for_head'] )  && ! empty ( $softhopper_glimmer['tracking_code_for_head']) ) {
        echo ( $softhopper_glimmer['tracking_code_for_head'] );
    } 
}
add_action( 'wp_head', 'glimmer_header_scripts', 300 );
