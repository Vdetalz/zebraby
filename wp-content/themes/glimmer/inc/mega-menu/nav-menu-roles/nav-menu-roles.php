<?php

/****************************************************
	Menu items display roles
	Thank's to: https://wordpress.org/plugins/nav-menu-roles/
****************************************************/
// don't load directly
if ( ! function_exists( 'is_admin' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


if ( ! class_exists( "Nav_Menu_Roles" ) ) :

class Nav_Menu_Roles {

	/**
	* @var Nav_Menu_Roles The single instance of the class
	* @since 1.5
	*/
	protected static $_instance = null;

	/**
	* Main Nav Menu Roles Instance
	*
	* Ensures only one instance of Nav Menu Roles is loaded or can be loaded.
	*
	* @since 1.5
	* @static
	* @see Nav_Menu_Roles()
	* @return Nav_Menu_Roles - Main instance
	*/
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	* Cloning is forbidden.
	*
	* @since 1.5
	*/
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' , 'glimmer'), '1.5' );
	}

	/**
	* Unserializing instances of this class is forbidden.
	*
	* @since 1.5
	*/
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' , 'glimmer'), '1.5' );
	}

	/**
	* Nav_Menu_Roles Constructor.
	* @access public
	* @return Nav_Menu_Roles
	* @since  1.0
	*/
	public function __construct(){

		// Admin functions
		add_action( 'admin_init', array( $this, 'admin_init' ) );

		// add new fields via hook
		add_action( 'wp_nav_menu_item_custom_fields', array( $this, 'custom_fields' ), 10, 4 );

		// save the menu item meta
		add_action( 'wp_update_nav_menu_item', array( $this, 'nav_update'), 10, 2 );

		// add meta to menu item
		add_filter( 'wp_setup_nav_menu_item', array( $this, 'setup_nav_item' ) );

		// exclude items via filter instead of via custom Walker
		if ( ! is_admin() ) {
			add_filter( 'wp_get_nav_menu_items', array( $this, 'exclude_menu_items' ) );
		}

	}

	/**
	* Include the custom admin walker
	*
	* @access public
	* @return void
	*/
	public function admin_init() {

		// Register Importer
		$this->register_importer();
	}


	/**
	* Register the Importer
	* the regular Importer skips post meta for the menu items
	*
	* @access private
	* @return void
	*/
	public function register_importer(){
		require_once get_template_directory().'/inc/mega-menu/nav-menu-roles/inc/class.Nav_Menu_Roles_Import.php';
		// Register the new importer
		if ( defined( 'WP_LOAD_IMPORTERS' ) ) {
			// Register the custom importer we've created.
			$roles_import = new Nav_Menu_Roles_Import();

			register_importer('nav_menu_roles', __('Nav Menu Roles', 'glimmer'), sprintf( __('Import %snav menu roles%s and other menu item meta skipped by the default importer', 'glimmer'), '<strong>', '</strong>'), array( $roles_import, 'dispatch' ) );
		}

	}



	/**
	* Add fields to hook added in Walker
	* This will allow us to play nicely with any other plugin that is adding the same hook
	* @params obj $item - the menu item 
	* @params array $args 
	* @since 1.6.0
	*/
	public function custom_fields( $item_id, $item, $depth, $args ) {
		global $wp_roles;

		/**
		* Pass the menu item to the filter function.
		* This change is suggested as it allows the use of information from the menu item (and
		* by extension the target object) to further customize what filters appear during menu
		* construction.
		*/
		$display_roles = apply_filters( 'nav_menu_roles', $wp_roles->role_names, $item );


		/**
		* If no roles are being used, don't display the role selection radio buttons at all.
		* Unless something deliberately removes the WordPress roles from this list, nothing will
		* be functionally altered to the end user.
		* This change is suggested for the benefit of users constructing granular admin permissions
		* using extensive custom roles as it is an effective means of stopping admins with partial
		* permissions to the menu from accidentally removing all restrictions from a menu item to
		* which they do not have access.
		*/
		if( ! $display_roles ) return;

		/* Get the roles saved for the post. */
		$roles = get_post_meta( $item->ID, '_nav_menu_role', true );

		// by default nothing is checked (will match "everyone" radio)
		$logged_in_out = '';

		// specific roles are saved as an array, so "in" or an array equals "in" is checked
		if( is_array( $roles ) || $roles == 'in' ){
			$logged_in_out = 'in';
		} else if ( $roles == 'out' ){
			$logged_in_out = 'out';
		}

		// the specific roles to check
		$checked_roles = is_array( $roles ) ? $roles : false;

		?>

		<input type="hidden" name="nav-menu-role-nonce" value="<?php echo wp_create_nonce( 'nav-menu-nonce-name' ); ?>" />

		<div class="field-nav_menu_role nav_menu_logged_in_out_field description-wide" style="margin: 5px 0;">
		    <span class="description"><?php esc_html_e( "Display Mode", 'glimmer' ); ?></span>
		    <br />

		    <input type="hidden" class="nav-menu-id" value="<?php echo esc_attr($item->ID); ?>" />

		    <div class="logged-input-holder" style="float: left; width: 35%;">
		        <input type="radio" class="nav-menu-logged-in-out" name="nav-menu-logged-in-out[<?php echo esc_attr($item->ID); ?>]" id="nav_menu_logged_in-for-<?php echo esc_attr($item->ID);?>" <?php checked( 'in', $logged_in_out ); ?>  value="in" />
		        <label for="nav_menu_logged_in-for-<?php echo esc_attr($item->ID);?>">
		            <?php esc_html_e( 'Logged In Users', 'glimmer'); ?>
		        </label>
		    </div>

		    <div class="logged-input-holder" style="float: left; width: 35%;">
		        <input type="radio" class="nav-menu-logged-in-out" name="nav-menu-logged-in-out[<?php echo esc_attr($item->ID); ?>]" id="nav_menu_logged_out-for-<?php echo esc_attr($item->ID);?>" <?php checked( 'out', $logged_in_out ); ?> value="out" />
		        <label for="nav_menu_logged_out-for-<?php echo esc_attr($item->ID);?>">
		            <?php esc_html_e( 'Logged Out Users', 'glimmer'); ?>
		        </label>
		    </div>

		    <div class="logged-input-holder" style="float: left; width: 30%;">
		        <input type="radio" class="nav-menu-logged-in-out" name="nav-menu-logged-in-out[<?php echo esc_attr($item->ID); ?>]" id="nav_menu_by_role-for-<?php echo esc_attr($item->ID); ?>" <?php checked( '', $logged_in_out ); ?> value="" />
		        <label for="nav_menu_by_role-for-<?php echo esc_attr($item->ID); ?>">
		            <?php esc_html_e( 'Everyone', 'glimmer'); ?>
		        </label>
		    </div>

		</div>

		<div class="field-nav_menu_role nav_menu_role_field description-wide hide" style="margin: 5px 0;">
		    <span class="description"><?php esc_html_e( "Restrict menu item to a minimum role", 'glimmer' ); ?></span>
		    <br />

		    <?php

		    /* Loop through each of the available roles. */
		    foreach ( $display_roles as $role => $name ) {

		        /* If the role has been selected, make sure it's checked. */
		        $checked = checked( true, ( is_array( $checked_roles ) && in_array( $role, $checked_roles ) ), false );
		        ?>

		        <div class="role-input-holder" style="float: left; width: 33.3%; margin: 2px 0;">
		        <input type="checkbox" name="nav-menu-role[<?php echo esc_attr($item->ID); ?>][<?php echo esc_attr($role); ?>]" id="nav_menu_role-<?php echo esc_attr($role); ?>-for-<?php echo esc_attr($item->ID); ?>" <?php echo esc_attr($checked); ?> value="<?php echo esc_attr($role); ?>" />
		        <label for="nav_menu_role-<?php echo esc_attr($role); ?>-for-<?php echo esc_attr($item->ID); ?>">
		        <?php echo esc_html( $name ); ?>
		        </label>
		        </div>
		<?php } ?>
		</div>
		<?php 
	}

	/**
	* Save the roles as menu item meta
	* @return string
	* @since 1.0
	*/
	public function nav_update( $menu_id, $menu_item_db_id ) {
		global $wp_roles;

		$allowed_roles = apply_filters( 'nav_menu_roles', $wp_roles->role_names );

		// verify this came from our screen and with proper authorization.
		if ( ! isset( $_POST['nav-menu-role-nonce'] ) || ! wp_verify_nonce( $_POST['nav-menu-role-nonce'], 'nav-menu-nonce-name' ) )
			return;

		$saved_data = false;

		if ( isset( $_POST['nav-menu-logged-in-out'][$menu_item_db_id]  )  && $_POST['nav-menu-logged-in-out'][$menu_item_db_id] == 'in' && ! empty ( $_POST['nav-menu-role'][$menu_item_db_id] ) ) {
			$custom_roles = array();
			// only save allowed roles
			foreach( $_POST['nav-menu-role'][$menu_item_db_id] as $role ) {
				if ( array_key_exists ( $role, $allowed_roles ) ) $custom_roles[] = $role;
			}
			if ( ! empty ( $custom_roles ) ) $saved_data = $custom_roles;
		} else if ( isset( $_POST['nav-menu-logged-in-out'][$menu_item_db_id]  )  && in_array( $_POST['nav-menu-logged-in-out'][$menu_item_db_id], array( 'in', 'out' ) ) ) {
			$saved_data = $_POST['nav-menu-logged-in-out'][$menu_item_db_id];
		} 

		if ( $saved_data ) {
			update_post_meta( $menu_item_db_id, '_nav_menu_role', $saved_data );
		} else {
			delete_post_meta( $menu_item_db_id, '_nav_menu_role' );
		}
	}

	/**
	* Adds value of new field to $item object
	* is be passed to Walker_Nav_Menu_Edit_Custom
	* @since 1.0
	*/
	public function setup_nav_item( $menu_item ) {
		$roles = get_post_meta( $menu_item->ID, '_nav_menu_role', true );

		if ( ! empty( $roles ) ) {
			$menu_item->roles = $roles;
		}
		return $menu_item;
	}

	/**
	* Exclude menu items via wp_get_nav_menu_items filter
	* this fixes plugin's incompatibility with theme's that use their own custom Walker
	* Thanks to Evan Stein @vanpop http://vanpop.com/
	* @since 1.2
	*/
	public function exclude_menu_items( $items ) {

		$hide_children_of = array();

		// Iterate over the items to search and destroy
		foreach ( $items as $key => $item ) {

			$visible = true;

			// hide any item that is the child of a hidden item
			if( in_array( $item->menu_item_parent, $hide_children_of ) ){
				$visible = false;
				$hide_children_of[] = $item->ID; // for nested menus
			}

			// check any item that has NMR roles set
			if( $visible && isset( $item->roles ) ) {

				// check all logged in, all logged out, or role
				switch( $item->roles ) {
					case 'in' :
					$visible = is_user_logged_in() ? true : false;
						break;
					case 'out' :
					$visible = ! is_user_logged_in() ? true : false;
						break;
					default:
						$visible = false;
						if ( is_array( $item->roles ) && ! empty( $item->roles ) ) {
							foreach ( $item->roles as $role ) {
								if ( current_user_can( $role ) ) 
									$visible = true;
							}
						}
						break;
				}

			}

			// add filter to work with plugins that don't use traditional roles
			$visible = apply_filters( 'nav_menu_roles_item_visibility', $visible, $item );

			// unset non-visible item
			if ( ! $visible ) {
				$hide_children_of[] = $item->ID; // store ID of item 
				unset( $items[$key] ) ;
			}
		}
		return $items;
}

} // end class

endif; // class_exists check


/**
* Launch the whole plugin
 * Returns the main instance of Nav Menu Roles to prevent the need to use globals.
 *
 * @since  1.5
 * @return Nav_Menu_Roles
*/
function Nav_Menu_Roles() {
	return Nav_Menu_Roles::instance();
}

// Global for backwards compatibility.
$GLOBALS['Nav_Menu_Roles'] = Nav_Menu_Roles();