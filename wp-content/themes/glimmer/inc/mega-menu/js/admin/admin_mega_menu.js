jQuery(document).ready(function($) {

    $('.menu_icon_bt').on('click', function() {
        mid = "#edit-menu-item-micon-" + $(this).attr('data-id');
        prv = "#icon_prv_" + $(this).attr('data-id');
        $('#sh_menu_icon').on('click', function() {
            var icon = $('input[name="sh_menu_item_icon"]:checked').val();
            $(mid).val(icon);
            $(prv).removeClass().addClass('icon_prv ' + icon);
            $(prv).find('.remove_icon').show();
            tb_remove();
            return false;
        });
    });
    $('.icon_prv').each(function() {
        $(this).addClass($(this).next('input').val());
    });

    $('.icon_prv .remove_icon').click(function() {
        $(this).hide();
        $(this).parent().removeClass().addClass('icon_prv');
        $(this).parent().next('input').val('');
        return false;
    });
    $('.edit-menu-item-micon').each(function() {
        if ($(this).val() === '') {
            $(this).prev('.icon_prv').find('.remove_icon').hide();
        }
    });

    $.sh_mega_menus_type = function() {
        $('.edit-menu-item-mtype').on('change', function() {
            var t = $(this);
            var id = $(this).attr('id');
            id = id.split('-');
            if ($(this).val() === 'cats') {
                $(this).parent().parent().parent().find('.field-mcats_layout').slideDown(250);
                $(this).parent().parent().parent().find('.field-mcustom').slideUp('fast');
                $(this).parent().parent().parent().find('.field-mcats_column_num').slideUp('fast');
            } else if ($(this).val() === 'cats_column') {
                $(this).parent().parent().parent().find('.field-mcats_column_num').slideDown(250);
                $(this).parent().parent().parent().find('.field-mcustom').slideUp('fast');
                $(this).parent().parent().parent().find('.field-mcats_layout').slideUp('fast');
            } else if ($(this).val() === 'custom') {
                $(this).parent().parent().parent().find('.field-mcustom').slideDown(250);
                $(this).parent().parent().parent().find('.field-mcats_layout').slideUp('fast');
                $(this).parent().parent().parent().find('.field-mcats_column_num').slideUp('fast');
            } else {
                $(this).parent().parent().parent().find('.field-mcustom').slideUp('fast');
                $(this).parent().parent().parent().find('.field-mcats_layout').slideUp('fast');
                $(this).parent().parent().parent().find('.field-mcats_column_num').slideUp('fast');
            }
        });
    }
    $.sh_mega_menus_type();

    $('.edit-menu-item-mtype').each(function() {
        if ($(this).val() === 'cats') {
            $(this).parent().parent().parent().find('.field-mcats_layout').slideDown(250);
            $(this).parent().parent().parent().find('.field-mcustom').slideUp('fast');
            $(this).parent().parent().parent().find('.field-mcats_column_num').slideUp('fast');
        } else if ($(this).val() === 'cats_column') {
            $(this).parent().parent().parent().find('.field-mcats_column_num').slideDown(250);
            $(this).parent().parent().parent().find('.field-mcustom').slideUp('fast');
            $(this).parent().parent().parent().find('.field-mcats_layout').slideUp('fast');
        } else if ($(this).val() === 'custom') {
            $(this).parent().parent().parent().find('.field-mcustom').slideDown(250);
            $(this).parent().parent().parent().find('.field-mcats_layout').slideUp('fast');
            $(this).parent().parent().parent().find('.field-mcats_column_num').slideUp('fast');
        } else {
            $(this).parent().parent().parent().find('.field-mcustom').slideUp('fast');
            $(this).parent().parent().parent().find('.field-mcats_layout').slideUp('fast');
            $(this).parent().parent().parent().find('.field-mcats_column_num').slideUp('fast');
        }
    });

    $(document).on('click mouseover', '.sh_select_icon_menu, .mce-sh-custom-icon', function() {
        if ($.isFunction($.sh_load_menu_icons)) {
            $.sh_load_menu_icons();
        }
    });

    $(document).on('mouseover', '.edit-menu-item-mtype', function() {
        if ($.isFunction($.sh_mega_menus_type)) {
            $.sh_mega_menus_type();
        }
    });

    // nav menu roles

    $('.nav_menu_logged_in_out_field').each(function(i) {

        var $field = $(this);

        var id = $field.find('input.nav-menu-id').val();

        // if set to display by role (aka is null) then show the roles list, otherwise hide
        if ($field.find('input.nav-menu-logged-in-out:checked').val() === 'in') {
            $field.next('.nav_menu_role_field').show();
        } else {
            $field.next('.nav_menu_role_field').hide();
        }
    });

    // on in/out/role change, hide/show the roles
    $('#menu-to-edit').on('change', 'input.nav-menu-logged-in-out', function() {
        if ($(this).val() === 'in') {
            $(this).parentsUntil('.nav_menu_logged_in_out').next('.nav_menu_role_field').slideDown();
        } else {
            $(this).parentsUntil('.nav_menu_logged_in_out').next('.nav_menu_role_field').slideUp();
        }
    });

    // modal box
    jQuery("body").on('click', '.sh_media_box_overlay', function() {
        $('.sh_modal_box').fadeOut();
        $('.sh_media_box_overlay').fadeOut();
    });

    jQuery("body").on('click', '#sh_modal_close', function() {
        $('.sh_modal_box').fadeOut();
        $('.sh_media_box_overlay').fadeOut();
    });

    jQuery("body").on('click', '.sh_modal_save', function(e) {
        e.preventDefault();
        $('.sh_modal_box').fadeOut();
        $('.sh_media_box_overlay').fadeOut();
    });

    // Load Icons 
    jQuery("body").on('click', '.sh_select_icon', function(e) {
        var t = jQuery(this);
        $('.sh_modal_box').fadeIn();
        $('.sh_media_box_overlay').fadeIn();
        jQuery.ajax({
            type: "post",
            url: ShCats.url,
            dataType: 'html',
            data: "action=sh_loadIcon&nonce=" + ShCats.nonce,
            beforeSend: function() {},
            success: function(data) {
                $('.sh_modal_box').find('.sh_modal_content').html(data);
                jQuery("body").on('click', '.sh_modal_save', function(e) {
                    var icon = $('.sh_modal_box').find('input[name="sh_menu_item_icon"]:checked').val();
                    t.parent('.sh_icons_selector').find('.sh_icon_holder').val(icon);
                    t.parent('.sh_icons_selector').find('.sh_icon_prev i').removeClass().addClass(icon);
                });
            }
        });

        return false;
    });

    // Load Menu Icons
    $.sh_load_menu_icons = function() {
        var idm = '';
        var iconm = '';
        jQuery(".sh_select_icon_menu, .mce-sh-custom-icon").on('click', function(e) {
            var t = jQuery(this);
            idm = t.data('id');
            $('.sh_modal_box').fadeIn();
            $('.sh_media_box_overlay').fadeIn();
            jQuery.ajax({
                type: "post",
                url: ShCats.url,
                dataType: 'html',
                data: "action=sh_loadIcon&nonce=" + ShCats.nonce,
                beforeSend: function() {},
                success: function(data) {
                    $('.sh_modal_box').find('.sh_modal_content').html(data);

                },
            });
            
            var post_and_page = ShCats.post_and_page;
            //jQuery("body").on('click', '.sh_modal_save', function(e) {
            $(".sh_modal_save").unbind().on('click', function (e) {
                iconm = $('.sh_modal_box').find('input[name="sh_menu_item_icon"]:checked').val();
                $('#menu-item-settings-' + idm + ' .sh_icons_selector').find('.sh_icon_holder').val(iconm);
                $('#menu-item-settings-' + idm + ' .sh_icons_selector').find('.sh_icon_prev i').removeClass().addClass(iconm);
                if ( post_and_page == true ) {
                    if(tinyMCE && tinyMCE.activeEditor) {
                        tinyMCE.activeEditor.selection.setContent('[custom_icon name="'+iconm+'"]');
                    } 
                }
            });
            return false;
        });

    }

    jQuery.custom_icon_Function = function() {
        var idm = '';
        var iconm = '';
        var custom_uploader;
        jQuery(".sh_upload_icon_menu").on('click', function(e) {
            var t = jQuery(this);
            idm = t.data('id');


            e.preventDefault();

            //If the uploader object has already been created, reopen the dialog
            if (custom_uploader) {
                custom_uploader.open();
                return;
            }
            //Extend the wp.media object
            custom_uploader = wp.media.frames.file_frame = wp.media({
                title: 'Choose Image',
                button: {
                    text: 'Choose Image'
                },
                multiple: false
            });
            //When a file is selected, grab the URL and set it as the text field's value
            custom_uploader.on('select', function() {
                var attachment = custom_uploader.state().get('selection').first().toJSON();
                $('#menu-item-settings-' + idm + ' .sh_icons_selector').find('.sh_icon_holder').val(attachment.url);
                $('#menu-item-settings-' + idm + ' .sh_icons_selector').find('.sh_icon_prev i').removeClass().parent().css({
                    background: 'url(' + attachment.url + ') center no-repeat',
                    backgroundSize: '24px'

                });
            });
            //Open the uploader dialog
            custom_uploader.open();

        });
    };

    $.sh_load_menu_icons();
    $.custom_icon_Function();


    //Icon slect 
    $('.sh_icons_selector').each(function() {
        var icon = $(this).find('.sh_icon_holder').val();
        if (icon.match("^http://") || icon.match("^https://")) {
            $(this).find('.sh_icon_prev').css({
                background: 'url(' + icon + ') center no-repeat',
                backgroundSize: '24px'

            });
        } else {
            $(this).find('.sh_icon_prev i').addClass(icon);
        }
    });

    $('.sh_icon_prev').on('click', '.remove_icon', function(e) {
        e.preventDefault();
        $(this).parent().parent('.sh_icons_selector').find('.sh_icon_holder').val('');
        $(this).parent().find('i').removeClass();
        $(this).parent().css('background', 'none');
    });

    //icon live search
    setTimeout(function() {
        jQuery("#sh_icons_filter").focus();
    }, 1000);
    $("body").on('keyup', '#sh_icons_filter', function() {
        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(),
            count = 0;

        // Loop through the icons
        $(".icons_wrap .sh_tiny_icon").each(function() {
            var classname = $('i', this).attr('class');
            // If the list item does not contain the text phrase fade it out
            if (classname.search(new RegExp(filter, "i")) < 0) { // use the variable here
                $(this).hide();

                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).fadeIn();
                count++;
            }
        });

    });

});