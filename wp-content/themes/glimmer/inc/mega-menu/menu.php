<?php

if ( ! function_exists( 'glimmer_theme_mega_menu_crop_size_setup' ) ) :
    function glimmer_theme_mega_menu_crop_size_setup() {
        add_image_size( 'shopper-mega-menu-cat-menu-vertical', 95, 77, true );
        add_image_size( 'shopper-mega-menu-cat-menu-horizontal', 115, 85, true );
        add_image_size( 'shopper-mega-menu-cat-column', 260, 175, true );
    }
endif; // glimmer_theme_mega_menu_crop_size_setup
add_action( 'after_setup_theme', 'glimmer_theme_mega_menu_crop_size_setup' );


if( function_exists('glimmer_self_serv_function_checker') ) {
    if ( glimmer_self_serv_function_checker() == "nav-menus.php" || glimmer_self_serv_function_checker() == "post.php" || glimmer_self_serv_function_checker() == "post-new.php" ) {
        add_action('admin_menu', 'sh_mega_menu_style');
    }
    if ( glimmer_self_serv_function_checker() == "nav-menus.php" ) {
        add_action('admin_menu', 'sh_mega_menu_wp_enqueue_media');
    }
    
    $post_and_page = false;
    if ( glimmer_self_serv_function_checker() == "post.php" || glimmer_self_serv_function_checker() == "post-new.php" ) {
        $post_and_page = true;
    }
}

function sh_mega_menu_style() {
    global $post_and_page;
    wp_enqueue_style(  'sh_mega_menu_style', get_template_directory_uri(). '/inc/mega-menu/css/admin/admin_mega_menu.css'); 
    wp_enqueue_style('thickbox'); 
    wp_enqueue_script('thickbox');
    wp_enqueue_script('sh-mega-menu', get_template_directory_uri(). '/inc/mega-menu/js/admin/admin_mega_menu.js'); 
    wp_localize_script( 'sh-mega-menu', 'ShCats', array(
        'url' => admin_url( 'admin-ajax.php' ),
        'post_and_page' => $post_and_page,
        'nonce' => wp_create_nonce( 'ajax-nonce' ),
        )
    );
}

function sh_mega_menu_wp_enqueue_media() {
    wp_enqueue_media();
}

/**
 * @package nav-menu-custom-fields
 * @version 0.1.0
 */
/*
Plugin Name: Nav Menu Custom Fields
*/

/*
 * Saves new field to postmeta for navigation
 */
add_action('wp_update_nav_menu_item', 'custom_nav_update',10, 3);
function custom_nav_update($menu_id, $menu_item_db_id, $args ) {
    if (isset($_REQUEST['menu-item-mtype']) ) {
        if ( is_array($_REQUEST['menu-item-mtype']) ) {
            $custom_value = $_REQUEST['menu-item-mtype'][$menu_item_db_id];
            update_post_meta( $menu_item_db_id, '_menu_item_mtype', $custom_value );
        }
    }

    if (isset($_REQUEST['menu-item-mcats_layout']) ) {
        if ( is_array($_REQUEST['menu-item-mcats_layout']) ) {
            $custom_value = $_REQUEST['menu-item-mcats_layout'][$menu_item_db_id];
            update_post_meta( $menu_item_db_id, '_menu_item_mcats_layout', $custom_value );
        }
    }

    if (isset($_REQUEST['menu-item-mcats_column_num']) ) {
        if ( is_array($_REQUEST['menu-item-mcats_column_num']) ) {
            $custom_value = $_REQUEST['menu-item-mcats_column_num'][$menu_item_db_id];
            update_post_meta( $menu_item_db_id, '_menu_item_mcats_column_num', $custom_value );
        }
    }

    if (isset($_REQUEST['menu-item-mcustom']) ) {
        if ( is_array($_REQUEST['menu-item-mcustom']) ) {
            $custom_value = $_REQUEST['menu-item-mcustom'][$menu_item_db_id];
            update_post_meta( $menu_item_db_id, '_menu_item_mcustom', $custom_value );
        }
    }


    if (isset($_REQUEST['menu-item-micon']) ) {
        if ( is_array($_REQUEST['menu-item-micon']) ) {
            $icon_value = $_REQUEST['menu-item-micon'][$menu_item_db_id];
            update_post_meta( $menu_item_db_id, '_menu_item_micon', $icon_value );
        }
    }

    if (isset($_REQUEST['menu-item-mdisplay']) ) {
        if ( is_array($_REQUEST['menu-item-mdisplay']) ) {
            $icon_value = $_REQUEST['menu-item-mdisplay'][$menu_item_db_id];
            update_post_meta( $menu_item_db_id, '_menu_item_mdisplay', $icon_value );
        }
    }
    

}

/*
 * Adds value of new field to $item object that will be passed to     Walker_Nav_Menu_Edit_Custom
 */
add_filter( 'wp_setup_nav_menu_item','custom_nav_item' );
function custom_nav_item($menu_item) {
    $menu_item->mtype = get_post_meta( $menu_item->ID, '_menu_item_mtype', true );
    $menu_item->mcats_layout = get_post_meta( $menu_item->ID, '_menu_item_mcats_layout', true );
    $menu_item->mcats_column_num = get_post_meta( $menu_item->ID, '_menu_item_mcats_column_num', true );
    $menu_item->mcustom = get_post_meta( $menu_item->ID, '_menu_item_mcustom', true );
    $menu_item->micon = get_post_meta( $menu_item->ID, '_menu_item_micon', true );
    $menu_item->mdisplay = get_post_meta( $menu_item->ID, '_menu_item_mdisplay', true );
    return $menu_item;
}

add_filter( 'wp_edit_nav_menu_walker', 'custom_nav_edit_walker',10,2 );
function custom_nav_edit_walker($walker,$menu_id) {
    return 'Walker_Nav_Menu_Edit_Custom';
}

/**
 * Copied from Walker_Nav_Menu_Edit class in core
 *
 * Create HTML list of nav menu input items.
 *
 * @package WordPress
 * @since 3.0.0
 * @uses Walker_Nav_Menu
 */
class Walker_Nav_Menu_Edit_Custom extends Walker_Nav_Menu {
    /**
     * Starts the list before the elements are added.
     *
     * @see Walker_Nav_Menu::start_lvl()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   Not used.
     */
    public function start_lvl( &$output, $depth = 0, $args = array() ) {}

    /**
     * Ends the list of after the elements are added.
     *
     * @see Walker_Nav_Menu::end_lvl()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   Not used.
     */
    public function end_lvl( &$output, $depth = 0, $args = array() ) {}

    /**
     * Start the element output.
     *
     * @see Walker_Nav_Menu::start_el()
     * @since 3.0.0
     *
     * @global int $_wp_nav_menu_max_depth
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   Not used.
     * @param int    $id     Not used.
     */
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $_wp_nav_menu_max_depth;
        $_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;

        ob_start();
        $item_id = esc_attr( $item->ID );
        $removed_args = array(
            'action',
            'customlink-tab',
            'edit-menu-item',
            'menu-item',
            'page-tab',
            '_wpnonce',
        );

        $original_title = '';
        if ( 'taxonomy' == $item->type ) {
            $original_title = get_term_field( 'name', $item->object_id, $item->object, 'raw' );
            if ( is_wp_error( $original_title ) )
                $original_title = false;
        } elseif ( 'post_type' == $item->type ) {
            $original_object = get_post( $item->object_id );
            $original_title = get_the_title( $original_object->ID );
        } elseif ( 'post_type_archive' == $item->type ) {
            $original_object = get_post_type_object( $item->object );
            $original_title = $original_object->labels->archives;
        }

        $classes = array(
            'menu-item menu-item-depth-' . $depth,
            'menu-item-' . esc_attr( $item->object ),
            'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive'),
        );

        $title = $item->title;

        if ( ! empty( $item->_invalid ) ) {
            $classes[] = 'menu-item-invalid';
            /* translators: %s: title of menu item which is invalid */
            $title = sprintf( '%s'.__( ' (Invalid)', 'glimmer' ), $item->title );
        } elseif ( isset( $item->post_status ) && 'draft' == $item->post_status ) {
            $classes[] = 'pending';
            /* translators: %s: title of menu item in draft status */
            $title = sprintf( '%s'.__(' (Pending)', 'glimmer'), $item->title );
        }

        $title = ( ! isset( $item->label ) || '' == $item->label ) ? $title : $item->label;

        $submenu_text = '';
        if ( 0 == $depth )
            $submenu_text = 'style="display: none;"';

        ?>
        <li id="menu-item-<?php echo esc_attr($item_id); ?>" class="<?php echo implode(' ', $classes ); ?>">
            <div class="menu-item-bar">
                <div class="menu-item-handle">
                    <span class="item-title"><span class="menu-item-title"><?php echo esc_html( $title ); ?></span> <span class="is-submenu" <?php echo esc_attr($submenu_text); ?>><?php esc_html_e( 'sub item', 'glimmer' ); ?></span></span>
                    <span class="item-controls">
                        <span class="item-type"><?php echo esc_html( $item->type_label ); ?></span>
                        <span class="item-order hide-if-js">
                            <a href="<?php
                                echo wp_nonce_url(
                                    add_query_arg(
                                        array(
                                            'action' => 'move-up-menu-item',
                                            'menu-item' => $item_id,
                                        ),
                                        remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
                                    ),
                                    'move-menu_item'
                                );
                            ?>" class="item-move-up"><abbr title="<?php esc_attr_e('Move up', 'glimmer'); ?>">&#8593;</abbr></a>
                            |
                            <a href="<?php
                                echo wp_nonce_url(
                                    add_query_arg(
                                        array(
                                            'action' => 'move-down-menu-item',
                                            'menu-item' => $item_id,
                                        ),
                                        remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
                                    ),
                                    'move-menu_item'
                                );
                            ?>" class="item-move-down"><abbr title="<?php esc_attr_e('Move down', 'glimmer'); ?>">&#8595;</abbr></a>
                        </span>
                        <a class="item-edit" id="edit-<?php echo esc_attr($item_id); ?>" title="<?php esc_attr_e('Edit Menu Item', 'glimmer'); ?>" href="<?php
                            echo ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? admin_url( 'nav-menus.php' ) : add_query_arg( 'edit-menu-item', $item_id, remove_query_arg( $removed_args, admin_url( 'nav-menus.php#menu-item-settings-' . $item_id ) ) );
                        ?>"><?php esc_html_e( 'Edit Menu Item', 'glimmer' ); ?></a>
                    </span>
                </div>
            </div>

            <div class="menu-item-settings" id="menu-item-settings-<?php echo esc_attr($item_id); ?>">
                <?php if ( 'custom' == $item->type ) : ?>
                    <p class="field-url description description-wide">
                        <label for="edit-menu-item-url-<?php echo esc_attr($item_id); ?>">
                            <?php esc_html_e( 'URL', 'glimmer' ); ?><br />
                            <input type="text" id="edit-menu-item-url-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-url" name="menu-item-url[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->url ); ?>" />
                        </label>
                    </p>
                <?php endif; ?>
                <p class="description description-wide">
                    <label for="edit-menu-item-title-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Navigation Label', 'glimmer' ); ?><br />
                        <input type="text" id="edit-menu-item-title-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-title" name="menu-item-title[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->title ); ?>" />
                    </label>
                </p>
                <p class="field-title-attribute description description-wide">
                    <label for="edit-menu-item-attr-title-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Title Attribute', 'glimmer' ); ?><br />
                        <input type="text" id="edit-menu-item-attr-title-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-attr-title" name="menu-item-attr-title[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->post_excerpt ); ?>" />
                    </label>
                </p>
                <p class="field-link-target description">
                    <label for="edit-menu-item-target-<?php echo esc_attr($item_id); ?>">
                        <input type="checkbox" id="edit-menu-item-target-<?php echo esc_attr($item_id); ?>" value="_blank" name="menu-item-target[<?php echo esc_attr($item_id); ?>]"<?php checked( $item->target, '_blank' ); ?> />
                        <?php esc_html_e( 'Open link in a new tab', 'glimmer' ); ?>
                    </label>
                </p>
                <p class="field-css-classes description description-thin">
                    <label for="edit-menu-item-classes-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'CSS Classes (optional)', 'glimmer' ); ?><br />
                        <input type="text" id="edit-menu-item-classes-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-classes" name="menu-item-classes[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( implode(' ', $item->classes ) ); ?>" />
                    </label>
                </p>
                <p class="field-xfn description description-thin">
                    <label for="edit-menu-item-xfn-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Link Relationship (XFN)', 'glimmer' ); ?><br />
                        <input type="text" id="edit-menu-item-xfn-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-xfn" name="menu-item-xfn[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->xfn ); ?>" />
                    </label>
                </p>
                <p class="field-description description description-wide">
                    <label for="edit-menu-item-description-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Description', 'glimmer' ); ?><br />
                        <textarea id="edit-menu-item-description-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-description" rows="3" cols="20" name="menu-item-description[<?php echo esc_attr($item_id); ?>]"><?php echo esc_html( $item->description ); // textarea_escaped ?></textarea>
                        <span class="description"><?php esc_html_e('The description will be displayed in the menu if the current theme supports it.', 'glimmer'); ?></span>
                    </label>
                </p>
                <?php
                /*
                 * Custom field added from here
                 */
                ?>      
                <p class="field-mtype description description-wide">
                    <label for="edit-menu-item-mtype-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Dropdown Menu Type', 'glimmer' ); ?><br />
                        <select id="edit-menu-item-mtype-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-mtype" name="menu-item-mtype[<?php echo esc_attr($item_id); ?>]">
                            <option value="" <?php selected( $item->mtype, '' ); ?>><?php esc_html_e( 'Default', 'glimmer' ); ?></option>
                            <option value="mega" <?php selected( $item->mtype, 'mega' ); ?>><?php esc_html_e( 'Mega Menu', 'glimmer' ); ?></option>
                            <option value="cats" <?php selected( $item->mtype, 'cats' ); ?>><?php esc_html_e( 'Category Menu', 'glimmer' ); ?></option>
                            <option value="cats_column" <?php selected( $item->mtype, 'cats_column' ); ?>><?php esc_html_e( 'Category Column', 'glimmer' ); ?></option>
                            <option value="custom" <?php selected( $item->mtype, 'custom' ); ?>><?php esc_html_e( 'Custom Mega Menu', 'glimmer' ); ?></option>
                        </select>
                    </label>
                </p>
                
                <p class="field-mcats_layout description description-wide hide">
                    <label for="edit-menu-item-mcats_layout-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Categories Posts layout', 'glimmer' ); ?><br />
                        <select id="edit-menu-item-mcats_layout-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-mcats_layout" name="menu-item-mcats_layout[<?php echo esc_attr($item_id); ?>]">
                            <option value="" <?php selected( $item->mcats_layout, '' ); ?>><?php esc_html_e( 'Vertical', 'glimmer' ); ?></option>
                            <option value="horz" <?php selected( $item->mcats_layout, 'horz' ); ?>><?php esc_html_e( 'Horizontal', 'glimmer' ); ?></option>
                        </select>
                    </label>
                </p>

                <p class="field-mcats_column_num description description-wide hide">
                    <label for="edit-menu-item-mcats_column_num-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Category Column', 'glimmer' ); ?><br />
                        <select id="edit-menu-item-mcats_column_num-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-mcats_column_num" name="menu-item-mcats_column_num[<?php echo esc_attr($item_id); ?>]">
                            <option value="4" <?php selected( $item->mcats_column_num, '4' ); ?>><?php esc_html_e( '4', 'glimmer' ); ?></option>
                            <option value="3" <?php selected( $item->mcats_column_num, '3' ); ?>><?php esc_html_e( '3', 'glimmer' ); ?></option>
                            <option value="2" <?php selected( $item->mcats_column_num, '2' ); ?>><?php esc_html_e( '2', 'glimmer' ); ?></option>
                        </select>
                    </label>
                </p>
                
                <p class="field-mcustom description description-wide hide">
                    <label for="edit-menu-item-mcustom-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Custom Mega Menu Content', 'glimmer' ); ?><br />
                        <textarea id="edit-menu-item-mcustom-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-mcustom" rows="3" cols="20" name="menu-item-mcustom[<?php echo esc_attr($item_id); ?>]"><?php echo wp_kses_post( $item->mcustom ); ?></textarea>
                        <small><?php esc_html_e('Custom text, HTML or Shortcodes note: all items under this menu will disappear', 'glimmer'); ?></small>
                    </label>
                </p>

                <p class="field-micon description description-wide">
                    <label for="edit-menu-item-micon-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Menu Item Icon', 'glimmer' ); ?>
                        <br />
                        <div class="sh_icons_selector">
                            <a class="sh_select_icon_menu button" data-id="<?php echo esc_attr($item_id); ?>"><?php esc_html_e('Select Icon','glimmer'); ?></a> 
                            <span class="or">or</span> 
                            <a class="sh_upload_icon_menu button simptip-position-top simptip-movable simptip-multiline" data-tooltip="<?php esc_attr_e('Best Icon sizes is : 24px for icon only and 18px for icon with label', 'glimmer'); ?>" data-id="<?php echo esc_attr($item_id); ?>"><?php esc_html_e('Upload Custom Icon','glimmer'); ?></a>

                            <span class="sh_icon_prev">
                                <i></i>
                                <a href="#" class="remove_icon enotype-icon-cross2" title="<?php esc_attr_e('Remove Icon', 'glimmer'); ?>"></a>
                            </span>

                            <input type="hidden" id="edit-menu-item-micon-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-micon sh_icon_holder" name="menu-item-micon[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->micon ); ?>" \>
                        </div>
                    </label>
                </p>

                <p class="field-mdisplay description description-wide">
                    <label for="edit-menu-item-mdisplay-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Display', 'glimmer' ); ?><br />
                        <select id="edit-menu-item-mdisplay-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-mdisplay" name="menu-item-mdisplay[<?php echo esc_attr($item_id); ?>]">
                            <option value="" <?php selected( $item->mdisplay, '' ); ?>><?php esc_html_e( 'All (label & icon)', 'glimmer' ); ?></option>
                            <option value="icon" <?php selected( $item->mdisplay, 'icon' ); ?>><?php esc_html_e( 'Icon Only', 'glimmer' ); ?></option>
                            <option value="none" <?php selected( $item->mdisplay, 'none' ); ?>><?php esc_html_e( 'None (hide icon and label)', 'glimmer' ); ?></option>
                        </select>
                    </label>
                </p>
                            
                <?php
                /*
                 * Custom field added end here
                 */
                ?>
                <?php 
                    // This is the added section
                    do_action( 'wp_nav_menu_item_custom_fields', $item_id, $item, $depth, $args );
                    // end added section 
                ?>
                

                <p class="field-move hide-if-no-js description description-wide">
                    <label>
                        <span><?php esc_html_e( 'Move', 'glimmer' ); ?></span>
                        <a href="#" class="menus-move menus-move-up" data-dir="up"><?php esc_html_e( 'Up one', 'glimmer' ); ?></a>
                        <a href="#" class="menus-move menus-move-down" data-dir="down"><?php esc_html_e( 'Down one', 'glimmer' ); ?></a>
                        <a href="#" class="menus-move menus-move-left" data-dir="left"></a>
                        <a href="#" class="menus-move menus-move-right" data-dir="right"></a>
                        <a href="#" class="menus-move menus-move-top" data-dir="top"><?php esc_html_e( 'To the top', 'glimmer' ); ?></a>
                    </label>
                </p>

                <div class="menu-item-actions description-wide submitbox">
                    <?php if ( 'custom' != $item->type && $original_title !== false ) : ?>
                        <p class="link-to-original">
                            <?php printf( __('Original: ', 'glimmer').'%s', '<a href="' . esc_attr( $item->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?>
                        </p>
                    <?php endif; ?>
                    <a class="item-delete submitdelete deletion" id="delete-<?php echo esc_attr($item_id); ?>" href="<?php
                    echo wp_nonce_url(
                        add_query_arg(
                            array(
                                'action' => 'delete-menu-item',
                                'menu-item' => $item_id,
                            ),
                            admin_url( 'nav-menus.php' )
                        ),
                        'delete-menu_item_' . $item_id
                    ); ?>"><?php esc_html_e( 'Remove', 'glimmer' ); ?></a> <span class="meta-sep hide-if-no-js"> | </span> <a class="item-cancel submitcancel hide-if-no-js" id="cancel-<?php echo esc_attr($item_id); ?>" href="<?php echo esc_url( add_query_arg( array( 'edit-menu-item' => $item_id, 'cancel' => time() ), admin_url( 'nav-menus.php' ) ) );
                        ?>#menu-item-settings-<?php echo esc_attr($item_id); ?>"><?php esc_html_e('Cancel', 'glimmer'); ?></a>
                </div>

                <input class="menu-item-data-db-id" type="hidden" name="menu-item-db-id[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr($item_id); ?>" />
                <input class="menu-item-data-object-id" type="hidden" name="menu-item-object-id[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->object_id ); ?>" />
                <input class="menu-item-data-object" type="hidden" name="menu-item-object[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->object ); ?>" />
                <input class="menu-item-data-parent-id" type="hidden" name="menu-item-parent-id[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->menu_item_parent ); ?>" />
                <input class="menu-item-data-position" type="hidden" name="menu-item-position[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->menu_order ); ?>" />
                <input class="menu-item-data-type" type="hidden" name="menu-item-type[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->type ); ?>" />
            </div><!-- .menu-item-settings-->
            <ul class="menu-item-transport"></ul>
        <?php
        $output .= ob_get_clean();
    }

} // Walker_Nav_Menu_Edit

/**
 * Custom Walker
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
class Sh_Custom_Walker extends Walker_Nav_Menu
{
    var $columns = 0;
    var $max_columns = 0;
    var $rows = 1;
    var $rowsCount = array();
    private $in_sub_menu = 0;
    
   /**
     * @see Walker::start_lvl()
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int $depth Depth of page. Used for padding.
     */
    function start_lvl(&$output, $depth = 0, $args = array()) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"sub-menu {locate_class}\">\n";
    }
    
    /**
     * @see Walker::end_lvl()
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int $depth Depth of page. Used for padding.
     */
    function end_lvl(&$output, $depth = 0, $args = array()) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
        
        if ($depth === 0) {
            if ($this->sh_mega == 'mega') {
                $output = str_replace("{locate_class}", "sh_mega_wrap sh_mega_col_".$this->max_columns."", $output);
                
                foreach($this->rowsCount as $row => $columns) {
                    $output = str_replace("{current_row_".$row."}", "sh_megamenu_columns_".$columns, $output);
                }
                
                $this->columns = 0;
                $this->max_columns = 0;
                $this->rowsCount = array();
            } else {
                $output = str_replace("{locate_class}", "", $output);
            }
        }
    }    
    
    function start_el(&$output, $item, $depth = 0, $args = Array(), $id = 0) {
        global $wp_query;

        // Detect first child of submenu then add class active
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
        $class_names = $value = '';
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $mega_class ='';
        $menu_icon = '';
        if ($depth === 0 && $item->mtype === 'mega') {
            $mega_class = ' sh_mega';
        } elseif ($depth === 0 && $item->mtype === 'cats') {
            $mega_class = ' sh_mega_cats';
        } elseif ($depth === 0 && $item->mtype === 'custom') {
            $mega_class = ' sh_mega menu-item-has-children';
        } elseif ($depth === 0 && $item->mtype =='cats_column') { 
            $mega_class = ' sh_mega menu-item-has-children';
        } else {
            $mega_class = ' sh_default_menu_item';
        }
        
        if ($depth === 1 && $this->sh_mega === 'mega') {
            $mega_class = ' mega_column mega_col_title';
        }
        
        $icon_class = '';
        if ($item->mdisplay == 'icon') {
            $icon_class = ' menu-item-iconsOnly';
        }
        
        if ( $depth == 1 ) {
            if ( ! $this->in_sub_menu ) {
                $mega_class .= ' active'; 
                $this->in_sub_menu = 1;
            }
        }

        if ( $depth == 0 ) {
            $this->in_sub_menu = 0;
        } // End addition of active class for first item 

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="'. esc_attr( $class_names.$mega_class.$icon_class." menu-item-depth-".$depth  ) . '"';
        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

        $prepend = '';
        $append = '';
        $description  = '';
        if ($depth != 0) {
            $description = $append = $prepend = "";
        }
        $menu_color = '';
        if ($depth === 0) {   
            $this->sh_mega = get_post_meta( $item->ID, '_menu_item_mtype', true);
            $menu_color  = '<span class="menu_bl" style="background:'.esc_attr( $item->mcolor ).';"></span>';
        }

        if ($depth === 1 && $this->sh_mega === 'mega') {
            $this->columns ++;
            $this->rowsCount[$this->rows] = $this->columns;
            
            if ($this->max_columns < $this->columns) $this->max_columns = $this->columns;
            
            $title = apply_filters( 'the_title', $item->title, $item->ID );

            if ($title != "-" && $title != '"-"') {
                //display
                if ($item->mdisplay == 'icon') {
        		    if (!empty( $item->micon )) {
            			if (0 === strpos($item->micon, 'http')) {
            			    $menu_icon = '<i class="icon_only img_icon" style="background-image: url('.esc_attr( $item->micon ).')"></i>';
            			} else {
                                        $menu_icon  = '<i class="icon_only '.esc_attr( $item->micon ).'"></i>';
            			}
        		    }
                    $the_link = '<span class="icon_only_label">'.$args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append.$args->link_after.'</span>';
                } elseif ($item->mdisplay == 'none') {
                    $menu_icon  = '';
                    $the_link = '';
                } else {
        		    if (!empty( $item->micon )) {
            			if (0 === strpos($item->micon, 'http')) {
            			    $menu_icon = '<i class="img_icon" style="background-image: url('.esc_attr( $item->micon ).')"></i>';
            			} else {
                            $menu_icon  = '<i class="'.esc_attr( $item->micon ).'"></i>';
            			}
        		    }
                    $the_link = $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append.$args->link_after;
                }

                $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
                $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
                $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
                $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
                $item_output = $args->before;
                if ($item->mdisplay != 'none') {
                    $item_output .= '<a'. $attributes .'>';
                    $item_output .= $menu_icon.$the_link;
                    $item_output .= '</a>';
                }
                $item_output .= $args->after;
            }
            
            $column_class  = ' {current_row_'.$this->rows.'}';
            
            if ($this->columns == 1)
            {
                $column_class  .= " sh_mega_first_column";
            }
        } else {
            //display
            if ($item->mdisplay == 'icon') {
    		    if (!empty( $item->micon )) {
        			if (0 === strpos($item->micon, 'http')) {
        			    $menu_icon = '<i class="icon_only img_icon" style="background-image: url('.esc_attr( $item->micon ).')"></i>';
        			} else {
                        $menu_icon  = '<i class="icon_only '.esc_attr( $item->micon ).'"></i>';
        			}
    		    }
                $the_link = '<span class="icon_only_label">'.$args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append.$args->link_after.'</span>';
            } elseif ($item->mdisplay == 'none') {
                $menu_icon  = '';
                $the_link = '';
            } else {

    		    if (!empty( $item->micon )) {
        			if (0 === strpos($item->micon, 'http')) {
        			    $menu_icon = '<i class="img_icon" style="background-image: url('.esc_attr( $item->micon ).')"></i>';
        			} else {
                        $menu_icon  = '<i class="'.esc_attr( $item->micon ).'"></i>';
        			}
    		    }
                            
                if ($depth !== 0 && empty( $item->micon ) && $this->sh_mega === 'mega') {
                    if (is_rtl()) {
                        $menu_icon = '<i class="enotype-icon-arrow-left6 mega_menu_arrow_holder"></i>';
                    } else {
                        $menu_icon = '<i class="enotype-icon-arrow-right6 mega_menu_arrow_holder"></i>';
                    }
                }
                $the_link = $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append.$args->link_after;
            }
            
            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

            $item_output = $args->before;
            if ($item->mdisplay != 'none') {
                $item_output .= '<a'. $attributes .'>';
                $item_output .= $menu_icon.$the_link;
                $item_output .= '</a>';
            }
            $item_output .= $args->after;
        }
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

        if ( $depth == 0 ) {
            if ($item->mtype =='cats') {
                if ($item->mcats_layout == 'horz') {
                    $layout_class = 'sh_cats_horizontal';
                } else {
                    $layout_class = '';
                }
                $output .= "<div class='cats-mega-wrap ".$layout_class."'>\n";
                $output .= "<div class=\"cats-mega-inner\">\n";
            } 
        }
        if ($item->mtype =='custom') {
            $output .= "<div class='sh_custom_mega sh_mega_wrap'>\n";
        }
        if ($item->mtype =='cats_column') {
            $output .= "<div class='sh_cats_column sh_mega_wrap'>\n";
        }
    } // start el
    
    function end_el( &$output, $item, $depth = 0, $args = array() ) {
		if ($depth==0){
            if ($item->mtype =='cats') {
    			$output .= "<div class='subcat'>";
    		    for ($i=0; $i<count($item->children);$i++) {
        			$child = $item->children[$i];
        			$output .="<div class='".(($i===0)?'active':'')." sh-cat-latest' id='mn-latest-".$child->ID."' data-id='".$child->object_id."' data-object='".$item->object."' data-layout='".$item->mcats_layout."'>";			$output .="<ul id='mn-ul-latest-".$child->ID."'>";
        			if ($i == 0) {
        				$output .= sh_mega_menu_cats_loop ($item->object, $item->mcats_layout, $child->object_id);
        			}
        			$output .= "</ul>";
        			$output .= "<a href='".$child->url."' title='".$child->attr_title."' class='view_all_posts'>".esc_html__('View all', 'glimmer')."<i class='long-arrow-icon'></i></a>";
        			$output .= "</div>";
    			}
    			$output .= "</div> \n</div>\n</div>\n";
            }
		} else {}
        if ($depth == 0 && $item->mtype =='custom') {
            $output .= do_shortcode($item->mcustom);
            $output .= "</div>\n";
        }
        if ($depth == 0 && $item->mtype =='cats_column') {
            ob_start(); ?>
            <div class="megacat cat_full">
                <div class="megamenu-content">
                    <div class="cat-wrap">
                    <?php 
                    $category_column = $item->mcats_column_num;
                    $cat_id = $item->object_id;

                    $wp_query = new WP_Query(
                        array(
                            'category__in' => $cat_id,
                            'posts_per_page' => $category_column,
                        ) 
                    );
                    
                    if ( $wp_query->have_posts() ) : 
                    $glimmer_theme_post_i = 1;      
                    $max = $wp_query->post_count;

                    if ( $category_column == "2" ) {
                        $post_in_row = 2;
                        $column_grid_class = "col-md-6";
                    } elseif ( $category_column == "3" ) {
                        $post_in_row = 3;
                        $column_grid_class = "col-md-4";
                    } elseif ( $category_column == "4" ) {
                        $post_in_row = 4;
                        $column_grid_class = "col-md-3";
                    }

                    while ( $wp_query->have_posts() ) : $wp_query->the_post(); 

                    if ( $glimmer_theme_post_i % $post_in_row == 1 && $post_in_row != 1 ) {
                        echo "<ul class='row'>";
                    }           
                    ?>
                    <li class="<?php echo esc_attr($column_grid_class);  ?>">
                        <div class="post-thumb bg-image">
                            <?php
                            if ( has_post_thumbnail() ) { ?>
                                    <a href="<?php the_permalink(); ?>">
                                        <?php
                                            the_post_thumbnail('shopper-mega-menu-cat-column', array( 'alt' => get_the_title()));
                                        ?>
                                    </a>
                            <?php } else { ?>
                                <a href="<?php the_permalink(); ?>">
                                   <img class="no-thumb" src="<?php echo get_template_directory_uri(); ?>/inc/mega-menu/images/nopreview-mega-menu.png; ?>" alt="<?php esc_attr_e('No Preview', 'glimmer' ); ?>" />
                                </a>
                            <?php } //end else ?>
                        </div>
                        <div class="post-content">
                            <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                            <span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
                        </div>
                    </li>
                    <?php
                    if ( $glimmer_theme_post_i % $post_in_row == 0 && $post_in_row !=1 ) {
                        echo "</ul><!-- /.row-->";
                    } elseif( $glimmer_theme_post_i == $max && $max % $post_in_row != 0 && $post_in_row != 1 ) {
                        echo "</ul><!-- /.row-->";
                    }
                    $glimmer_theme_post_i++;
                    endwhile; endif; ?>
                        <div class="view-all">
                            <a href="<?php echo esc_url( get_category_link( $cat_id ) ); ?>"><?php esc_html_e( 'View All', 'glimmer' ); ?> <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>                                                        
                </div>
            </div>
            <?php   
            $output .= ob_get_clean();
            $output .= "</div>\n";
        }
        if ($item->children || $item->mtype =='custom') {
	      $output .= "<i class='responsive-caret'></i>\n";
        }
		$output .= "</li>\n";
	} // end end_el()

    /*
        Menu call back function
    */
    public static function fallback_top_menu( $args ) {
        if ( current_user_can( 'manage_options' ) ) {
        echo '<ul class="main-menu top-menu">';
        echo '<li class="active"><a href="' . admin_url( 'nav-menus.php' ) . '">' . esc_html__('Add a menu', 'glimmer') . '</a></li>';
        echo '</ul>';
        }
    }

    public static function fallback_main_menu( $args ) {
        if ( current_user_can( 'manage_options' ) ) {
        echo '<ul class="main-menu">';
        echo '<li class="active"><a href="' . admin_url( 'nav-menus.php' ) . '">' . esc_html__('Add a menu', 'glimmer') . '</a></li>';
        echo '</ul>';
        }
    }
} //end of walker class

add_filter( 'wp_nav_menu_objects', 'add_menu_child_items' );
function add_menu_child_items( $items ) {
	
	$parents = array();
	foreach ( $items as $item ) {
		$item->children = array();
		if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
			$parents[] = $item->menu_item_parent;
		}
	}
	
	foreach ( $items as $item ) {
		if ( in_array( $item->ID, $parents ) ) {
			$item->classes[] = 'menu-parent-item'; 
	
			foreach ( $items as $citem ) {
				if ( $citem->menu_item_parent && $citem->menu_item_parent == $item->ID ) {
					$item->children[] = $citem;
				}
			}
		}
	}
	return $items;    
}


// Ajax categories
add_action( 'wp_ajax_mmcl', 'sh_mega_menu_cats_loop' );  
add_action( 'wp_ajax_nopriv_mmcl', 'sh_mega_menu_cats_loop' );

function sh_mega_menu_cats_loop ($object = '', $layout = '', $id = '') {
    global $post;
    if ($object == '') { $object = $_POST['object']; }
    if ($layout == '') {$layout = isset($_POST['layout']) ? $_POST['layout']: '';}
    if ($id =='') {$id = $_POST['id'];}
    
	if ($layout == 'horz') {
	    $post_count = 3;
	    $sep = '';
	    $img_size = 'shopper-mega-menu-cat-menu-horizontal';
	    $imgw = 117;
	    $imgh = 85;
	} else {
	    $post_count = 4;
	    $sep = '-';
	    $img_size = 'shopper-mega-menu-cat-menu-vertical';
	    $imgw = 70;
	    $imgh = 55;
	}

    $output = '';
    $ajax_cats_posts = new WP_Query( apply_filters( 'widget_posts_args', 
	array( 
	    'posts_per_page'    => $post_count, 
	    'no_found_rows'         => true,
	    'cache_results' => false, 
	    'post_status'           => 'publish',
	    'post_type' => 'post', 
	    'cat'              =>      $id
	) ) );

        if ($ajax_cats_posts->have_posts()) :
		    while ( $ajax_cats_posts->have_posts() ) {
			    $ajax_cats_posts->the_post();
                //Get the Thumbnail URL
                $img_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $img_size, false);
				$output.= "<li ";
				$output.= "><div class='subcat-thumbnail'>";
                if ($img_src) {
                $output .= "<a href='".get_permalink()."' title='".get_the_title()."'><img src='".$img_src[0]."' alt='".get_the_title()."' ></a>";
                } else {
                $output .= "<a href='".get_permalink()."' title='".get_the_title()."'><img src='".get_template_directory_uri()."/inc/mega-menu/images/nopreview-mega-menu.png' alt='".get_the_title()."' ></a>";
                }
                $output .= "</div><div class='subcat-title'><a href='".get_permalink()."' title='".get_the_title()."'> ".get_the_title()."</a><span> ".$sep." ". human_time_diff( get_the_time('U'), current_time('timestamp') ) ." " . esc_html__('ago', 'glimmer'). "</span></div></li>";
			} 
		    // Reset the global $the_post as this query will have stomped on it
		    wp_reset_postdata();

	    endif;
    if (isset($_POST['id'])) {	    
        echo wp_kses_post( $output );
    } else {
	    return $output;
    }
  
    if (isset($_POST['id'])) {
    exit();
    }
} //sh_mega_menu_cats_loop
