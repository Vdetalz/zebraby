<?php 
/* Sidebar Layout Condition  */
if ( ! function_exists( 'glimmer_theme_sidebar_layout' ) ) :
    function glimmer_theme_sidebar_layout() {
        ( isset($_GET["layout"]) ) ? $layout = $_GET["layout"]  : $layout = "";

        if ( $layout != "") {
            return $layout;
        } else {
            if ( is_archive() || is_search() ) {
                return softhopper_glimmer('sidebar_layout_archive');
            } else {
                return softhopper_glimmer('sidebar_layout');
            }
        }
    }
endif;

if ( ! function_exists( 'glimmer_theme_check_list_grid' ) ) :
    function glimmer_theme_check_list_grid() {
        global $softhopper_glimmer;
        // check list or grid post layout
        if ( !is_single() ) {
            if( is_archive() || is_search() ) {
                $post_layout = ( $softhopper_glimmer['post_layout_archive'] == 'list' ) ? 'col-md-12 full-width' : 'col-md-'.softhopper_glimmer('grid_column_archive').' col-sm-6 grid';
            } else {
                $post_layout = ( $softhopper_glimmer['post_layout'] == 'list' ) ? 'col-md-12 full-width' : 'col-md-'.softhopper_glimmer('grid_column').' col-sm-6 grid';
            }
        } else {
            $post_layout = 'col-md-12 full-width';
        }

        if ( is_sticky() && !is_paged() ) {
            if ( is_archive() || is_search() ) {
                $post_layout = ( $softhopper_glimmer['post_layout_archive'] == 'list' ) ? 'col-md-12 full-width' : 'col-md-'.softhopper_glimmer('grid_column_archive').' col-sm-6 grid';
            } else {
                if (big_sticky_post() == false) {
                    $post_layout = 'col-md-'.softhopper_glimmer('grid_column').' col-sm-6 grid';
                } else {
                    $post_layout = 'col-md-12 full-width';
                }
            }
        }
        return $post_layout;
    }
endif;

if ( ! function_exists( 'big_sticky_post' ) ) :
    function big_sticky_post() {
        global $softhopper_glimmer;
        ( isset($_GET["big_sticky_post"]) ) ? $big_sticky_post = $_GET["big_sticky_post"]  : $big_sticky_post = "";

        if ( $big_sticky_post == "hide") { 
            return false;
        } else {
            if (softhopper_glimmer('post_layout') == "grid" && softhopper_glimmer('big_sticky_post') == 0) {
                return false;
            } else {
                return true;
            }
        }
    }
endif;

if ( ! function_exists( 'glimmer_theme_full_width_column' ) ) :
    function glimmer_theme_full_width_column() {
        global $softhopper_glimmer;
        if ( is_archive() || is_search() ) {
            return softhopper_glimmer('sidebar_layout_archive_full_width_column');
        } elseif ( is_single() ) {
            return softhopper_glimmer('sidebar_layout_single_full_width_column');
        } elseif ( is_home() ) {
            return softhopper_glimmer('sidebar_layout_full_width_column');
        } else {
            return softhopper_glimmer('sidebar_layout_page_full_width_column');
        }
    }
endif;

/* archive or search page title bar  */
if ( ! function_exists( 'glimmer_theme_archive_search_page_title' ) ) :
    function glimmer_theme_archive_search_page_title() {
        if ( is_archive() || is_search() ) { ?>
            <div class="container">
                <div class="row">
                <?php
                    /* Show breadcrumbs with condition */
                    $columns_grid = 12;
                    $columns_offset  = '';
                    if ( softhopper_glimmer('sidebar_layout_archive') == 1 ) {
                        $columns_grid = 10;
                        $columns_offset = 'col-md-offset-1';
                    }
                ?>
                <div class="<?php echo esc_attr( $columns_offset); ?> col-md-<?php echo esc_attr( $columns_grid); ?>">
                    <?php
                        if ( is_archive() ) { ?>
                            <div class="archive-page-title">
                                <div class="title-content">
                                    <?php glimmer_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
                                </div>
                            </div> <!-- /.archive-page-title -->
                        <?php } elseif ( is_search() ) { ?>
                            <header class="page-header">
                               <h1 class="page-title"><?php printf( '<span>'. esc_html__( 'Search Results for:', 'glimmer' ).'</span>%s', get_search_query() ); ?></h1>

                               <?php if ( softhopper_glimmer('search_form_in_search_page') == 1 ) : ?>
                                   <div class="search">
                                       <?php get_search_form(); ?>
                                   </div> <!-- /.search-form -->
                               <?php endif; ?>
                           </header><!-- .page-header -->
                            <?php
                        }
                    ?>
                </div> <!-- /.col-md-10 -->
                </div> <!-- /.row -->
            </div> <!-- /.container --> 
        <?php }
    }
endif;