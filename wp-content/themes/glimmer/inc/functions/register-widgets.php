<?php
/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function glimmer_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Home Page', 'glimmer' ),
		'id'            => 'sidebar-home-page',
		'description'   => 'This sidebar is only for home page',
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title-area"><h5 class="widget-title"><span>',
		'after_title'   => '</span></h5></div>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Default', 'glimmer' ),
		'id'            => 'sidebar-default',
		'description'   => 'This sidebar is for single post, single page, archive page and others pages.',
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title-area"><h5 class="widget-title"><span>',
		'after_title'   => '</span></h5></div>',
	) );


	// Register footer sidebars
	if ( softhopper_glimmer('footer_widgets_bottom') ) :
		if ( softhopper_glimmer('footer_widgets_bottom_columns') != 1 ) {
			register_sidebars( softhopper_glimmer('footer_widgets_bottom_columns'), array(
				'name'          => esc_html__( 'Footer Bottom %d', 'glimmer' ),
				'id'            => 'footer-bottom-sidebar',
				'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<div class="widget-title-area"><h5 class="widget-title"><span>',
				'after_title'   => '</span></h5></div>',
			) );
		} else {
			register_sidebar(  array(
				'name'          => esc_html__( 'Footer Bottom', 'glimmer' ),
				'id'            => 'footer-bottom-sidebar',
				'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<div class="widget-title-area"><h5 class="widget-title"><span>',
				'after_title'   => '</span></h5></div>',
			) );
		}
	endif;
}
add_action( 'widgets_init', 'glimmer_widgets_init' );