<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Glimmer
 */

if ( ! function_exists( 'the_posts_navigation' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 */
function the_posts_navigation() {
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}
	?>
	<nav class="posts-navigation">
		<h2 class="screen-reader-text"><?php esc_html_e( 'Posts navigation', 'glimmer' ); ?></h2>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( esc_html__( 'Older posts', 'glimmer' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( esc_html__( 'Newer posts', 'glimmer' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'the_post_navigation' ) ) :
/**
 * Display navigation to next/previous post when applicable.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 */
function the_post_navigation() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}
	?>
	<nav class="post-navigation">
		<h2 class="screen-reader-text"><?php esc_html_e( 'Post navigation', 'glimmer' ); ?></h2>
		<div class="nav-links">
			<?php
				previous_post_link( '<div class="nav-previous">%link</div>', '%title' );
				next_post_link( '<div class="nav-next">%link</div>', '%title' );
			?>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'glimmer_posts_pagination_nav' ) ) :
/**
 * This is for post pagination
 */
function glimmer_posts_pagination_nav($wp_query = '', $custom_class = '', $page_url = false) {
    if(!$wp_query) {
        $wp_query = $GLOBALS['wp_query'];
    }

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 ) {
        return;
    }

    if($page_url == false) {
        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    } else {
        $paged = ( isset($_GET['paged']) ? sanitize_text_field( wp_unslash( $_GET['paged'] ) ) : 1 );
    }

    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="paging-navigation"><ul class="nav-links">' . "\n";

    /** Previous Post Link */
    if ( get_previous_posts_link() ) {
        printf( '<li class="nav-previous">%s</li>' . "\n", get_previous_posts_link('<span class="fa fa-angle-double-left"></span>'.esc_html__('Previous','glimmer') ) );
    } else {
    	?>
    	<li class="nav-previous disabled">
            <a href="#"><span class="fa fa-angle-double-left"></span><?php esc_html_e(' No Post','glimmer'); ?></a>
        </li>
    	<?php
    }


    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a class="page-numbers" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li><span class="page-numbers dots">&#46;&#46;&#46;</span></li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a class="page-numbers" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li><span class="page-numbers dots">&hellip;</span></li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a class="page-numbers curent" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( get_next_posts_link() ) {
        printf( '<li class="nav-next">%s</li>' . "\n", get_next_posts_link( esc_html__('Next','glimmer').'<span class="fa fa-angle-double-right"></span>') );
    } else {
    	?>
    	<li class="nav-next disabled">
            <a href="#"><?php esc_html_e('No Post','glimmer'); ?><span class="fa fa-angle-double-right"></span>
            </a>
        </li>
    	<?php
    }

    echo '</ul></div>' . "\n";
}
endif;

if ( ! function_exists( 'glimmer_post_format_icon' ) ) :
/**
 * Prints post format icon
 */
function glimmer_post_format_icon( $no_thumb = false ) {
    $meta = get_post_meta( get_the_ID() );
    $post_format = get_post_format();
    if ( false === $post_format ) {
        $post_format = "standard";
    }
    ob_start(); ?>
                                                         
    <?php
    if ( is_sticky() ) {
        echo '<div class="post-format"><i class="dashicons dashicons-format-standard"></i></div>';
    } ?>
    <?php
    $post_format_icon = ob_get_clean();
    if ( $post_format != "quote" ) {
        if ($no_thumb == true) {
            if ( softhopper_glimmer('post_format_icon') ) {
                echo wp_kses_post( $post_format_icon );
            }
        } else {
            if ( has_post_thumbnail() ) {
                if ( softhopper_glimmer('post_format_icon') ) {
                    echo wp_kses_post( $post_format_icon );
                }
            }
        }
    } 
}
endif;


if ( ! function_exists( 'glimmer_cropping_image_size' ) ) :
/**
 * To show cropping image size
 */
function glimmer_cropping_image_size() {
    global $softhopper_glimmer;
    global $image_size;
    if ( is_archive() || is_search() ) {
        if ($softhopper_glimmer['sidebar_layout_archive'] == 'full-width') {
           $image_size = ( $softhopper_glimmer['post_layout_archive'] == 'list' ) ? 'glimmer-single-full' : 'glimmer-single-grid-full';
        } else {
           $image_size = ( $softhopper_glimmer['post_layout_archive'] == 'list' ) ? 'glimmer-single-list' : 'glimmer-single-grid';
        }  
    } else {
        if ($softhopper_glimmer['sidebar_layout'] == 'full-width') {
           $image_size = ( $softhopper_glimmer['post_layout'] == 'list' ) ? 'glimmer-single-full' : 'glimmer-single-grid-full';
        } else {
          $image_size = ( $softhopper_glimmer['post_layout'] == 'list' ) ? 'glimmer-single-list' : 'glimmer-single-grid';
        } 
    }

    // this is for showing demo by url id
    if ( !is_archive() ) {
       ( isset($_GET["layout"]) ) ? $layout = $_GET["layout"]  : $layout = "";

       if ( $layout == "full-width"  ) {
           $image_size = ( $softhopper_glimmer['post_layout'] == 'list' ) ? 'glimmer-single-full' : 'glimmer-single-grid-full';
       } elseif ($layout == "sidebar-left" || $layout == "sidebar-right") {
           $image_size = ( $softhopper_glimmer['post_layout'] == 'list' ) ? 'glimmer-single-list' : 'glimmer-single-grid';
       } 
    }

    if (is_sticky()) {
        if ( is_archive() ) { 
            $image_size = ( $softhopper_glimmer['sidebar_layout_archive'] == 'full-width' ) ? 'glimmer-single-full' : 'glimmer-single-list';
        } else {
            $image_size = ( $layout == "full-width" || $softhopper_glimmer['sidebar_layout'] == 'full-width' ) ? 'glimmer-single-full' : 'glimmer-single-list';
        }
    }
}
endif;

if ( ! function_exists( 'glimmer_tag_list' ) ) :
/**
 * Prints post format icon
 */
function glimmer_tag_list() {
    if ( has_tag() ) :
        ?>
        <div class="tag clearfix">
            <span class="tags"><?php esc_html_e('Tagged In:', 'glimmer'); ?></span>
            <?php 
            echo get_the_tag_list("", "", "");
            ?>
        </div> <!-- /.tag -->
        <?php
    endif;
}
endif;

if ( ! function_exists( 'glimmer_entry_header' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function glimmer_entry_header() {
    global $post;
    $meta = get_post_meta( $post->ID );
    glimmer_post_format_icon(); 
    if ( is_single() ) {
        if ( isset($meta["_glimmer_post_title"][0]) ) {
            $post_title = ($meta["_glimmer_post_title"][0] == 'show') ? true : false ;
        } elseif( softhopper_glimmer('post_title') !== null ) {
            $post_title = ( softhopper_glimmer('post_title') == true) ? true : false ;
        } else {
            $post_title = true;
        }
        if ($post_title == true) {
            the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); 
        }        
    } else {
        the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 
    }
    ob_start();
    ?>
    <div class="entry-meta">
        <?php if( is_archive() || is_search() ) {
            $header_layout_list = ( softhopper_glimmer('post_layout_archive') == 'list' ) ? true : false;
        } else {
            $header_layout_list = ( softhopper_glimmer('post_layout') == "list"  ) ? true : false;
        } ?>

        <?php if( $header_layout_list == true ) { ?>
        <?php
            if ( 'post' == get_post_type() ) { ?>
            <span class="cat-links">
                <?php esc_html_e('In: ','glimmer').the_category( ', ' ); ?>
            </span>
            <span class="devider">/</span>
            <?php } ?>
            <div class="entry-date">
                <?php the_time( get_option( 'date_format' ) ); ?>
            </div>
            <span class="devider">/</span>
            <span class="byline">
                <span class="author vcard">
                    <?php esc_html_e('By: ','glimmer').the_author_posts_link(); ?>
                </span>
            </span>
        <?php } else { ?>
            <?php
                if ( 'post' == get_post_type() ) { ?>
                <span class="cat-links">
                    <?php esc_html_e('In: ','glimmer').the_category( ', ' ); ?>
                </span>
                <span class="devider">/</span>
            <?php } ?>
            <span class="byline">
                <span class="author vcard">
                    <?php esc_html_e('By: ','glimmer').the_author_posts_link(); ?>
                </span>
            </span>
        <?php } ?>
    </div> <!-- .entry-meta -->
    
    <?php
    $entry_meta = ob_get_clean();
    if ( is_single() ) {
        if ( isset($meta["_glimmer_post_cat_date_author_meta"][0]) ) {
            $post_cat_date_author_meta = ($meta["_glimmer_post_cat_date_author_meta"][0] == 'show') ? true : false ;
        } else {
            $post_cat_date_author_meta = ( softhopper_glimmer('post_cat_date_author_meta') == true) ? true : false ;
        }
        if ($post_cat_date_author_meta == true) {
            echo wp_kses_post( $entry_meta );
        }
    } else {
        echo wp_kses_post( $entry_meta );
    }
}
endif;

if ( ! function_exists( 'glimmer_page_entry_header' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function glimmer_page_entry_header() {
    global $post;
    $meta = get_post_meta( $post->ID );
    if ( isset($meta["_glimmer_page_title"][0]) ) {
        $page_title = ($meta["_glimmer_page_title"][0] == 'show') ? true : false ;
    } elseif( softhopper_glimmer('page_title') !== null ) {
        $page_title = ( softhopper_glimmer('page_title') == true) ? true : false ;
    } else {
        $page_title = true;
    }

    if ($page_title == true ) {
        the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); 
    }

    if ( isset($meta["_glimmer_page_author_date_meta"][0]) ) {
        $page_author_date_meta = ($meta["_glimmer_page_author_date_meta"][0] == 'show') ? true : false ;
    } else {
        $page_author_date_meta = ( softhopper_glimmer('page_author_date_meta') == true) ? true : false ;
    }
    if ($page_author_date_meta == true) { ?>
    <div class="entry-meta">        
        <span class="byline">
            <span class="author vcard">
                <?php esc_html_e('By: ','glimmer').the_author_posts_link(); ?>
            </span>
        </span>
        <span class="devider">/</span>
        <span class="entry-date">
            <?php the_time( get_option( 'date_format' ) ); ?>
        </span>
    </div> <!-- .entry-meta -->
    <?php
    }
}
endif;


if ( ! function_exists( 'glimmer_entry_footer' ) ) :
    /**
     * Prints HTML with meta information for the categories, tags and comments.
     */
    function glimmer_entry_footer() {
        global $post;
        $meta = get_post_meta( $post->ID ); ?>

        <?php if( is_archive() || is_search() ) {
            $footer_layout_list = ( softhopper_glimmer('post_layout_archive') == 'list' ) ? true : false;
        } else {
            $footer_layout_list = ( softhopper_glimmer('post_layout') == "list" || is_sticky() ) ? true : false;
        } ?>

        <?php if( $footer_layout_list == true ) { ?>
        <div class="footer-meta clearfix">  
            <div class="post-comment">
                <a href="<?php comments_link(); ?>" class="comments-link">
                    <span><?php comments_number( esc_html('0 Comment', 'glimmer'), esc_html('1 Comment', 'glimmer'), '% '.esc_html('Comments', 'glimmer') ); ?></span>
                </a>
            </div>

            <?php if( function_exists('glimmer_social_share_link') ) { ?>
            <?php glimmer_social_share_link(); ?> 
            <?php } ?>   

            <div class="post-view">
                <a href="<?php the_permalink(); ?>" class="view-link">
                    <span><?php echo softhopper_get_post_views( get_the_ID() ); ?></span>
                </a>
            </div>
        </div>
        <?php } else { ?>
            <div class="more-wraper">
                <a href="<?php the_permalink(); ?>" class="more-link"><?php esc_html_e('Continue', 'glimmer'); ?></a>   
            </div> 
            <?php if( function_exists('glimmer_social_share_link') ) { ?>               
            <div class="footer-meta">  
                <?php glimmer_social_share_link(); ?> 
            </div> <!-- /.footer-meta -->
            <?php } 
        } ?>
        <?php
    }
endif;


if ( ! function_exists( 'glimmer_entry_footer_single' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function glimmer_entry_footer_single() {
    if(  function_exists('glimmer_social_share_link') ) {    
    	// Hide category and tag text for pages.
    	if ( 'post' == get_post_type() ) { ?>
            <div class="footer-meta clearfix">  
                <div class="post-comment">
                    <a href="<?php comments_link(); ?>" class="comments-link">
                        <span><?php comments_number( esc_html('0 Comment', 'glimmer'), esc_html('1 Comment', 'glimmer'), '% '.esc_html('Comments', 'glimmer') ); ?></span>
                    </a>
                </div>

                <?php if( function_exists('glimmer_social_share_link') ) { ?>
                <?php glimmer_social_share_link(); ?> 
                <?php } ?>   

                <div class="post-view">
                    <a href="<?php the_permalink(); ?>" class="view-link">
                        <span><?php echo softhopper_get_post_views( get_the_ID() ); ?></span>
                    </a>
                </div>
            </div> 
    		<?php
    	} // end if post type
    }
}
endif;

if ( ! function_exists( 'glimmer_archive_title' ) ) :
/**
 * Shim for `glimmer_archive_title()`.
 *
 * Display the archive title based on the queried object.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 *
 * @param string $before Optional. Content to prepend to the title. Default empty.
 * @param string $after  Optional. Content to append to the title. Default empty.
 */
function glimmer_archive_title( $before = '', $after = '' ) {
    $allowed_html_array = array(
        'span' => array()
    );
	if ( is_category() ) {
		$title = sprintf( wp_kses( __( '<span>Browsing Category</span>', 'glimmer' ), $allowed_html_array ).esc_html__( '%s', 'glimmer' ), single_cat_title( '', false ) );
	} elseif ( is_tag() ) {
		$title = sprintf( wp_kses( __( '<span>Browsing Tag</span>', 'glimmer' ), $allowed_html_array ).esc_html__( '%s', 'glimmer' ), single_tag_title( '', false ) );
	} elseif ( is_author() ) {
		$title = sprintf( wp_kses( __( '<span>Browsing Author</span>', 'glimmer' ), $allowed_html_array ).esc_html__( '%s', 'glimmer' ), '<span class="vcard">' . get_the_author() . '</span>' );
	} elseif ( is_year() ) {
		$title = sprintf( wp_kses( __( '<span>Browsing Year</span>', 'glimmer' ), $allowed_html_array ).esc_html__( '%s', 'glimmer' ), get_the_date( esc_html_x( 'Y', 'yearly archives date format', 'glimmer' ) ) );
	} elseif ( is_month() ) {
		$title = sprintf( wp_kses( __( '<span>Browsing Month</span>', 'glimmer' ), $allowed_html_array ).esc_html__( '%s', 'glimmer' ), get_the_date( esc_html_x( 'F Y', 'monthly archives date format', 'glimmer' ) ) );
	} elseif ( is_day() ) {
		$title = sprintf( wp_kses( __( '<span>Browsing Day</span>', 'glimmer' ), $allowed_html_array ).esc_html__( '%s', 'glimmer' ), get_the_date( esc_html_x( 'F j, Y', 'daily archives date format', 'glimmer' ) ) );
	} elseif ( is_tax( 'post_format' ) ) {
		if ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = wp_kses( __( '<span>Browsing Post Format</span>', 'glimmer' ), $allowed_html_array ).esc_html_x( 'Aside', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = wp_kses( __( '<span>Browsing Post Format</span>', 'glimmer' ), $allowed_html_array ).esc_html_x( 'Gallery', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = wp_kses( __( '<span>Browsing Post Format</span>', 'glimmer' ), $allowed_html_array ).esc_html_x( 'Image', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = wp_kses( __( '<span>Browsing Post Format</span>', 'glimmer' ), $allowed_html_array ).esc_html_x( 'Video', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = wp_kses( __( '<span>Browsing Post Format</span>', 'glimmer' ), $allowed_html_array ).esc_html_x( 'Quote', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = wp_kses( __( '<span>Browsing Post Format</span>', 'glimmer' ), $allowed_html_array ).esc_html_x( 'Link', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = wp_kses( __( '<span>Browsing Post Format</span>', 'glimmer' ), $allowed_html_array ).esc_html_x( 'Status', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = wp_kses( __( '<span>Browsing Post Format</span>', 'glimmer' ), $allowed_html_array ).esc_html_x( 'Audio', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = wp_kses( __( '<span>Browsing Post Format</span>', 'glimmer' ), $allowed_html_array ).esc_html_x( 'Chat', 'post format archive title', 'glimmer' );
		}
	} elseif ( is_post_type_archive() ) {
		$title = sprintf( __( '<span>Browsing Archives</span>', 'glimmer' ).esc_html__( '%s', 'glimmer' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
		$title = sprintf( esc_html__( '%1$s: %2$s', 'glimmer' ), $tax->labels->singular_name, single_term_title( '', false ) );
	} else {
		$title = esc_html__( 'Browsing Archives', 'glimmer' );
	}

	/**
	 * Filter the archive title.
	 *
	 * @param string $title Archive title to be displayed.
	 */
	$title = apply_filters( 'get_the_archive_title', $title );

	if ( ! empty( $title ) ) {
		echo wp_kses_post( $before ) . wp_kses_post( $title ) .  wp_kses_post( $after );  // WPCS: XSS OK
	}
}
endif;

if ( ! function_exists( 'the_archive_description' ) ) :
/**
 * Shim for `the_archive_description()`.
 *
 * Display category, tag, or term description.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 *
 * @param string $before Optional. Content to prepend to the description. Default empty.
 * @param string $after  Optional. Content to append to the description. Default empty.
 */
function the_archive_description( $before = '', $after = '' ) {
	$description = apply_filters( 'get_the_archive_description', term_description() );

	if ( ! empty( $description ) ) {
		/**
		 * Filter the archive description.
		 *
		 * @see term_description()
		 *
		 * @param string $description Archive description to be displayed.
		 */
        echo wp_kses_post( $before ) . wp_kses_post( $description ) .  wp_kses_post( $after );
	}
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function glimmer_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'glimmer_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'glimmer_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so glimmer_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so glimmer_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in glimmer_categorized_blog.
 */
function glimmer_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'glimmer_categories' );
}
add_action( 'edit_category', 'glimmer_category_transient_flusher' );
add_action( 'save_post',     'glimmer_category_transient_flusher' );
