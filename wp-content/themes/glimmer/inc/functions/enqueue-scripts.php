<?php
/**
 * Enqueue scripts and styles.
 */
function glimmer_scripts() {
	global $wp_scripts, $softhopper_glimmer;
	$protocol = is_ssl() ? 'https' : 'http';
	// enqueue style
	if( !class_exists( 'Redux' ) ) {
		wp_enqueue_style('glimmer-google-font', glimmer_google_fonts_url() );
	}
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_style('glimmer-font-awesome', GLIMMER_TEMPLATE_DIR_URL . "/fonts/font-awsome/css/font-awesome.min.css");
	wp_enqueue_style('glimmer-bootstrap', GLIMMER_TEMPLATE_DIR_URL . "/lib/bootstrap/css/bootstrap.min.css");
	wp_enqueue_style('glimmer-glimmer-shape', GLIMMER_TEMPLATE_DIR_URL . "/fonts/glimmer/glimmer-shape.css");
	wp_enqueue_style('glimmer-owl-carousel', GLIMMER_TEMPLATE_DIR_URL . "/css/owl.carousel.css");
	wp_enqueue_style('glimmer-magnific-popup', GLIMMER_TEMPLATE_DIR_URL . "/lib/magnific-popup/magnific-popup.min.css");
	wp_enqueue_style('glimmer-justified-gallery', GLIMMER_TEMPLATE_DIR_URL . "/lib/justifiedgallery/justifiedGallery.min.css");
	wp_enqueue_style( 'glimmer-style', get_stylesheet_uri() );

	// load RTL
	(isset($_GET["rtl"])) ? $rtl_url = $_GET["rtl"] : $rtl_url = "";
	$rtl = ( $rtl_url == "true" ) ? true :  is_rtl();
	if ( $rtl ) {
		wp_enqueue_style('petty-theme-rtl', get_template_directory_uri() . "/rtl.css");
	}

	// enqueue scripts
	wp_enqueue_script('glimmer-bootstrap-js', GLIMMER_TEMPLATE_DIR_URL . "/lib/bootstrap/js/bootstrap.min.js", array("jquery"), false, true);
	wp_enqueue_script('glimmer-plugins-js', GLIMMER_TEMPLATE_DIR_URL . '/js/plugins.js', array("jquery"), false, true);
	wp_enqueue_script('glimmer-owl-carousel-js', GLIMMER_TEMPLATE_DIR_URL . '/js/owl.carousel.min.js', array("jquery"), false, true);
	wp_enqueue_script('isInViewport', GLIMMER_TEMPLATE_DIR_URL . '/js/isInViewport.jquery.min.js', array("jquery"), false, true);
	// load masonry from wordpress for grid layout
	wp_enqueue_script('masonry');
	
	//google map load only contact page
	if ( is_page_template( "contact-page.php" ) ) {
		$personal_api = softhopper_glimmer("google_map_api");
		$personal_api_key = (!empty($personal_api)) ? "?key=".$personal_api : "";
		wp_enqueue_script('glimmer-googleapis-js', "$protocol://maps.google.com/maps/api/js".$personal_api_key, array("jquery"), false, true);
		wp_enqueue_script('glimmer-gmaps-js', GLIMMER_TEMPLATE_DIR_URL . '/js/gmaps.min.js', array("jquery"), false, true);
	}
	wp_enqueue_script('glimmer-magnific-popup-js', GLIMMER_TEMPLATE_DIR_URL . '/lib/magnific-popup/jquery.magnific-popup.min.js', array("jquery"), false, true);
	wp_enqueue_script('glimmer-justified-gallery-js', GLIMMER_TEMPLATE_DIR_URL . '/lib/justifiedgallery/jquery.justifiedGallery.min.js', array("jquery"), false, true);
	wp_enqueue_script('glimmer-js', GLIMMER_TEMPLATE_DIR_URL . '/js/glimmer.js', array("jquery"), false, true);
	wp_enqueue_script( 'glimmer-skip-link-focus-fix', GLIMMER_TEMPLATE_DIR_URL . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	
	//localization glimmer js
	( $softhopper_glimmer['sidebar_layout'] == 1 ) ? $owl_item = 7 : $owl_item = 5;
    wp_localize_script("glimmer-js", "glimmer", array (
        	"lat" => $softhopper_glimmer['contact_lat'],
        	"lon" => $softhopper_glimmer['contact_lon'],
        	"map_mouse_wheel" => $softhopper_glimmer['map_mouse_wheel'],
        	"map_zoom_control" => $softhopper_glimmer['map_zoom_control'],
        	"map_point_img" => $softhopper_glimmer['contact_map_point_img']['url'],
        	"featured_post_auto_slide" => $softhopper_glimmer['featured_post_auto_slide'],
        	"featured_slide_speed" => $softhopper_glimmer['featured_slide_speed'],
        	"featured_autoplay_timeout" => $softhopper_glimmer['featured_autoplay_timeout'],
        	"tiled_gallery_row_height" => $softhopper_glimmer['tiled_gallery_row_height'],
        	"owl_item" => $owl_item,
        	"theme_uri" => GLIMMER_TEMPLATE_DIR_URL,
        	"check_rtl" => $rtl
    	)
    );
  

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'glimmer_scripts' );

// Remove Open Sans that WP adds from frontend
if (!function_exists('glimmer_remove_wp_open_sans')) :
function glimmer_remove_wp_open_sans() {
	wp_deregister_style( 'open-sans' );
	wp_register_style( 'open-sans', false );
}
add_action('wp_enqueue_scripts', 'glimmer_remove_wp_open_sans');
// Uncomment below to remove from admin
// add_action('admin_enqueue_scripts', 'glimmer_remove_wp_open_sans');
endif;

/**
 * Enqueue scripts and styles for WordPress admin panel.
 */
function glimmer_admin_panel_scripts($hook) {
	// enqueue style
	$protocol = is_ssl() ? 'https' : 'http';
	wp_enqueue_style('glimmer-admin-font-awesome', GLIMMER_TEMPLATE_DIR_URL . "/fonts/font-awsome/css/font-awesome.min.css");
	wp_enqueue_style('glimmer-admin-custom', GLIMMER_TEMPLATE_DIR_URL . "/css/backend/custom.css");
	wp_enqueue_style('glimmer-redux-custom', GLIMMER_TEMPLATE_DIR_URL . "/css/backend/redux-custom.css");	
	if( $hook == 'widgets.php' ) {
		wp_enqueue_style('thickbox');
	}

	// enqueue scripts
	wp_enqueue_script('glimmer-backend-js', GLIMMER_TEMPLATE_DIR_URL . '/js/backend/admin.js', array("jquery"), false, true);
	if( $hook == 'widgets.php' ) {		 
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_enqueue_script('glimmer-widget-js', GLIMMER_TEMPLATE_DIR_URL . '/js/backend/widget.js', null, null, true);
	}
}
add_action( 'admin_enqueue_scripts', 'glimmer_admin_panel_scripts' );