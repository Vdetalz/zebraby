<?php
/**
 * Sample implementation of the Custom Header feature
 * http://codex.wordpress.org/Custom_Headers
 *
 * You can add an optional custom header image to header.php like so ...
 *
 * @package Glimmer
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses glimmer_header_style()
 * @uses glimmer_admin_header_style()
 * @uses glimmer_admin_header_image()
 */
function glimmer_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'glimmer_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '000000',
		'width'                  => 1000,
		'height'                 => 250,
		'flex-height'            => true,
	) ) );
}
add_action( 'after_setup_theme', 'glimmer_custom_header_setup' );