<?php
/*
* This template for override wordpress some function to match the theme
*/

/**
 * Get excerpt
 *
 * @since 1.0
 */
if ( ! function_exists( 'glimmer_excerpt' ) ) {

    function glimmer_excerpt( $length = 30 ) {
        global $post;

        // Check for custom excerpt
        if ( has_excerpt( $post->ID ) ) {
            $output = $post->post_excerpt;
        }

        // No custom excerpt
        else {
            // Check for more tag and return content if it exists
            if ( strpos( $post->post_content, '<!--more-->' ) ) {
                $output = apply_filters( 'the_content', get_the_content() );
            }

            // No more tag defined
            else {
                $output = wp_trim_words( strip_shortcodes( $post->post_content ), $length );
            }

        }

        return $output;

    }

}  

// remove parentheses from category list and add span class to count
add_filter('wp_list_categories','glimmer_categories_postcount_filter');
function glimmer_categories_postcount_filter ($args) {
    $args = str_replace('(', '<span class="count"> ', $args);
    $args = str_replace(')', ' </span>', $args);
   return $args;
}

// remove parentheses from archive list and add span class to count
add_filter('get_archives_link', 'glimmer_archive_count_no_brackets');
function glimmer_archive_count_no_brackets($links) {
    $links = str_replace('</a>&nbsp;(', '</a> <span class="count">', $links);
    $links = str_replace(')', ' </span>', $links);
    return $links;
}

/* remove redux framework menu under the tools */
add_action( 'admin_menu', 'glimmer_remove_redux_menu', 12 );
function glimmer_remove_redux_menu() {
    remove_submenu_page('tools.php','redux-about');
}

// Support shortcodes in text widgets
add_filter( 'widget_text', 'do_shortcode' );

// custom post excerpt
function glimmer_custom_post_excerpt($string, $length, $dots = "&hellip;") {
    return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
}

if ( ! function_exists( 'glimmer_theme_get_file_name' ) ) :
function glimmer_theme_get_file_name( $file_path ){
    $get_file_name_path = explode("/", $file_path);
    return end($get_file_name_path);
}
endif;

if ( ! function_exists( 'glimmer_theme_var_template_include' ) ) :
// to get current template name
add_filter( 'template_include', 'glimmer_theme_var_template_include', 1000 );
function glimmer_theme_var_template_include( $t ) {
    $GLOBALS['current_theme_template'] = glimmer_theme_get_file_name($t);
    return $t;
}
endif;

// remove unnecessary p and br tag from shortcode
if( !function_exists('glimmer_fix_shortcodes') ) {
    function glimmer_fix_shortcodes($content){
        $array = array (
            '<p>[' => '[',
            ']</p>' => ']',
            ']<br />' => ']'
        );
        $content = strtr($content, $array);
        return $content;   
    }
    add_filter('the_content', 'glimmer_fix_shortcodes');
}

/**
 * Include mega menu
 */
require GLIMMER_TEMPLATE_DIR . '/inc/mega-menu/sh_menu_functions.php';

if ( file_exists( GLIMMER_TEMPLATE_DIR . '/inc/mega-menu/menu.php' ) ) {
    require_once( GLIMMER_TEMPLATE_DIR . '/inc/mega-menu/menu.php' );
}
require_once GLIMMER_TEMPLATE_DIR . '/inc/mega-menu/nav-menu-roles/nav-menu-roles.php';
require_once GLIMMER_TEMPLATE_DIR . '/inc/mega-menu/icons.php';