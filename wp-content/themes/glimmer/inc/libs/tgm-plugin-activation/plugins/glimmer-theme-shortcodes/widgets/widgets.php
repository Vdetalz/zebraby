<?php
/**
 * Load and register widgets
 *
 * @package Glimmer
 */
require_once plugin_dir_path( __FILE__ ) . 'popular-posts.php';
require_once plugin_dir_path( __FILE__ ) . 'latest-posts.php';
require_once plugin_dir_path( __FILE__ ) . 'about-me.php';
require_once plugin_dir_path( __FILE__ ) . 'follow-me.php';
require_once plugin_dir_path( __FILE__ ) . 'advertisement.php';
require_once plugin_dir_path( __FILE__ ) . 'aboutus-contact.php';
require_once plugin_dir_path( __FILE__ ) . 'facebook-likebox.php';
require_once plugin_dir_path( __FILE__ ) . 'google-plus.php';
require_once plugin_dir_path( __FILE__ ) . 'twitter-feed.php';
require_once plugin_dir_path( __FILE__ ) . 'newsletter.php';
require_once plugin_dir_path( __FILE__ ) . 'recent-comments.php';
require_once plugin_dir_path( __FILE__ ) . 'loginform.php';

/**
 * Register widgets
 *
 */
add_action('widgets_init','glimmer_register_widgets');
function glimmer_register_widgets() {
	register_widget('Glimmer_Popular_Posts');
	register_widget('Glimmer_Latest_Posts');
	register_widget('Glimmer_About_Me');
	register_widget('Glimmer_Follow_Me');
	register_widget('Glimmer_Advertisement');
	register_widget('Glimmer_Aboutus_Contact');
	register_widget('Glimmer_Fb_Likebox');
	register_widget('Glimmer_Googleplus');
	register_widget('Glimmer_Twitter_Feed');
	register_widget('Glimmer_Newsletter');
	register_widget('Glimmer_Comments');
	register_widget('Glimmer_Login_Form');
}