<?php
// glimmer rewrite excerpt function
if ( ! function_exists( 'glimmer_wp_new_excerpt' ) ) :
	function glimmer_wp_new_excerpt($text) {
	    global $softhopper_glimmer;
	    if ($text == '')
	    {
	        $text = get_the_content('');
	        $text = strip_shortcodes( $text );
	        $text = apply_filters('the_content', $text);
	        $text = str_replace(']]>', ']]>', $text);
	        //$text = strip_tags($text);
	        $text = nl2br($text);
	        $excerpt_length = apply_filters('excerpt_length', $softhopper_glimmer['post_excerpt']);
	        $words = explode(' ', $text, $excerpt_length + 1);
	        if (count($words) > $excerpt_length) {
	            array_pop($words);
	            array_push($words, __( '&hellip;<a href="'.get_the_permalink().'" class="more-link">'.esc_html__( 'Continue Reading',  'glimmer').'</a>', 'glimmer'));
	            $text = implode(' ', $words);
	        }
	    }
	    return $text;
	}
	remove_filter('get_the_excerpt', 'wp_trim_excerpt');
	add_filter('get_the_excerpt', 'glimmer_wp_new_excerpt');
endif;

//Remove Redux Demo Link
if ( ! function_exists( 'glimmer_removeDemoModeLink' ) ) :
	// remove redux demo mode link from plugin
	function glimmer_removeDemoModeLink() { // Be sure to rename this function to something more unique
	    if ( class_exists('ReduxFrameworkPlugin') ) {
	        remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
	    }
	    if ( class_exists('ReduxFrameworkPlugin') ) {
	        remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );    
	    }
	}
	add_action('init', 'glimmer_removeDemoModeLink');
endif;

/**
 * Removes the demo link and the notice of integrated demo from the redux-framework plugin
 */
if ( ! function_exists( 'remove_demo' ) ) {
    function remove_demo() {
        // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
        if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
            remove_filter( 'plugin_row_meta', array(
                ReduxFrameworkPlugin::instance(),
                'plugin_metalinks'
            ), null, 2 );

            // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
            remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
        }
    }
}

if( ! function_exists('glimmer_self_serv_function_checker') ) {
	function glimmer_self_serv_function_checker() {
		return glimmer_theme_get_file_name( $_SERVER['PHP_SELF'] );
	}
}

if( ! function_exists('glimmer_return_mentaboxes') ) {
	function glimmer_return_mentaboxes( $a ) {
		add_action( 'add_meta_boxes', array( $a, 'add_metaboxes' ) );
	}
}

if( ! function_exists('glimmer_return_mentaboxes_extra_parameter') ) {
	function glimmer_return_mentaboxes_extra_parameter( $a_id, $a_title, $a_metabox, $a_posttype, $a_context, $a_priority ) {
		add_meta_box( $a_id, $a_title, $a_metabox, $a_posttype, $a_context, $a_priority );
	}
}

// adding some js code
if( ! function_exists('sh_color_scheme_color_js') ) {
	function sh_color_scheme_color_js() { ?>

	    <?php  
	        global $sh_glimmer_rgba_color_scheme;
	    ?>

	    <script>
	        /* convert rgba to hex*/

	        (function () {

	        /**
	         * @constructor
	         * @param {number} r 0-255
	         * @param {number} g 0-255
	         * @param {number} b 0-255
	         * @param {number=} a 0-1
	         */
	        function Color(r, g, b, a)
	        {
	            this.r = r;
	            this.b = b;
	            this.g = g;
	            this.a = typeof a == "number" ? a : 1;
	        }

	        /**
	         * Return an rgb() or rgba() color
	         * 
	         * @override
	         * @return {string}
	         */
	        Color.prototype.toString = function ()
	        {
	            var r = Math.round(this.r),
	                g = Math.round(this.g),
	                b = Math.round(this.b),
	                a = this.a;

	            if (a === 1)
	                return "rgb(" + r + ", " + g + ", " + b + ")";
	            else
	                return "rgba(" + r + ", " + g + ", " + b + ", " + a + ")";
	        };

	        /**
	         * @param {Color} color
	         * @return {Color}
	         */
	        Color.prototype.add = function (color)
	        {
	            var a = color.a;

	            return new Color(
	                (1 - a) * this.r + a * color.r,
	                (1 - a) * this.g + a * color.g,
	                (1 - a) * this.b + a * color.b
	            );
	        };

	        /**
	         * Be liberal in finding RGB values: find the first three numbers.
	         * 
	         * @param {string} haystack
	         * @return {Color}
	         */
	        function findRGB(haystack)
	        {
	            var match = /([\d]{1,3})[^\d]+([\d]{1,3})[^\d]+([\d]{1,3})/.exec(haystack);
	            
	            return match && new Color(
	                parseInt(match[1], 10),
	                parseInt(match[2], 10),
	                parseInt(match[3], 10)
	            );
	        }

	        /**
	         * @param {string} haystack
	         * @return {Color}
	         */
	        function findRGBA(haystack)
	        {
	            var match = /([\d]{1,3})[^\d]+([\d]{1,3})[^\d]+([\d]{1,3})[^\d]+([01]?\.[\d]+|0|1)/.exec(haystack);
	            
	            return match && new Color(
	                parseInt(match[1], 10),
	                parseInt(match[2], 10),
	                parseInt(match[3], 10),
	                parseFloat(match[4])
	            );
	        }

	        function recalc(color_rgba, color_bg)
	        {
	            var colorInput = color_rgba;
	            var inputColor = findRGBA(colorInput);
	            if (inputColor)
	                rgba = inputColor;

	            var bgInput = color_bg;
	            var bgColor = findRGB(bgInput);
	            if (bgColor)
	                bg = bgColor;

	            return rgb = bg.add(rgba);

	        }

	        var rgba_to_hex = recalc("<?php echo esc_attr( $sh_glimmer_rgba_color_scheme ); ?>","rgb(255, 255, 255)");
	        jQuery('#secondary .widget .widget-title > span').css('background', rgba_to_hex);

	        })();
	    </script>                  
	<?php
	}
	add_action('wp_footer', 'sh_color_scheme_color_js');
}

if ( ! function_exists( 'glimmer_social_share_link' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function glimmer_social_share_link() { ?>
    <div class="share-area">    
        <div>
        <!-- facebook share -->
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'facebook-share', 580, 400); return false;"><span class="fa fa-facebook"></span></a>
        <!-- twitter share -->
        <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'facebook-share', 580, 400); return false;"><span class="fa fa-twitter"></span></a>
        <!-- google plus share -->
        <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'google-plus-share', 550,530); return false;"><span class="fa fa-google-plus"></span></a>
        <!-- pinterest share -->
        <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','https://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><span class="fa fa-pinterest-p"></span></a>
        <!-- linkedin share -->
        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'linkedin-share', 550,530); return false;"><span class="fa fa-linkedin"></span></a>
        </div>
    </div><!-- /.share-area -->                     
    <?php
}
endif;