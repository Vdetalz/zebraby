<?php
if ( ! class_exists( 'Glimmer_Instagram_Section' ) ) {
	Class Glimmer_Instagram_Section {
		public function __construct() {
			$this->instagram_template();
		}

		public function instagram_template( ) {
			$title = softhopper_glimmer('instagram_title_content');
			$username = softhopper_glimmer('instagram_usernames');
			$limit = softhopper_glimmer('instagram_post_limit');
			$columns = softhopper_glimmer('instagram_item_columns');
			$size = softhopper_glimmer('instagram_image_size');
			$target = softhopper_glimmer('instagram_image_open');

			if ( '' !== $username ) {
				$media_array = $this->scrape_instagram( $username );

				if ( is_wp_error( $media_array ) ) {

					echo wp_kses_post( $media_array->get_error_message() );

				} else {

					// filter for images only?
					if ( $images_only = apply_filters( 'glimmer_instagram_images_only', false ) ) {
						$media_array = array_filter( $media_array, array( $this, 'images_only' ) );
					}

					// slice list down to required limit.
					$media_array = array_slice( $media_array, 0, $limit );

					// filters for custom classes.
					$ulclass = 'glimmer_instagram_pics instagram-size-' . $size . ' columns-layout-' . $columns;
					$liclass = 'glimmer_instagram_item';
					$aclass = 'glimmer_instagram_a';
					$imgclass = 'glimmer_instagram_img';
					?>
					<div class="widget widget_instagram_blocks">					
						<div class="widget-title-area"><h5 class="widget-title"><span><?php echo wp_kses_post($title); ?></span></h5></div>
						<ul class="<?php echo esc_attr($ulclass); ?> clearfix">
						<?php
						foreach( $media_array as $item ) {					
							echo '<li class="' . esc_attr( $liclass ) . '"><a href="' . esc_url( $item['link'] ) . '" target="' . esc_attr( $target ) . '"  class="' . esc_attr( $aclass ) . '"><img src="' . esc_url( $item[$size] ) . '"  alt="' . esc_attr( $item['description'] ) . '" title="' . esc_attr( $item['description'] ) . '"  class="' . esc_attr( $imgclass ) . '"/></a></li>';
						}
						?></ul>
					</div><!--  /.widget -->
					<?php
				}
			}

			switch ( substr( $username, 0, 1 ) ) {
				case '#':
					$url = '//instagram.com/explore/tags/' . str_replace( '#', '', $username );
					break;

				default:
					$url = '//instagram.com/' . str_replace( '@', '', $username );
					break;
			}
		}

		// based on https://gist.github.com/cosmocatalano/4544576.
		public function scrape_instagram( $username ) {
			$username = trim( strtolower( $username ) );

			switch ( substr( $username, 0, 1 ) ) {
				case '#':
					$url              = 'https://instagram.com/explore/tags/' . str_replace( '#', '', $username );
					$transient_prefix = 'h';
					break;

				default:
					$url              = 'https://instagram.com/' . str_replace( '@', '', $username );
					$transient_prefix = 'u';
					break;
			}

			if ( false === ( $instagram = get_transient( 'insta-a10-' . $transient_prefix . '-' . sanitize_title_with_dashes( $username ) ) ) ) {

				$remote = wp_remote_get( $url );

				if ( is_wp_error( $remote ) ) {
					return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'glimmer-core' ) );
				}

				if ( 200 !== wp_remote_retrieve_response_code( $remote ) ) {
					return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'glimmer-core' ) );
				}

				$shards      = explode( 'window._sharedData = ', $remote['body'] );
				$insta_json  = explode( ';</script>', $shards[1] );
				$insta_array = json_decode( $insta_json[0], true );

				if ( ! $insta_array ) {
					return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'glimmer-core' ) );
				}

				if ( isset( $insta_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'] ) ) {
					$images = $insta_array['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'];
				} elseif ( isset( $insta_array['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges'] ) ) {
					$images = $insta_array['entry_data']['TagPage'][0]['graphql']['hashtag']['edge_hashtag_to_media']['edges'];
				} else {
					return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'glimmer-core' ) );
				}

				if ( ! is_array( $images ) ) {
					return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'glimmer-core' ) );
				}

				$instagram = array();

				foreach ( $images as $image ) {
					if ( true === $image['node']['is_video'] ) {
						$type = 'video';
					} else {
						$type = 'image';
					}

					$caption = __( 'Instagram Image', 'glimmer-core' );
					if ( ! empty( $image['node']['edge_media_to_caption']['edges'][0]['node']['text'] ) ) {
						$caption = wp_kses( $image['node']['edge_media_to_caption']['edges'][0]['node']['text'], array() );
					}

					$instagram[] = array(
						'description' => $caption,
						'link'        => trailingslashit( '//instagram.com/p/' . $image['node']['shortcode'] ),
						'time'        => $image['node']['taken_at_timestamp'],
						'comments'    => $image['node']['edge_media_to_comment']['count'],
						'likes'       => $image['node']['edge_liked_by']['count'],
						'thumbnail'   => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][0]['src'] ),
						'small'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][2]['src'] ),
						'large'       => preg_replace( '/^https?\:/i', '', $image['node']['thumbnail_resources'][4]['src'] ),
						'original'    => preg_replace( '/^https?\:/i', '', $image['node']['display_url'] ),
						'type'        => $type,
					);
				} // End foreach().

				// do not set an empty transient - should help catch private or empty accounts.
				if ( ! empty( $instagram ) ) {
					$instagram = base64_encode( serialize( $instagram ) );
					set_transient( 'insta-a10-' . $transient_prefix . '-' . sanitize_title_with_dashes( $username ), $instagram, apply_filters( 'null_instagram_cache_time', HOUR_IN_SECONDS * 2 ) );
				}
			}

			if ( ! empty( $instagram ) ) {

				return unserialize( base64_decode( $instagram ) );

			} else {

				return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'glimmer-core' ) );

			}
		}

		function images_only( $media_item ) {

			if ( 'image' === $media_item['type'] ) {
				return true;
			}

			return false;
		}
	}
}
