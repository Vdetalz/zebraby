<?php
class Glimmer_Follow_Me extends WP_Widget {

    function __construct() {
        $params = array (
            'description' => esc_html__('Glimmer : Follow Me Info', 'glimmer'),
            'name' => esc_html__('Glimmer : Follow Me', 'glimmer')
        );
        parent::__construct('Glimmer_Follow_Me','',$params);
    }

    public function form( $instance) {
        extract($instance);
            if ( empty($total_link) ) $total_link = 12;
            $font_awesome_link = "http://fortawesome.github.io/Font-Awesome/cheatsheet/";
        ?>   
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:','glimmer'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('title'); ?>"
                name="<?php echo $this->get_field_name('title'); ?>"
                value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
        </p> 
        <p>
            <?php esc_html_e('Font Awesome Icon Class, example: fa-facebook. Get the full list','glimmer'); ?> <a href="<?php echo $font_awesome_link; ?>"><?php esc_html_e('Here','glimmer'); ?></a>
        </p>
          
        <?php
            for ($i=1; $i <= $total_link; $i++) { 
                $icon='icon_'.$i;
                $url='icon'.$i.'_url';
                ?>
                <p>
                    <label for="<?php echo $this->get_field_id($icon); ?>"><?php echo __("Social Link","glimmer")." ".$i.":"; ?></label>
                    <input
                        class="widefat margin-bottom"
                        type="text"
                        placeholder="Font Awesome Icon"
                        id="<?php echo $this->get_field_id($icon); ?>"
                        name="<?php echo $this->get_field_name($icon); ?>"
                        value="<?php if( isset($$icon) ) echo esc_attr($$icon); ?>" />
                    <input
                        class="widefat"
                        type="text"
                        placeholder="Social URL"
                        id="<?php echo $this->get_field_id($url); ?>"
                        name="<?php echo $this->get_field_name($url); ?>"
                        value="<?php if( isset($$url) ) echo esc_attr($$url); ?>" />
                </p>
                <?php
            }
        ?>
            <p>
                <label for="<?php echo $this->get_field_id('total_link'); ?>"><?php esc_html_e('Number of link you want to post:','glimmer'); ?></label>
                <input 
                    id="<?php echo $this->get_field_id('total_link'); ?>" 
                    type="text" 
                    name="<?php echo $this->get_field_name('total_link'); ?>"
                    value="<?php if( isset($total_link) ) echo esc_attr($total_link); ?>"
                    size="3" />
            </p>         
        <?php
    } // end form function

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        foreach ($new_instance as $key => $value) {
            if ( $key == "total_link" ) {
                $instance[$key] = intval( $new_instance[$key] );
            } else {                
                $instance[$key] = strip_tags( $new_instance[$key] );
            }
        } // end for each
        return $instance;
    }

    public function widget($args, $instance) {
        extract($args);
        extract($instance);
        $title = apply_filters( 'widget_title', $title );       
   
        echo $before_widget;
            if ( !empty( $title ) ) {
                echo $before_title . $title . $after_title;
            }
            ?>
            <div class="follow-us-area">                    
                    <?php
                    $print_url = "";
                    foreach ($instance as $keys => $value) {
                        $key=explode("_", $keys);
                        if($key[0] == 'icon')
                        {
                            if ( !empty($value) ) {
                              $print_url=true;
                              $icon = apply_filters( $value, $value );
                            }
                        }
                        elseif ( isset( $key[1] ) && $key[1] == 'url' && $print_url) {
                            $url = apply_filters( $value, $value );   
                            echo '<a href="'.$url.'" class="follow-link"><i class="fa '.$icon.'"></i></a>';
                            $print_url=false;
                        }
                    } // end foreach
                ?>                
            </div> <!-- /.follow-us-area -->                
            <?php
        echo $after_widget;
    }
}