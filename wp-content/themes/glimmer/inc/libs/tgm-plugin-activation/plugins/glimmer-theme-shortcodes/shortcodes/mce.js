(function() {
	tinymce.PluginManager.add('glimmer_mce_button', function( editor, url ) {
		editor.addButton( 'glimmer_mce_button', {
            text: glimmerLang.shortcode_glimmer, 
            icon: false,
			tooltip: glimmerLang.shortcode_glimmer,
			type: 'menubutton',
			minWidth: 210,
			menu: [
				{
					text: glimmerLang.shortcode_button,
					onclick: function() {
						editor.windowManager.open( {
							title: glimmerLang.shortcode_button,
							body: [
								{
									type: 'listbox',
									name: 'ButtonType',
									label: glimmerLang.shortcode_type,
									'values': [
										{text: glimmerLang.shortcode_default, value: 'btn-default'},
										{text: glimmerLang.shortcode_primary, value: 'btn-primary'},
										{text: glimmerLang.shortcode_success, value: 'btn-success'},
										{text: glimmerLang.shortcode_info, value: 'btn-info'},
										{text: glimmerLang.shortcode_warning, value: 'btn-warning'},
										{text: glimmerLang.shortcode_danger, value: 'btn-danger'},
									]
								},
								{
									type: 'listbox',
									name: 'ButtonSize',
									label: glimmerLang.shortcode_size,
									'values': [
										{text: glimmerLang.shortcode_size_large, value: 'btn-lg'},
										{text: glimmerLang.shortcode_size_default, value: ''},
										{text: glimmerLang.shortcode_size_small, value: 'btn-sm'},
										{text: glimmerLang.shortcode_size_ex_small, value: 'btn-xs'},
									]
								},
								{
									type: 'textbox',
									name: 'ButtonText',
									label: glimmerLang.shortcode_text,
									minWidth: 300,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[button type="' + e.data.ButtonType + '" size="' + e.data.ButtonSize + '" text="' + e.data.ButtonText + '"]');
							}
						});
					} // onclick
				},
				{
					text: glimmerLang.shortcode_progressbar,
					onclick: function() {
						editor.windowManager.open( {
							title: glimmerLang.shortcode_progressbar,
							body: [
								{
									type: 'listbox',
									name: 'ProgressbarType',
									label: glimmerLang.shortcode_type,
									'values': [
										{text: glimmerLang.shortcode_default, value: 'default'},
										{text: glimmerLang.shortcode_primary, value: 'primary'},
										{text: glimmerLang.shortcode_success, value: 'success'},
										{text: glimmerLang.shortcode_info, value: 'info'},
										{text: glimmerLang.shortcode_warning, value: 'warning'},
										{text: glimmerLang.shortcode_danger, value: 'danger'},
									]
								},
								{
									type: 'textbox',
									name: 'ProgressbarWidth',
									label: glimmerLang.shortcode_width,
									minWidth: 300,
									value: '50'
								},
								{
									type: 'textbox',
									name: 'ProgressbarTitle',
									label: glimmerLang.shortcode_title,
									minWidth: 300,
									value: ''
								},
								{
									type: 'listbox',
									name: 'ProgressbarStriped',
									label: glimmerLang.shortcode_striped,
									'values': [
										{text: glimmerLang.shortcode_true, value: 'true'},
										{text: glimmerLang.shortcode_false, value: 'false'},
									]
								},
								{
									type: 'listbox',
									name: 'ProgressbarAnimation',
									label: glimmerLang.shortcode_animation,
									'values': [
										{text: glimmerLang.shortcode_true, value: 'true'},
										{text: glimmerLang.shortcode_false, value: 'false'},
									]
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[progress_bar type="' + e.data.ProgressbarType + '" width="' + e.data.ProgressbarWidth + '" title="' + e.data.ProgressbarTitle + '" striped="' + e.data.ProgressbarStriped + '" animation="' + e.data.ProgressbarAnimation + '"]');
							}
						});
					}
				},
				{
					text: glimmerLang.shortcode_status,
					onclick: function() {
						editor.windowManager.open( {
							title: glimmerLang.shortcode_status,
							body: [
								{
									type: 'listbox',
									name: 'StatusType',
									label: glimmerLang.shortcode_type,
									'values': [
										{text: glimmerLang.shortcode_facebook, value: 'facebook'},
										{text: glimmerLang.shortcode_twitter, value: 'twitter'},
										{text: glimmerLang.shortcode_google_plus, value: 'google_plus'},
									]
								},
								{
									type: 'textbox',
									name: 'StatusURL',
									label: glimmerLang.shortcode_url,
									minWidth: 300,
									value: ''
								},
								{
									type: 'textbox',
									name: 'StatusBackground',
									label: glimmerLang.shortcode_background_img,
									minWidth: 300,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[status type="' + e.data.StatusType + '" url="' + e.data.StatusURL + '" background="' + e.data.StatusBackground + '"]');
							}
						});
					}
				},
				{
					text: glimmerLang.shortcode_alert,
					onclick: function() {
						editor.windowManager.open( {
							title: glimmerLang.shortcode_alert,
							body: [
								{
									type: 'listbox',
									name: 'AlertType',
									label: glimmerLang.shortcode_type,
									'values': [
										{text: glimmerLang.shortcode_default, value: 'default'},
										{text: glimmerLang.shortcode_primary, value: 'primary'},
										{text: glimmerLang.shortcode_success, value: 'success'},
										{text: glimmerLang.shortcode_info, value: 'info'},
										{text: glimmerLang.shortcode_warning, value: 'warning'},
										{text: glimmerLang.shortcode_danger, value: 'danger'},
									]
								},
								{
									type: 'listbox',
									name: 'AlertDismiss',
									label: glimmerLang.shortcode_dismiss,
									'values': [
										{text: glimmerLang.shortcode_true, value: 'true'},
										{text: glimmerLang.shortcode_false, value: 'false'},
									]
								},
								{
									type: 'textbox',
									name: 'AlertContent',
									label: glimmerLang.shortcode_content,
									value: '',									
									multiline: true,
									minWidth: 300,
									minHeight: 100
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[alert type="' + e.data.AlertType + '" dismiss="' + e.data.AlertDismiss + '"]' + e.data.AlertContent + '[/alert]');
							}
						});
					}
				},
				{
					text: glimmerLang.shortcode_video,
					onclick: function() {
						editor.windowManager.open( {
							title: glimmerLang.shortcode_video,
							body: [
								{
									type: 'textbox',
									name: 'VideoURL',
									label: glimmerLang.shortcode_video_url,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'VideoWidth',
									label: glimmerLang.shortcode_width,
									value: ''
								},
								{
									type: 'textbox',
									name: 'Videoheight',
									label: glimmerLang.shortcode_height,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[embed width="' + e.data.VideoWidth + '" height="' + e.data.Videoheight + '"]' + e.data.VideoURL + '[/embed]');
							}
						});
					}
				},
				{
					text: glimmerLang.shortcode_audio,
					onclick: function() {
						editor.windowManager.open( {
							title: glimmerLang.shortcode_audio,
							body: [
								{
									type: 'textbox',
									name: 'mp3URL',
									label: glimmerLang.shortcode_mp3,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'm4aURL',
									label: glimmerLang.shortcode_m4a,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'oggURL',
									label: glimmerLang.shortcode_ogg,
									value: 'http://',
									minWidth: 300,
								},
								
							],
							onsubmit: function( e ) {
								editor.insertContent( '[audio mp3="' + e.data.mp3URL + '" m4a="' + e.data.m4aURL + '" ogg="' + e.data.oggURL + '"]');
							}
						});
					}
				},
				{
					text: glimmerLang.shortcode_tooltip,
					onclick: function() {
						editor.windowManager.open( {
							title: glimmerLang.shortcode_tooltip,
							body: [
								{
									type: 'textbox',
									name: 'TooltipText',
									label: glimmerLang.shortcode_text,
									value: '',
									minWidth: 300,
								},
								{
									type: 'listbox',
									name: 'TooltipDirection',
									label: glimmerLang.shortcode_direction,
									'values': [
										{text: glimmerLang.shortcode_top, value: 'top'},
										{text: glimmerLang.shortcode_right, value: 'right'},
										{text: glimmerLang.shortcode_bottom, value: 'bottom'},
										{text: glimmerLang.shortcode_left, value: 'left'},
									]
								},
								{
									type: 'textbox',
									name: 'ToolTipContent',
									label: glimmerLang.shortcode_content,
									value: '',
									multiline: true,
									minWidth: 300,
									minHeight: 100
								}
								
							],
							onsubmit: function( e ) {
								editor.insertContent( '[tooltip text="' + e.data.TooltipText + '" direction="' + e.data.TooltipDirection + '"]'+ e.data.ToolTipContent +'[/tooltip]');
							}
						});
					}
				},
				{
					text: glimmerLang.shortcode_columns,
						onclick: function() {
							editor.insertContent( '\
							[row]<br />\
								[column number="6" offset=""] Column Content Goes Here [/column]<br />\
								[column number="6" offset=""] Column Content Goes Here [/column]<br />\
							[/row]');
						}
				},
				{
					text: glimmerLang.shortcode_accordion,
						onclick: function() {
							editor.insertContent( '\
							[collapse_group]<br />\
								[collapse id="accordion_one" title="Accordion Title" expanded="false"] Accordion Content [/collapse]<br />\
								[collapse id="accordion_two" title="Accordion Title" expanded="false"] Accordion Content [/collapse]<br />\
							[/collapse_group]');
						}
				},
				{
					text: glimmerLang.shortcode_tab,
						onclick: function() {
							editor.insertContent( '\
							[tabs]<br />\
								[tab id="tab_one" title="Tab Title" active="active"] Tab Content [/tab]<br />\
								[tab id="tab_two" title="Tab Title"] Tab Content [/tab]<br />\
								[tab id="tab_three" title="Tab Title"] Tab Content [/tab]<br />\
							[/tabs]');
						}
				},
				{
                    text: glimmerLang.shortcode_custom_icon,
                    classes: 'sh-custom-icon',
                },
			]
		});
	});
})();