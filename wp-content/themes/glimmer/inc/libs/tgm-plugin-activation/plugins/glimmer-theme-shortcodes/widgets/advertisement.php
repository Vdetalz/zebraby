<?php
class Glimmer_Advertisement extends WP_Widget {

    function __construct() {
        $params = array (
            'description' => esc_html__('Glimmer : Advertisement Widget', 'glimmer'),
            'name' => esc_html__('Glimmer : Advertisement', 'glimmer')
        );
        parent::__construct('Glimmer_Advertisement','',$params);
    }

    /** @see WP_Widget::form */
    public function form( $instance) {
        extract($instance); ?> 
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:','glimmer'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('title'); ?>"
                name="<?php echo $this->get_field_name('title'); ?>"
                value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
        </p> 
        <p>
            <?php
                if( !isset($ad_type) ) {
                    $ad_type = ""; 
                }
            ?>
            <label for="<?php echo $this->get_field_id('ad_type'); ?>"><?php esc_html_e('Ad Type','glimmer'); ?>:</label> 
            <select id="<?php echo $this->get_field_id('ad_type'); ?>" class="ad-type" name="<?php echo $this->get_field_name('ad_type'); ?>" class="widefat">
                <option value="image" <?php if ('image' == $ad_type) echo 'selected="selected"'; ?>><?php esc_html_e('Image', 'glimmer'); ?></option>
                <option value="code" <?php if ('code' == $ad_type) echo 'selected="selected"'; ?>><?php esc_html_e('Code', 'glimmer'); ?></option>
            </select>
        </p> 
        <p class="type-image">
           <label class="ad-img-lebel"><?php esc_html_e('Ad Image:','glimmer'); ?></label>       
        <?php
          $arg = array(       
            'parent_div_class'=> 'custom-image-upload',                    
            'field_name' => $this->get_field_name('image'),
            'field_id' => 'upload_logo',
            'field_class' => 'upload_image_field',
            
            'upload_button_id' => 'upload_logo_button',
            'upload_button_class' => 'upload_logo_button',
            'upload_button_text' => 'Upload',
            
            'remove_button_id' => 'remove_logo',
            'remove_button_class' => 'remove_logo_button',
            'remove_button_text' => 'Remove'
            
            );
           if ( empty($image) ) $image = NULL;
           glimmer_theme_ad_media_custom($arg,false,$image);
        ?>
        </p>
        <p class="type-image">
            <label for="<?php echo $this->get_field_id('ad_url'); ?>"><?php esc_html_e('Ad URL:','glimmer'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('ad_url'); ?>"
                name="<?php echo $this->get_field_name('ad_url'); ?>"
                value="<?php if( isset($ad_url) ) echo esc_url($ad_url); ?>" />
        </p>
        <p class="type-code">
            <label for="<?php echo $this->get_field_id('ad_code'); ?>"><?php esc_html_e('Code:','glimmer'); ?></label>
            <textarea 
                class="widefat" 
                rows="6" 
                cols="20" 
                id="<?php echo $this->get_field_id('ad_code'); ?>" 
                name="<?php echo $this->get_field_name('ad_code'); ?>"><?php if( isset($ad_code) ) echo esc_attr($ad_code); ?></textarea>
        </p> 
        <script>
        (function ($) {
            "use strict"; 
            // Show/hide advertisement widget field
            $( '.ad-type' ).on( 'change', function() {
                if( $(this).val() == "image") {
                    $(this).parent().nextAll('.type-image, .custom-image-upload').show();
                    $(this).parent().nextAll('.type-code').hide();
                } else {
                    $(this).parent().nextAll('.type-image, .custom-image-upload').hide();
                    $(this).parent().nextAll('.type-code').show();
                }
            } ).trigger( 'change' );
        }(jQuery));
        </script>
      <?php       
    } // end form function

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['image'] = strip_tags( $new_instance['image'] );
        $instance['ad_url'] = strip_tags( $new_instance['ad_url'] );
        $instance['ad_type'] = strip_tags( $new_instance['ad_type'] );
        $instance['ad_code'] = $new_instance['ad_code'];
     
        return $instance;
    }

    public function widget($args, $instance) {
        extract($args);
        extract($instance);
        $title = apply_filters('widget_title', $title);
        $ad_url = apply_filters('widget_ad_url', $ad_url);
        $image = apply_filters('widget_image', $image);
        
       
        echo $before_widget;
            if ( !empty( $title ) ) {
                echo $before_title . $title . $after_title;
            }

            if ( $ad_type == "image" ) { ?>
            <div class="advertisement-img">                    
                <?php 
                    if ( !empty( $image ) ) {
                         if ( !empty( $ad_url ) ) {
                            echo "<a class='advertisement-link' href='$ad_url'><img class='img-responsive' src='$image' alt='Ad Image' /></a>";
                        } else {
                            echo "<img class='img-responsive' src='$image' alt='Ad Image' />";
                        }
                    }
                ?>
            </div> <!-- /.ad-img -->                
            <?php } else {
                echo '<div class="advertisement-code">';
                echo $ad_code;
                echo '</div>';
            }
        echo $after_widget;
    } // end widget function
    

} // class Cycle Widget

?>
<?php function glimmer_theme_ad_media_custom( $arg, $use_custom_buttons = false, $value = "" ){
    
    $defaults = array(
        'useid' => false ,
        'hidden' => true,
        
        'parent_div_class'=> 'custom-image-upload',
        
        'field_label' => 'upload_image_field_label',        
        'field_name' => 'upload_image_field',
        'field_id' => 'upload_image_field',
        'field_class' => 'upload_image_field',
        
        'upload_button_id' => 'upload_logo_button',
        'upload_button_class' => 'upload_logo_button',
        'upload_button_text' => 'Upload',
        
        'remove_button_id' => 'remove_logo_button',
        'remove_button_class' => 'remove_logo_button',
        'remove_button_text' => 'Remove',
        
        'preview_div_class' => 'preview',
        'preview_div_class2' => 'preview remove_box',
        'preview_div_id' => 'preview',
        
        'height' => '100',
        'width' => '100'
                    );
        $arguments = wp_parse_args($arg,$defaults);
        
        extract($arguments);
        wp_enqueue_media();
    ?>                                   
   <?php if( ! $use_custom_buttons ): ?>
   <div class="<?php echo $parent_div_class; ?>" id="<?php echo $parent_div_class; ?>">
   
        <input name="<?php echo $field_name; ?>" id="<?php echo $field_id; ?>" class="<?php echo $field_class; ?>" <?php if($hidden): ?>  type="hidden" <?php else: ?> type="text" <?php endif; ?> value="<?php if ( $value != "") { echo stripslashes($value); }  ?>" />
        
        <input type="button" class="button button-primary <?php echo $upload_button_class; ?>" id="<?php echo $upload_button_id; ?>"  value="<?php echo $upload_button_text; ?>">
        
        <input type="button" class="button button-primary <?php echo $remove_button_class; ?>" id="<?php echo $remove_button_id; ?>" <?php  if ( $value == "") {  ?> disabled="disabled" <?php } ?> value="<?php echo $remove_button_text; ?>">
        
        <div class="<?php echo $preview_div_class; ?>" style="float: none; <?php  if ( empty($value) ) { ?> display: none; <?php } ?>">
            <img src="<?php  echo stripslashes($value);  ?>" alt="img-preview">
        </div>   
        <div style="clear: both;"></div>
    </div>
   <?php endif; ?>
    <?php
        $usesep = ($useid) ? "#" : ".";
        if($useid):
        
         $field_class = $field_id;
         $upload_button_class = $upload_button_id;
         $remove_button_class = $remove_button_id;
         $preview_div_class = $preview_div_id;
            
        endif;  
    ?>
    <script type="text/javascript">

    jQuery(document).ready(function($){
        $('<?php echo $usesep.$remove_button_class; ?>').on('click', function(e) {
            <?php if(!$useid): ?>
           $(this).parent().find("<?php echo $usesep.$field_class; ?>").val(""); 
           $(this).parent().find("<?php echo $usesep.$preview_div_class; ?> img").attr("src","").fadeOut("fast");
           <?php else: ?>
           $("<?php echo $usesep.$field_class; ?>").val("");
           $("<?php echo $usesep.$preview_div_class; ?> img").attr("src","").fadeOut("fast");
           <?php endif; ?>
           $(this).attr("disabled","disabled");
         return false;   
        });
        var _custom_media = true,
          _orig_send_attachment = wp.media.editor.send.attachment;

      $('<?php echo $usesep.$upload_button_class; ?>').on('click', function(e) {
        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = $(this);
        var id = button.attr('id').replace('_button', '');
        _custom_media = true;
        wp.media.editor.send.attachment = function(props, attachment){
          if ( _custom_media ) {
              
              <?php if(!$useid): ?>
            button.parent().find("<?php echo $usesep.$field_class; ?>").val(attachment.url);
            button.parent().find("<?php echo $usesep.$preview_div_class; ?> img").attr("src",attachment.url).fadeIn("fast");
            button.parent().find("<?php echo $usesep.$remove_button_class; ?>").removeAttr("disabled");
            if($('<?php echo $usesep.$preview_div_class; ?> img').length > 0){ $('<?php echo $usesep.$preview_div_class; ?>').css('display','block'); };
            <?php else: ?>
            $("<?php echo $usesep.$field_class; ?>").val(attachment.url);
            $("<?php echo $usesep.$preview_div_class; ?> img").attr("src",attachment.url).fadeIn("fast");        
            $("<?php echo $usesep.$remove_button_class; ?>").removeAttr("disabled");
            <?php endif; ?>
          } else {
            return _orig_send_attachment.apply( this, [props, attachment] );
          };
          $('.preview').removeClass('remove_box');
        }

        wp.media.editor.open(button);
        return false;
      });
    });        
    </script>
   <?php  
}