<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "softhopper_glimmer";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();

    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_html__( 'Glimmer Options', 'glimmer' ),
        'page_title'           => esc_html__( 'Glimmer Options', 'glimmer' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-admin-generic',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 100,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'forced_dev_mode_off' => true,
        // To disable tracking
        'disable_tracking' => true,
        // To forcefully disable the dev mode
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => 62,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => esc_html__( 'Documentation', 'glimmer' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => esc_html__( 'Support', 'glimmer' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => esc_html__( 'Extensions', 'glimmer' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/softhopper',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/softhopper',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/softhopperbd',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        //$args['intro_text'] = sprintf( esc_html__( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'glimmer' ), $v );
    } else {
        //$args['intro_text'] = esc_html__( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'glimmer' );
    }

    // Add content after the form.
    //$args['footer_text'] = esc_html__( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'glimmer' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_html__( 'Theme Information 1', 'glimmer' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'glimmer' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_html__( 'Theme Information 2', 'glimmer' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'glimmer' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = esc_html__( '<p>This is the sidebar content, HTML is allowed.</p>', 'glimmer' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*
        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for
     */

    // -> START General Options
    Redux::setSection( $opt_name, array(
        'title' => esc_html__('General Options', 'glimmer' ),
        'icon' => 'el el-cog',
        'id'               => 'general-options',
    ) );

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Favicon', 'glimmer' ),
            'subsection'       => true,
            'fields' => array (    
                array (
                    // this is load because if it not load other slides extension not work
                    'id'          => 'glimmer_hidden_slides',
                    'type'        => 'slides',
                    'title'       => esc_html__( 'Glimmer Hidden Slider', 'glimmer' ),
                ),
                array(
                    'id'=>'favicon',
                    'type' => 'media',
                    'url'=> true,
                    'readonly' => false,
                    'title' => esc_html__('Favicon Main', 'glimmer'),
                    'default' => array(
                        'url' => GLIMMER_TEMPLATE_DIR_URL . '/images/favicon/favicon.ico'
                    )
                ),
                array(
                    'id'=>'icon_iphone',
                    'type' => 'media',
                    'url'=> true,
                    'readonly' => false,
                    'title' => esc_html__('Apple iPhone Icon', 'glimmer'),
                    'desc' => esc_html__('Icon for Apple iPhone (57px X 57px)', 'glimmer'),
                    'default' => array(
                        'url' => GLIMMER_TEMPLATE_DIR_URL . '/images/favicon/apple-touch-icon.png'
                    )
                ),

                array(
                    'id'=>'icon_iphone_retina',
                    'type' => 'media',
                    'url'=> true,
                    'readonly' => false,
                    'title' => esc_html__('Apple iPhone Retina Icon', 'glimmer'),
                    'desc' => esc_html__('Icon for Apple iPhone Retina (114px X 114px)', 'glimmer'),
                    'default' => array(
                        'url' => GLIMMER_TEMPLATE_DIR_URL . '/images/favicon/apple-touch-icon_114x114.png'
                    )
                ),

                array(
                    'id'=>'icon_ipad',
                    'type' => 'media',
                    'url'=> true,
                    'readonly' => false,
                    'title' => esc_html__('Apple iPad Icon', 'glimmer'),
                    'desc' => esc_html__('Icon for Apple iPad (72px X 72px)', 'glimmer'),
                    'default' => array(
                        'url' => GLIMMER_TEMPLATE_DIR_URL . '/images/favicon/apple-touch-icon_72x72.png'
                    )
                ),

                array(
                    'id'=>'icon_ipad_retina',
                    'type' => 'media',
                    'url'=> true,
                    'readonly' => false,
                    'title' => esc_html__('Apple iPad Retina Icon', 'glimmer'),
                    'desc' => esc_html__('Icon for Apple iPad Retina (144px X 144px)', 'glimmer'),
                    'default' => array(
                        'url' => GLIMMER_TEMPLATE_DIR_URL . '/images/favicon/apple-touch-icon_144x144.png'
                    )
                ),               
            )
        ) 
    ); //favicon
    
    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Preloader', 'glimmer' ),
            'subsection'       => true,
            'fields' => array (    
                array(
                    'id'=>'preloader',
                    'type' => 'switch',
                    'title' => esc_html__('Site Preloader', 'glimmer'),
                    'default' => false
                ),
                array(
                    'id'=>'preloader_logo',
                    'type' => 'media',
                    'url'=> true,
                    'required' => array( 'preloader', '=', '1' ),
                    'readonly' => false,
                    'title' => esc_html__('Preloader Logo', 'glimmer'),
                    'subtitle' => esc_html__('This logo image will show in preloader', 'glimmer'),
                    'default' => array(
                        'url' => GLIMMER_TEMPLATE_DIR_URL . '/images/preloader.png'
                    )
                ), 
                array(
                    'id'       => 'preloader_animated_icon',
                    'type'     => 'select',
                    'title'    => esc_html__( 'Preloader Animated Icon', 'glimmer' ),
                    'subtitle' => esc_html__( 'This animated icon will show in preloader', 'glimmer' ),
                    'required' => array( 'preloader', '=', '1' ),
                    //Must provide key => value pairs for select options
                    'options'  => array(
                        '1' => 'fa-spinner fa-pulse',
                        '2' => 'fa-spinner fa-spin',
                        '3' => 'fa-circle-o-notch fa-spin',
                        '4' => 'fa-refresh fa-spin',
                        '5' => 'fa-cog fa-spin',
                    ),
                    'description' => __( "These animated icon used from font awesome. See demo <a target='_blank' href='http://fortawesome.github.io/Font-Awesome/examples/'>here</a> from Animated Icons List", "glimmer" ),
                    'default'  => '1'
                ),            
            )
        ) 
    ); //preloader

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Single Post Page Options', 'glimmer' ),
            'subsection'       => true,
            'fields' => array (  
                array(
                    'id'       => 'post_title',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Post Title', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can show or hide post title', 'glimmer' ),
                    'default' => 1
                ),    
                array(
                    'id'       => 'post_cat_date_author_meta',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Post category, data and author meta', 'glimmer' ),
                    'default' => 1
                ),   
                array(
                    'id'       => 'post_social_share',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Social Share Link', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can show or hide social share link', 'glimmer' ),
                    'default' => 1
                ),  
                array(
                    'id'       => 'post_pagination',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Post Pagination', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can show or hide post pagination', 'glimmer' ),
                    'default' => 1
                ),   
                array(
                    'id'       => 'post_author_info_box',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Author Info Box', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can show or hide author info box', 'glimmer' ),
                    'default' => 0
                ), 
                array(
                    'id'       => 'related_post',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Related Posts', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can show or hide related posts', 'glimmer' ),
                    'default' => 1
                ),
                array(
                    'id'       => 'related_query',
                    'type'     => 'radio',
                    'title'    => esc_html__( 'Query Type', 'glimmer' ),
                    'subtitle' => esc_html__( 'Show related by this query type', 'glimmer' ),
                    //Must provide key => value pairs for radio options
                    'options'  => array(
                        'tag' => esc_html__( 'Tag', 'glimmer' ),
                        'category' => esc_html__( 'Category', 'glimmer' ),
                        'author' => esc_html__( 'Author', 'glimmer' )
                    ),
                    'default'  => 'tag',
                    'required' => array( 'related_post', '=', '1' ),  
                ),          
            )
        ) 
    ); //Single Post Page Options

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Page Options', 'glimmer' ),
            'subsection'       => true,
            'fields' => array (
                array(
                    'id'       => 'page_title',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Page Title', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can show or hide page title', 'glimmer' ),
                    'default' => 1
                ),    
                array(
                    'id'       => 'page_author_date_meta',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Page author and date meta', 'glimmer' ),
                    'default' => 0
                ),   
                array(
                    'id'       => 'page_social_share',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Social Share Link', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can show or hide social share link', 'glimmer' ),
                    'default' => 0
                ),    
                array(
                    'id'       => 'page_author_info_box',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Author Info Box', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can show or hide author info box', 'glimmer' ),
                    'default' => 0
                ),  
            )
        ) 
    ); //Page Options

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Other Options', 'glimmer' ),
            'subsection'       => true,
            'fields' => array ( 
                array (
                    'id'       => 'custom_css',
                    'type'     => 'ace_editor',
                    'title'    => esc_html__( 'Custom CSS Code', 'glimmer' ),
                    'subtitle' => esc_html__( 'Paste your CSS code here.', 'glimmer' ),
                    'mode'     => 'css',
                    'theme'    => 'monokai',
                    'default'  => "#header {\n   margin: 0 auto;\n}"
                ),
                array (
                    'id'       => 'custom_js',
                    'type'     => 'ace_editor',
                    'title'    => esc_html__( 'Custom JS Code', 'glimmer' ),
                    'subtitle' => esc_html__( 'Paste your JS code here.', 'glimmer' ),
                    'mode'     => 'javascript',
                    'theme'    => 'monokai',
                    'default'  => "jQuery(document).ready(function($){\n\n});"
                ),   
                array(
                    'id'       => 'tracking_code_for_head',
                    'type'     => 'textarea',
                    'title'    => esc_html__( 'Tracking Code For Header', 'glimmer' ),
                    'subtitle' => esc_html__( 'Paste your Google Analytics (or other) tracking code here. This will be added into the header template in &lt;head&gt; tag of your theme.', 'glimmer' ),
                    'default'  => ""
                ), 
                array(
                    'id'       => 'tracking_code_for_footer',
                    'type'     => 'textarea',
                    'title'    => esc_html__( 'Tracking Code For Footer', 'glimmer' ),
                    'subtitle' => esc_html__( 'Paste your Google Analytics (or other) tracking code here. This will be added into the footer template befor &lt;/body&gt; tag of your theme.', 'glimmer' ),
                    'default'  => ""
                ),            
            )
        ) 
    ); //Other Options

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Layout', 'glimmer'),
            //'icon_class' => 'fa-lg',
            'icon' => 'fa fa-th-list',
            'fields' => array ( 
                array (
                    'id' => 'sidebar_layout',
                    'type' => 'image_select',
                    'title' => esc_html__('Default Layout', 'glimmer'),
                    'subtitle' => esc_html__('Default layout for whole site', 'glimmer'),
                    'options' => array (
                        'full-width' => array (
                            'alt' => 'empty.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/sidebars/empty.png'
                        ),
                        'sidebar-left' => array(
                            'alt' => 'single-left.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/sidebars/single-left.png'
                        ),
                        'sidebar-right' => array(
                            'alt' => 'single-right.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/sidebars/single-right.png'
                        ),                               
                    ),
                    'default' => 'sidebar-right'
                ),
                array(
                    'id'       => 'sidebar_layout_full_width_column',
                    'type'     => 'select',
                    'title'    => esc_html__( 'Default Layout Full Width Column', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can increase or decrease full width size', 'glimmer' ),
                    'required' => array( 'sidebar_layout', '=', 'full-width' ),
                    'options'  => array(
                        '6' => '6 Grid',
                        '7' => '7 Grid',
                        '8' => '8 Grid',
                        '9' => '9 Grid',
                        '10' => '10 Grid',
                        '11' => '11 Grid',
                        '12' => '12 Grid',
                    ),
                    'default'  => '10'
                ),   
                array(
                    'id' => 'post_layout',
                    'type' => 'button_set',
                    'title' => esc_html__('Post Layout', 'glimmer'),
                    'subtitle' => esc_html__('From here you can choose list or grid layout', 'glimmer'),
                    'options' => array(
                        'list' => esc_html__('List', 'glimmer'),
                        'grid' => esc_html__('Grid', 'glimmer'),
                     ), 
                    'default' => 'list',
                ), 
                array(
                    'id' => 'grid_style',
                    'type' => 'button_set',
                    'title' => esc_html__('Grid Style', 'glimmer'),
                    'required' => array("post_layout", "=", "grid"),
                    'subtitle' => esc_html__('From here you can choose grid or masonry layout', 'glimmer'),
                    'options' => array(
                        'grid' => esc_html__('Normal', 'glimmer'),
                        'masonry' => esc_html__('Masonry', 'glimmer'),
                     ), 
                    'default' => 'grid',
                ), 
                array(
                    'id' => 'big_sticky_post',
                    'type' => 'switch',
                    'title' => esc_html__('Full width sticky post', 'glimmer'),
                    'required' => array("post_layout", "=", "grid"),
                    'subtitle' => esc_html__('If you on this option sticky post will show as a big post', 'glimmer'),
                    'default' => 1,
                ), 
                array(
                    'id' => 'grid_column',
                    'type' => 'button_set',
                    'title' => esc_html__('Grid Column', 'glimmer'),
                    'required' => array("post_layout", "=", "grid"),
                    'options' => array(
                        '6' => esc_html__('Two', 'glimmer'),
                        '4' => esc_html__('Three', 'glimmer'),
                        '3' => esc_html__('Four', 'glimmer'),
                     ), 
                    'default' => '6',
                ), 
                array (
                    'id' => 'sidebar_layout_single',
                    'type' => 'image_select',
                    'title' => esc_html__('Single Layout', 'glimmer'),
                    'subtitle' => esc_html__('Default layout of single post', 'glimmer'),
                    'options' => array (
                        'full-width' => array (
                            'alt' => 'empty.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/sidebars/empty.png'
                        ),
                        'sidebar-left' => array(
                            'alt' => 'single-left.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/sidebars/single-left.png'
                        ),
                        'sidebar-right' => array(
                            'alt' => 'single-right.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/sidebars/single-right.png'
                        ),                               
                    ),
                    'default' => 'sidebar-right'
                ),    
                array(
                    'id'       => 'sidebar_layout_single_full_width_column',
                    'type'     => 'select',
                    'title'    => esc_html__( 'Single Layout Full Width Column', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can increase or decrease full width size', 'glimmer' ),
                    'required' => array( 'sidebar_layout_single', '=', 'full-width' ),
                    'options'  => array(
                        '6' => '6 Grid',
                        '7' => '7 Grid',
                        '8' => '8 Grid',
                        '9' => '9 Grid',
                        '10' => '10 Grid',
                        '11' => '11 Grid',
                        '12' => '12 Grid',
                    ),
                    'default'  => '10'
                ),   
                array (
                    'id' => 'sidebar_layout_page',
                    'type' => 'image_select',
                    'title' => esc_html__('Page Layout', 'glimmer'),
                    'subtitle' => esc_html__('Default layout of single page ', 'glimmer'),
                    'options' => array (
                        'full-width' => array (
                            'alt' => 'empty.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/sidebars/empty.png'
                        ),
                        'sidebar-left' => array(
                            'alt' => 'single-left.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/sidebars/single-left.png'
                        ),
                        'sidebar-right' => array(
                            'alt' => 'single-right.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/sidebars/single-right.png'
                        ),                               
                    ),
                    'default' => 'sidebar-right'
                ),
                array(
                    'id'       => 'sidebar_layout_page_full_width_column',
                    'type'     => 'select',
                    'title'    => esc_html__( 'Page Layout Full Width Column', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can increase or decrease full width size', 'glimmer' ),
                    'required' => array( 'sidebar_layout_page', '=', 'full-width' ),
                    'options'  => array(
                        '6' => '6 Grid',
                        '7' => '7 Grid',
                        '8' => '8 Grid',
                        '9' => '9 Grid',
                        '10' => '10 Grid',
                        '11' => '11 Grid',
                        '12' => '12 Grid',
                    ),
                    'default'  => '10'
                ),   
                array (
                    'id' => 'sidebar_layout_archive',
                    'type' => 'image_select',
                    'title' => esc_html__('Archive Layout', 'glimmer'),
                    'subtitle' => esc_html__('Default layout of category, archive, tag page and search page ', 'glimmer'),
                    'options' => array (
                        'full-width' => array (
                            'alt' => 'empty.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/sidebars/empty.png'
                        ),
                        'sidebar-left' => array(
                            'alt' => 'single-left.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/sidebars/single-left.png'
                        ),
                        'sidebar-right' => array(
                            'alt' => 'single-right.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/sidebars/single-right.png'
                        ),                               
                    ),
                    'default' => 'sidebar-right'
                ),
                array(
                    'id'       => 'sidebar_layout_archive_full_width_column',
                    'type'     => 'select',
                    'title'    => esc_html__( 'Archive Layout Full Width Column', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can increase or decrease full width size', 'glimmer' ),
                    'required' => array( 'sidebar_layout_archive', '=', 'full-width' ),
                    'options'  => array(
                        '6' => '6 Grid',
                        '7' => '7 Grid',
                        '8' => '8 Grid',
                        '9' => '9 Grid',
                        '10' => '10 Grid',
                        '11' => '11 Grid',
                        '12' => '12 Grid',
                    ),
                    'default'  => '10'
                ),   
                array(
                    'id' => 'post_layout_archive',
                    'type' => 'button_set',
                    'title' => esc_html__('Post Layout Archive', 'glimmer'),
                    'subtitle' => esc_html__('From here you can choose list or grid layout', 'glimmer'),
                    'options' => array(
                        'list' => esc_html__('List', 'glimmer'),
                        'grid' => esc_html__('Grid', 'glimmer'),
                     ), 
                    'default' => 'list',
                ), 
                array(
                    'id' => 'grid_style_archive',
                    'type' => 'button_set',
                    'title' => esc_html__('Grid Style', 'glimmer'),
                    'required' => array("post_layout_archive", "=", "grid"),
                    'subtitle' => esc_html__('From here you can choose grid or masonry layout', 'glimmer'),
                    'options' => array(
                        'grid' => esc_html__('Normal', 'glimmer'),
                        'masonry' => esc_html__('Masonry', 'glimmer'),
                     ), 
                    'default' => 'grid',
                ), 
                array(
                    'id' => 'grid_column_archive',
                    'type' => 'button_set',
                    'title' => esc_html__('Grid Column', 'glimmer'),
                    'required' => array("post_layout_archive", "=", "grid"),
                    'options' => array(
                        '6' => esc_html__('Two', 'glimmer'),
                        '4' => esc_html__('Three', 'glimmer'),
                        '3' => esc_html__('Four', 'glimmer'),
                     ), 
                    'default' => '6',
                ), 
            )
        ) 
    ); //Layout

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Typography', 'glimmer'),
            //'icon_class' => 'fa-lg',
            'icon' => 'fa fa-font',
            'fields' => array (  
                array (
                    'id'       => 'body-font',
                    'type'     => 'typography',
                    'title'    => esc_html__( 'Body Font', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can Specify the body font properties.', 'glimmer' ),
                    'google'   => true,
                    'color'    => true,
                    'text-align'=> true,
                    'update-weekly'=>false,
                    'line-height'=> true,
                    'subsets'  =>false,
                    'font-style'=> true,
                    'font-backup' => false,
                    'font-size' => true,
                    'font-weight'=>true,
                    'letter-spacing'=>true,
                    'word-spacing'=>true,
                    'all_styles'=>true,                            
                    'units'     => 'em',
                    'output'      => array('body'),
                    'units'   => 'em',
                    'default'  => array(
                        'font-size'   => '0.938em',
                        'line-height'   => '1.45em',
                        'font-family' => 'Lato',
                        'font-weight' => '400',
                    ),
                ),
                array (
                    'id'       => 'all-post-title',
                    'type'     => 'typography',
                    'title'    => esc_html__( 'All Main Post &amp; Page Title', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can Specify the all post and page title font properties.', 'glimmer' ),
                    'google'   => true,
                    'color'    => true,
                    'text-align'=> true,
                    'update-weekly'=>false,
                    'line-height'=> true,
                    'subsets'  =>false,
                    'font-style'=> true,
                    'font-backup' => false,
                    'font-size' => true,
                    'letter-spacing'=>true,
                    'word-spacing'=>true,
                    'font-weight'=>true,
                    'units'     => 'em',
                    'output'      => array('.post .entry-title, .page .entry-title'),
                    'units'   => 'em',
                    'default'  => array(
                        'font-size'   => '1.953em',
                        'line-height'   => '1.4em',
                        'font-family' => 'Lato',
                        'font-weight' => '400',
                    ),
                ), 
                array (
                    'id'       => 'quote-content',
                    'type'     => 'typography',
                    'title'    => esc_html__( 'Block quote content', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can Specify the Block quote content font properties.', 'glimmer' ),
                    'google'   => true,
                    'color'    => false,
                    'text-align'=> false,
                    'update-weekly'=>false,
                    'line-height'=> false,
                    'subsets'  =>false,
                    'font-style'=> false,
                    'font-backup' => false,
                    'font-size' => false,
                    'letter-spacing'=> false,
                    'word-spacing'=> false,
                    'font-weight'=> true,
                    'units'     => 'em',
                    'output'      => array('blockquote'),
                    'units'   => 'em',
                    'default'  => array(
                        'font-family' => 'Playfair Display',
                        'font-weight' => '400italic',
                    ),
                ), 
                array (
                    'id'       => 'heading-one',
                    'type'     => 'typography',
                    'title'    => esc_html__( 'Heading h1 Font', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can Specify the heading h1 font properties.', 'glimmer' ),
                    'google'   => true,
                    'color'    => true,
                    'text-align'=> true,
                    'update-weekly'=>false,
                    'line-height'=> true,
                    'subsets'  =>false,
                    'font-style'=> true,
                    'font-backup' => false,
                    'font-size' => true,
                    'font-weight'=>true,
                    'letter-spacing'=>true,
                    'word-spacing'=>true,
                    'units'     => 'em',
                    'output'      => array('h1'),
                    'units'   => 'em',
                    'default'  => array(
                        'font-size'   => '2.441em',
                        'line-height'   => '1.4em',
                        'font-family' => 'Lato',
                        'font-weight' => '500',
                    ),
                ), 
                array (
                    'id'       => 'heading-two',
                    'type'     => 'typography',
                    'title'    => esc_html__( 'Heading h2 Font', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can Specify the heading h2 font properties.', 'glimmer' ),
                    'google'   => true,
                    'color'    => true,
                    'text-align'=> true,
                    'update-weekly'=>false,
                    'line-height'=> true,
                    'subsets'  =>false,
                    'font-style'=> true,
                    'font-backup' => false,
                    'font-size' => true,
                    'font-weight'=>true,
                    'letter-spacing'=>true,
                    'word-spacing'=>true,
                    'units'     => 'em',
                    'output'      => array('h2'),
                    'units'   => 'em',
                    'default'  => array(
                        'font-size'   => '1.953em',
                        'line-height'   => '1.4em',
                        'font-family' => 'Lato',
                        'font-weight' => '400',
                    ),
                ), 
                array (
                    'id'       => 'heading-three',
                    'type'     => 'typography',
                    'title'    => esc_html__( 'Heading h3 Font', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can Specify the heading h3 font properties.', 'glimmer' ),
                    'google'   => true,
                    'color'    => true,
                    'text-align'=> true,
                    'update-weekly'=>false,
                    'line-height'=> true,
                    'subsets'  =>false,
                    'font-style'=> true,
                    'font-backup' => false,
                    'font-size' => true,
                    'font-weight'=>true,
                    'letter-spacing'=>true,
                    'word-spacing'=>true,
                    'units'     => 'em',
                    'output'      => array('h3'),
                    'units'   => 'em',
                    'default'  => array(
                        'font-size'   => '1.563em',
                        'line-height'   => '1.4em',
                        'font-family' => 'Lato',
                        'font-weight' => '500',
                    ),
                ), 
                array (
                    'id'       => 'heading-four',
                    'type'     => 'typography',
                    'title'    => esc_html__( 'Heading h4 Font', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can Specify the heading h4 font properties.', 'glimmer' ),
                    'google'   => true,
                    'color'    => true,
                    'text-align'=> true,
                    'update-weekly'=>false,
                    'line-height'=> true,
                    'subsets'  =>false,
                    'font-style'=> true,
                    'font-backup' => false,
                    'font-size' => true,
                    'font-weight'=>true,
                    'letter-spacing'=>true,
                    'word-spacing'=>true,
                    'units'     => 'em',
                    'output'      => array('h4'),
                    'units'   => 'em',
                    'default'  => array(
                        'font-size'   => '1.25em',
                        'line-height'   => '1.4em',
                        'font-family' => 'Lato',
                        'font-weight' => '500',
                    ),
                ), 
                array (
                    'id'       => 'heading-five',
                    'type'     => 'typography',
                    'title'    => esc_html__( 'Heading h5 Font', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can Specify the heading h5 font properties.', 'glimmer' ),
                    'google'   => true,
                    'color'    => true,
                    'text-align'=> true,
                    'update-weekly'=>false,
                    'line-height'=> true,
                    'subsets'  =>false,
                    'font-style'=> true,
                    'font-backup' => false,
                    'font-size' => true,
                    'font-weight'=>true,
                    'letter-spacing'=>true,
                    'word-spacing'=>true,
                    'units'     => 'em',
                    'output'      => array('h5'),
                    'units'   => 'em',
                    'default'  => array(
                        'font-size'   => '1em',
                        'line-height'   => '1.4em',
                        'font-family' => 'Lato',
                        'font-weight' => '500',
                    ),
                ), 

                array (
                    'id'       => 'heading-six',
                    'type'     => 'typography',
                    'title'    => esc_html__( 'Heading h6 Font', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can Specify the heading h6 font properties.', 'glimmer' ),
                    'google'   => true,
                    'color'    => true,
                    'text-align'=> true,
                    'update-weekly'=>false,
                    'line-height'=> true,
                    'subsets'  => false,
                    'font-style'=> true,
                    'font-backup' => false,
                    'font-size' => true,
                    'font-weight'=>true,
                    'letter-spacing'=>true,
                    'word-spacing'=>true,
                    'units'     => 'em',
                    'default'  => array(
                        'font-size'   => '0.8em',
                        'line-height'   => '1.4em',
                        'font-family' => 'Lato',
                        'font-weight' => '500',
                    ),
                ),   
            )
        ) 
    ); //Typography

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Color Options', 'glimmer'),
            //'icon_class' => 'fa-lg',
            'icon' => 'fa fa-life-ring',
            'fields' => array ( 
                array(
                    'id' => 'glimmer_color_scheme',
                    'type' => 'radio',
                    'title' => esc_html__('Color Scheme', 'glimmer'),
                    'subtitle' => esc_html__('Select Predefined Color Schemes or your Own', 'glimmer'),
                    'options'  => array(
                        '1' => '<div class="redux-custom-color" style="background: #9272ce;"> </div>',
                        '2' => '<div class="redux-custom-color" style="background: #1ABC9C;"> </div>',
                        '3' => '<div class="redux-custom-color" style="background: #D2527F;"> </div>',
                        '4' => '<div class="redux-custom-color" style="background: #F26D7E;"> </div>',
                        '5' => '<div class="redux-custom-color" style="background: #CC6054;"> </div>',
                        '6' => '<div class="redux-custom-color" style="background: #667A61;"> </div>',
                        '7' => '<div class="redux-custom-color" style="background: #A74C5B;"> </div>',
                        '8' => '<div class="redux-custom-color" style="background: #95A5A6;"> </div>',
                        '9' => '<img src="'.GLIMMER_TEMPLATE_DIR_URL . '/images/color-scheme.png'.'">',
                    ),
                    'default'  => '1'
                ),
                array(
                    'id' => 'glimmer_custom_color',
                    'type' => 'color',
                    'title' => esc_html__('Your Own Theme Color', 'glimmer'),
                    'subtitle' => esc_html__('Glimmer a custom color', 'glimmer'),
                    'default' => '#FFFFFF',
                    'validate' => 'color',
                    'required' => array("glimmer_color_scheme", "=", "9")
                ),
            )
        ) 
    ); //Style  

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Header Section', 'glimmer'),
            //'icon_class' => 'fa-lg',
            'icon' => 'fa fa-laptop',
            'fields' => array(  
                array(
                    'id' => 'header_top_section',
                    'type' => 'switch',
                    'title' => esc_html__('Header Top Section', 'glimmer'),
                    'default' => 1,
                ),
                array(
                    'id'          => 'header_top_right_social_link',
                    'type'        => 'softhopper_slides',
                    'title'       => esc_html__( 'Header top right social link', 'glimmer' ),
                    'show' => array(
                        'title' => true,
                        'description' => false,
                        'url' => true,
                        ),                            
                    'subtitle'    => __( "Font Awesome Icon Class, ex: fa-search. Get the full list <a target='_blank' href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>", 'glimmer' ),
                    'placeholder' => array(
                        'title'       => esc_html__( 'Add font awesome Icon Class', 'glimmer' ),
                        'url'       => esc_html__( 'Social Icon Url', 'glimmer' ),
                    ), 
                    'default' => array(
                        0 => array(
                            'title' => 'fa-facebook',
                            'url' => '#'
                        ),
                        1 => array(
                            'title' => 'fa-twitter',
                            'url' => '#'
                        ),
                        2 => array(
                            'title' => 'fa-google-plus',
                            'url' => '#'
                        ),
                        3 => array(
                            'title' => 'fa-behance',
                            'url' => '#'
                        ),
                        4 => array(
                            'title' => 'fa-pinterest-p',
                            'url' => '#'
                        ),
                    ),                                                                    
                ),                 
                array(
                    'id' => 'header_logo',
                    'type' => 'media',
                    'title' => esc_html__('Header Logo', 'glimmer'),
                    'subtitle' => esc_html__('This image will show as a logo in header banner area', 'glimmer'),
                    'default' => array("url" => GLIMMER_TEMPLATE_DIR_URL . "/images/logo.png"),
                    'preview' => true,
                    'readonly' => false,
                    "url" => true,
                ),
                array(
                    'id'             => 'header_logo_margin',
                    'type'           => 'spacing',
                    'output'         => array('#header-middle #site-logo'),
                    'mode'           => 'margin',
                    'units'          => array('em', 'px'),
                    'units_extended' => 'false',
                    'title'    => esc_html__( 'Header logo margin', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can increase or decrease header logo margin', 'glimmer' ),
                    'default'            => array(
                        'margin-top'     => '0px', 
                        'margin-right'   => '0px', 
                        'margin-bottom'  => '0px', 
                        'margin-left'    => '25px',
                        'units'          => 'px', 
                    )
                ),
                array(
                    'id'             => 'header_area_padding',
                    'type'           => 'spacing',
                    'output'         => array('.site-header #header-middle'),
                    'mode'           => 'padding',
                    'units'          => array('em', 'px'),
                    'units_extended' => 'false',
                    'title'    => esc_html__( 'Header area padding', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can increase or decrease header area padding', 'glimmer' ),
                    'default'            => array(
                        'padding-top'     => '75px', 
                        'padding-right'   => '0px', 
                        'padding-bottom'  => '75px', 
                        'padding-left'    => '0px',
                        'units'          => 'px', 
                    )
                ),
                array (
                    'id'       => 'header_menu_animation',
                    'type'     => 'select',
                    'title'    => esc_html__( 'Menu Animation', 'glimmer' ),
                    'subtitle' => esc_html__( 'There are 5 types of animation in menu. You can change it from here', 'glimmer' ),
                   'options'  => array(
                        'slide' => esc_html__( 'Slide', 'glimmer' ),
                        'fade' => esc_html__( 'Fade', 'glimmer' ),
                        'fadeInUp' => esc_html__( 'FadeInUp', 'glimmer' ),
                        'skew' => esc_html__( 'Skew', 'glimmer' ),
                        'flipIn' => esc_html__( 'FlipIn', 'glimmer' ),
                    ),
                    'default'  => 'slide',
                ),
                array(
                    'id' => 'header_menu_top_left_right',
                    'type' => 'switch',
                    'title' => esc_html__('Header Menu Left, Right', 'glimmer'),
                    'subtitle' => esc_html__('You can show or hide header menu left and right', 'glimmer'),
                    'default' => 0,
                ),
                array(
                    'id' => 'header_main_menu',
                    'type' => 'switch',
                    'title' => esc_html__('Header Main Menu', 'glimmer'),
                    'subtitle' => esc_html__('You can show or hide main menu', 'glimmer'),
                    'default' => 1,
                ),
                array(
                    'id'             => 'header_menu_margin',
                    'type'           => 'spacing',
                    'output'         => array('#header-middle #nav-left, #header-middle #nav-right'),
                    'mode'           => 'margin',
                    'units'          => array('em', 'px'),
                    'units_extended' => 'false',
                    'title'    => esc_html__( 'Header top menu margin', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can increase or decrease header menu margin', 'glimmer' ),
                    'required' => array( 'header_menu_top_left_right', '=', '1' ),
                    'default'            => array(
                        'margin-top'     => '73px', 
                        'margin-right'   => '0px', 
                        'margin-bottom'  => '0px', 
                        'margin-left'    => '0px',
                        'units'          => 'px', 
                    )
                ),
            )
        )
    ); //header

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Featured Post', 'glimmer'),
            //'icon_class' => 'fa-lg',
            'icon' => 'fa fa-star',
            'fields' => array(                       
                array(
                    'id' => 'featured_display',
                    'type' => 'switch',
                    'title' => esc_html__('Display Featured Post', 'glimmer'),
                    'default' => 1,
                ),
                array(
                    'id' => 'featured_display_in_post',
                    'type' => 'switch',
                    'title' => esc_html__('Show Featured Post In Post', 'glimmer'),
                    'subtitle' => esc_html__('If your want you can show featured post in Post. Post show in both place', 'glimmer'),
                    'default' => 0,
                ),
                array(
                    'id'       => 'featured_bg_img',
                    'type'     => 'media',
                    'title'    => esc_html__('Featured post background image', 'glimmer'),
                    'subtitle' => esc_html__('This image will show behind featured post', 'glimmer'),
                    'default'  => array("url" => GLIMMER_TEMPLATE_DIR_URL . "/images/featured-bg.png"),
                    'preview'  => true,
                    'readonly' => false,
                    "url"      => true,
                ),  
                array (
                    'id'       => 'featured_per_page',
                    'type'     => 'slider',
                    'title'    => esc_html__( 'Number of featured post', 'glimmer' ),
                    'subtitle' => esc_html__( 'How many featured post you want to load', 'glimmer' ),
                    'default'       => 5,
                    'min'           => 1,
                    'step'          => 1,
                    'max'           => 20,
                    'display_value' => 'text'
                ),
                array(
                    'id' => 'featured_post_auto_slide',
                    'type' => 'switch',
                    'title' => esc_html__('Featured Post Auto Slide', 'glimmer'),
                    'default' => true,
                ),
                array(
                    'id'            => 'featured_slide_speed',
                    'type'          => 'slider',
                    'title'         => esc_html__('Featured post slide speed', 'glimmer'),
                    'subtitle'      => esc_html__('How fast featured post slide you can modified from here', 'glimmer'),
                    'default'       => 1000,
                    'min'           => 50,
                    'step'          => 10,
                    'max'           => 50000,
                    'display_value' => 'text'
                ),   
                array(
                    'id'            => 'featured_autoplay_timeout',
                    'type'          => 'slider',
                    'title'         => esc_html__('Featured autoplay timeout', 'glimmer'),
                    'subtitle'      => esc_html__('When auto slide remain true you can define here how many times later featured post will slide', 'glimmer'),
                    'default'       => 3000,
                    'min'           => 50,
                    'step'          => 10,
                    'max'           => 50000,
                    'display_value' => 'text'
                ),                      
            )
        )
    ); //featured post

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Post Settings', 'glimmer'),
            //'icon_class' => 'fa-lg',
            'icon' => 'fa fa-file-text',
            'fields' => array (
                array(
                    'id'       => 'excerpt_status',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Excerpt Status', 'glimmer' ),
                    'subtitle' => esc_html__( 'If you off excerpt status always show full post', 'glimmer' ),
                    'default' => 1
                ), 
                array(
                    'id'       => 'post_format_icon',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Post format icon', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can show or hide post format icon', 'glimmer' ),
                    'default' => 1
                ), 
                array(
                    'id'            => 'post_excerpt',
                    'type'          => 'slider',
                    'title' => esc_html__('Default Excerpt Length', 'glimmer'),
                    'subtitle' => esc_html__('How many words you want to show in post excerpt', 'glimmer'),
                    'default'       => 55,
                    'min'           => 1,
                    'step'          => 1,
                    'max'           => 1000,
                    'display_value' => 'text',
                    'required' => array( 'excerpt_status', '=', '1' ),                          
                ),
                array(
                    'id'       => 'tiled_gallery_row_height',
                    'type'     => 'text',
                    'title'    => esc_html__('Tiled Gallery Row Height', 'glimmer'),
                    'subtitle' => esc_html__( 'This is for tiled gallery post', 'glimmer' ),
                    'default'  => "150",
                ),   
            )
        ) 
    ); //Post Settings

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('About Me Page', 'glimmer'),
            'icon' => 'fa fa-user',
            'fields' => array( 
                array(
                    'id'       => 'author_img',
                    'type'     => 'media',
                    'title'    => esc_html__('Author Image', 'glimmer'),
                    'subtitle' => esc_html__('This image will show in the navigation', 'glimmer'),
                    'default'  => array("url" => GLIMMER_TEMPLATE_DIR_URL . "/images/author/author-image.png"),
                    'preview'  => true,
                    'readonly' => false,
                    "url"      => true,
                ),                      
                array(
                    'id'       => 'author_name',
                    'type'     => 'text',
                    'title'    => esc_html__('Author Name', 'glimmer'),
                    'subtitle' => esc_html__( 'This author will show in about page below author image', 'glimmer' ),
                    'default'  => "Johan Smith",
                ), 
                array(
                    'id'      => 'author_description',
                    'type'    => 'editor',
                    'title'   => esc_html__( 'Author description', 'glimmer' ),
                    'default' => '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures.</p>',
                    'args'    => array(
                        'wpautop'       => false,
                        'media_buttons' => false,
                        'textarea_rows' => 5,
                        'teeny'         => false,
                        'quicktags'     => false,
                    )
                ), 
                array (
                    'id' => 'author_sign',
                    'type' => 'media',
                    'title' => esc_html__('Author Singnature', 'glimmer'),
                    'subtitle' => esc_html__('This image will show in the navigation', 'glimmer'),
                    'default' => array("url" => GLIMMER_TEMPLATE_DIR_URL . "/images/author/sign.png"),
                    'preview' => true,
                    'readonly' => false,
                    "url" => true,
                ), 
                array(
                    'id'          => 'author_social_link',
                    'type'        => 'softhopper_slides',
                    'title'       => esc_html__( 'Add Follow Link', 'glimmer' ),
                    'show' => array(
                        'title' => true,
                        'description' => false,
                        'url' => true,
                        ),
                    'subtitle'    => __( "Font Awesome Icon Class, ex: fa-search. Get the full list <a target='_blank' href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>", 'glimmer' ),
                    'placeholder' => array(
                        'title'       => esc_html__( 'Add font awesome Icon Class', 'glimmer' ),
                        'url'       => esc_html__( 'Social Icon Url', 'glimmer' ),
                    ),
                    'default' => array(
                        0 => array(
                            'title' => 'fa-facebook',
                            'url' => '#'
                        ),
                        1 => array(
                            'title' => 'fa-twitter',
                            'url' => '#'
                        ),
                        2 => array(
                            'title' => 'fa-google-plus',
                            'url' => '#'
                        ),
                        3 => array(
                            'title' => 'fa-behance',
                            'url' => '#'
                        ),
                        4 => array(
                            'title' => 'fa-linkedin',
                            'url' => '#'
                        ),
                    ),
                ),                  
            )
        )
    ); //about me

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Contact Page', 'glimmer'),
            'icon' => 'fa fa-phone',
            'fields' => array(
                array(
                    'id' => 'contact_lat',
                    'type' => 'text',
                    'title' => esc_html__('Latitude', 'glimmer'),
                    'subtitle' => __("You can get Latitude and Longitude from <a href='http://www.latlong.net/'>latlong.net</a>", "glimmer"),
                    'default' => "-37.817314",
                ),
                array(
                    'id' => 'contact_lon',
                    'type' => 'text',
                    'title' => esc_html__('Longitude', 'glimmer'),
                    'default' => "144.955431",
                ),
                array(
                    'id' => 'google_map_api',
                    'type' => 'text',
                    'title' => esc_html__('Google Map API', 'glimmer'),
                    'subtitle' => esc_html__('If google map not load, Add your personal google map api key.', 'glimmer'),
                    'default' => "",
                ),          
                array(
                    'id' => 'google_map_embade_code',
                    'type' => 'textarea',
                    'title' => esc_html__('or Google Map Embedded Code', 'glimmer'),
                    'default' => "",
                ),
                array(
                    'id' => 'map_mouse_wheel',
                    'type' => 'switch',
                    'title' => esc_html__('Map Mouse Wheel', 'glimmer'),
                    'default' => true
                ),
                array(
                    'id' => 'map_zoom_control',
                    'type' => 'switch',
                    'title' => esc_html__('Map zoomControl', 'glimmer'),
                    'default' => true
                ),
                array(
                    'id' => 'contact_map_point_img',
                    'type' => 'media',
                    'title' => esc_html__('Contact Map Point Image', 'glimmer'),
                    'subtitle' => esc_html__('This image will show in the google map of your location point', 'glimmer'),
                    'default' => array("url" => GLIMMER_TEMPLATE_DIR_URL . "/images/map-icon.png"),
                    'preview' => true,
                    'readonly' => false,
                    "url" => true,
                ),
                array(
                    'id' => 'contact_subtitle',
                    'type' => 'editor',
                    'title' => esc_html__('Contact Subtitle', 'glimmer'),
                    'default'=> 'We provides a wide array of specialised advisory and<br>Strategic services for our clients.',
                    'args'    => array(                                
                        'textarea_rows' => 5,
                    )
                ),
                array(
                    'id' => 'contact_address_main',
                    'type' => 'text',
                    'title' => esc_html__('Contact Address Main', 'glimmer'),
                    'default'=> "Glimmer Creative Agency, 02 Melborn, Australia"
                ),
                array(
                    'id' => 'contact_description',
                    'type' => 'editor',
                    'title' => esc_html__('Contact Description', 'glimmer'),
                    'default'=> 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum.',
                    'args'    => array(                                
                        'textarea_rows' => 6,
                    )
                ),
                array(
                    'id' => 'contact_address',
                    'type' => 'editor',
                    'title' => esc_html__('Contact Address', 'glimmer'),
                    'default'=>'<dl class="dl-horizontal">
                                    <dt>Phone :</dt>
                                    <dd>+088(017) 1123 - 987654321</dd>
                                    <dt>Fax :</dt>
                                    <dd>+088(142) 564-2225632</dd>
                                    <dt>E-mail :</dt>
                                    <dd><a href="mailto:#">glimmer@domain.net</a></dd>  
                                    <dt>Web :</dt>
                                    <dd><a href="#">http://glimmercompany.net</a></dd>       
                                </dl>',
                ),
                array(
                    'id' => 'contact_form7_shortcode',
                    'type' => 'text',
                    'title' => esc_html__('Contact Form 7 Shortcode', 'glimmer'),
                    'default'=> ""
                ),
                
            )
        )
    ); //contact

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Search Page', 'glimmer'),
            'icon' => 'fa fa-search',
            'fields' => array(                        
                array(
                    'id'       => 'search_only_form_post',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Search only post', 'glimmer' ),
                    'subtitle' => esc_html__( 'If you on this option query search form post. Otherwise query search form post and pages.', 'glimmer' ),
                    'default' => 0
                ),
                array(
                    'id'       => 'search_form_in_search_page',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Search form', 'glimmer' ),
                    'subtitle' => esc_html__( 'Show search form in search page', 'glimmer' ),
                    'default' => 0
                ),                        
            )
        )
    ); //search

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('Footer Section', 'glimmer'),
            'icon' => 'fa fa-edit',
            'fields' => array(  
                array(
                    'id'       => 'footer_widgets_top',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Enable Instagram Feed On Footer', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can off or on footer Instagram feed content', 'glimmer' ),
                    'default'  => true,
                ),
                array(
                    'id'       => 'instagram_title_content',
                    'type'     => 'text',
                    'title'    => esc_html__('Instagram Feed Title', 'glimmer'),
                    'default'  => 'Follow @ Instagram',
                    'required' => array( 'footer_widgets_top', '=', '1' ),
                ),                
                array(
                    'id'       => 'instagram_usernames',
                    'type'     => 'text',
                    'title'    => esc_html__('Instagram User Name', 'glimmer'),
                    'default'  => '',
                    'required' => array( 'footer_widgets_top', '=', '1' ),
                ),   
                array(
                    'id'            => 'instagram_post_limit',
                    'type'          => 'slider',
                    'title' => esc_html__('Instagram Photo Item', 'glimmer'),
                    'subtitle' => esc_html__('How many Instagram photo you want to show', 'glimmer'),
                    'default'       => 8,
                    'min'           => 1,
                    'step'          => 1,
                    'max'           => 100,
                    'display_value' => 'text',
                    'required' => array( 'footer_widgets_top', '=', '1' ),                          
                ),                 
                array(
                    'id'            => 'instagram_item_columns',
                    'type'          => 'slider',
                    'title' => esc_html__('Instagram Column', 'glimmer'),
                    'subtitle' => esc_html__('How many Instagram photo column you want to show', 'glimmer'),
                    'default'       => 6,
                    'min'           => 1,
                    'step'          => 1,
                    'max'           => 12,
                    'display_value' => 'text',
                    'required' => array( 'footer_widgets_top', '=', '1' ),                          
                ), 
                array(
                    'id'       => 'instagram_image_size',
                    'type'     => 'select',
                    'title'    => esc_html__( 'Instagram Image Size', 'glimmer' ),
                    'required' => array( 'footer_widgets_top', '=', '1' ),
                    //Must provide key => value pairs for select options
                    'options'  => array(
                        'thumbnail' => esc_html__('Thumbnail', 'glimmer' ),
                        'small' => esc_html__('Small', 'glimmer' ),
                        'large' => esc_html__('Large', 'glimmer' ),
                        'original' => esc_html__('Original', 'glimmer' ),
                    ),
                    'default'  => 'small'
                ),                 
                array(
                    'id'       => 'instagram_image_open',
                    'type'     => 'select',
                    'title'    => esc_html__( 'Instagram Image Open In', 'glimmer' ),
                    'required' => array( 'footer_widgets_top', '=', '1' ),
                    //Must provide key => value pairs for select options
                    'options'  => array(
                        '_self' => esc_html__('Current window (_self)', 'glimmer' ),
                        '_blank' => esc_html__('New window (_blank)', 'glimmer' ),
                    ),
                    'default'  => '_blank'
                ),               
                array(
                    'id'       => 'footer_widgets_bottom',
                    'type'     => 'switch',
                    'title'    => esc_html__( 'Enable Footer Bottom Widget', 'glimmer' ),
                    'subtitle' => esc_html__( 'You can off or on footer widgets', 'glimmer' ),
                    'default'  => true,
                ),
                array (
                    'id' => 'footer_widgets_bottom_columns',
                    'type' => 'image_select',
                    'required' => array( 'footer_widgets_bottom', '=', '1' ),
                    'title' => esc_html__('Footer Bottom Widget Layout', 'glimmer'),
                    'subtitle' => esc_html__('How many sidebar you want to show on footer', 'glimmer'),
                    'options' => array (
                        '1' => array (
                            'alt' => 'one-column.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/footer/one-column.png'
                        ),
                        '2' => array(
                            'alt' => 'two-columns.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/footer/two-columns.png'
                        ),
                        '3' => array(
                            'alt' => 'three-columns.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/footer/three-columns.png'
                        ), 
                        '4' => array(
                            'alt' => 'four-columns.png',
                            'img' => GLIMMER_TEMPLATE_DIR_URL . '/images/backend/footer/four-columns.png'
                        ),                               
                    ),
                    'default' => '3'
                ),                
                array(
                    'id'      => 'footer_copyright_info',
                    'type'    => 'editor',
                    'title'   => esc_html__( 'Footer Copyright Info', 'glimmer' ),
                    'default' => 'Copyright&copy; 2015 Softhopper. All right reserved.',
                    'args'    => array(
                        'wpautop'       => false,
                        'media_buttons' => false,
                        'textarea_rows' => 5,
                        //'tabindex' => 1,
                        //'editor_css' => '',
                        'teeny'         => false,
                        //'tinymce' => array(),
                        'quicktags'     => false,
                    )
                ),
            )
        )
    ); //footer

    Redux::setSection( $opt_name, array(
            'title' => esc_html__('404 Settings', 'glimmer'),
            //'icon_class' => 'fa-lg',
            'icon' => 'fa fa-question-circle',                
            'fields' => array(
                array(
                    'id' => '404_img',
                    'type' => 'media',
                    'title' => esc_html__('404  Image', 'glimmer'),
                    'subtitle' => esc_html__('This image will show in 404 page', 'glimmer'),
                    'default' => array("url" => GLIMMER_TEMPLATE_DIR_URL . "/images/404.png"),
                    'readonly' => false,
                    'preview' => true,
                    "url" => true,
                ),
            )
        )
    ); //404   


    /*
     * <--- END SECTIONS
     */


    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => esc_html__( 'Section via hook', 'glimmer' ),
                'desc'   => '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>',
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }