<?php
/**
 * Glimmer functions and definitions
 *
 * @package Glimmer
 */

/**
 * Define theme's constant
 */
if ( ! defined( 'GLIMMER_THEME_VERSION' ) ) {
	define( 'GLIMMER_THEME_VERSION', '3.0' );
}
if ( ! defined( 'GLIMMER_TEMPLATE_DIR' ) ) {
	define( 'GLIMMER_TEMPLATE_DIR', get_template_directory() );
}
if ( ! defined( 'GLIMMER_TEMPLATE_DIR_URL' ) ) {
	define( 'GLIMMER_TEMPLATE_DIR_URL', get_template_directory_uri() );
}


/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
if ( ! function_exists( 'glimmer_setup' ) ) :

function glimmer_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Glimmer, use a find and replace
	 * to change 'glimmer' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'glimmer', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'header-menu-left' => esc_html__( 'Header Menu Left', 'glimmer' ),
		'header-menu-right' => esc_html__( 'Header Menu Right', 'glimmer' ),
		'main-menu' => esc_html__( 'Main Menu', 'glimmer' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
		) 
	);
	/* Define image size */
	add_image_size( 'glimmer-featured-img', 460, 290, true );
	add_image_size( 'glimmer-single-full', 1170, 560, true );
	add_image_size( 'glimmer-single-grid-full', 450, 295, true );
	add_image_size( 'glimmer-single-list', 800, 470, true );
	add_image_size( 'glimmer-single-grid', 370, 250, true );
	add_image_size( 'glimmer-gallery-small', 170, 115, true );
	add_image_size( 'glimmer-small-img', 66, 66, true );
	add_image_size( 'glimmer-related-posts', 370, 250, true );
	
	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'image', 'gallery', 'audio', 'video', 'quote', 'link', 'aside', 'status', 'chat'
		) 
	);
        
	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'glimmer_custom_background_args', array (
		'default-color' => 'f2f2f2',
		'default-image' => '',
	) ) );

	/** 
	 * Enable WP Responsive embedded content
	 *1
	 * @since 3.0
	 */
	add_theme_support( 'responsive-embeds' );

	/** 
	 * Enable WP Gutenberg Align Wide
	 *
	 * @since 3.0
	 */
	add_theme_support( 'align-wide' );


	/** 
	 * Enable selective refresh for widgets.
	 *
	 * @since 3.0
	 */
	add_theme_support( 'customize-selective-refresh-widgets' );

	/** 
	 * Enable WP Gutenberg Block Style
	 *
	 * @since 3.0
	 */
	add_theme_support( 'wp-block-styles' );

	/**
	 * Add Editor Style
	 *
	 * @since 3.0
	 */
	// Add support for editor styles.
	add_theme_support( 'editor-styles' );

	/**
	 * Enable support for custom Editor Style.
	 *
	 * @since 3.0
	 */
	add_editor_style( 'editor-style.css' );

	/**
	 * Enable fonts Google font family
	 *
	 * @since 3.0
	 */
	// Enqueue fonts in the editor.
	add_editor_style( glimmer_google_fonts_url() );
}
endif; // glimmer_setup
add_action( 'after_setup_theme', 'glimmer_setup' );

function glimmer_google_fonts_url() {
    $font_url = '';
    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== esc_html_x( 'on', 'Google font: on or off', 'glimmer' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Lato:300,300i,400,400i,700,700i,900,900i|Playfair Display:400,400i,700,700i' ), "https://fonts.googleapis.com/css" );
    }
    return $font_url;
}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
if ( ! isset( $content_width ) ) {
	$content_width = 960; /* pixels */
}

/**
 * Include the Redux theme options framework
 */
//Include the Redux theme options framework extension
require GLIMMER_TEMPLATE_DIR . '/inc/libs/redux-framework/redux-extensions-loader/loader.php';

if ( !isset( $redux_demo ) ) {
    require GLIMMER_TEMPLATE_DIR . '/inc/libs/redux-framework/redux-framework-config.php';
}

/*
 * To get redux option value
*/
if ( ! function_exists( 'softhopper_glimmer' ) ) :
	function softhopper_glimmer($option, $arr = null) {
		global $softhopper_glimmer;
		if ($arr) {
		    if ( isset($softhopper_glimmer[$option][$arr]) ) {
				return $softhopper_glimmer[$option][$arr];
		    }
	    } else {
		    if ( isset($softhopper_glimmer[$option]) ) {
		   		return $softhopper_glimmer[$option];
		    }
	    }
	}
endif;

/**
 * Query function to get post
 */
require GLIMMER_TEMPLATE_DIR . '/inc/functions/theme-functions.php';

/**
 * Include Register widget function
 */
require GLIMMER_TEMPLATE_DIR . '/inc/functions/register-widgets.php';

/**
 * Enqueue scripts and styles function
 */
require GLIMMER_TEMPLATE_DIR . '/inc/functions/enqueue-scripts.php';

/**
 * Custom template tags for this theme.
 */
require GLIMMER_TEMPLATE_DIR . '/inc/functions/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require GLIMMER_TEMPLATE_DIR . '/inc/functions/extras.php';

/**
 * Customizer additions.
 */
require GLIMMER_TEMPLATE_DIR . '/inc/functions/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require GLIMMER_TEMPLATE_DIR . '/inc/functions/jetpack.php';

/**
 * Include the TGM_Plugin_Activation class.
 */
require GLIMMER_TEMPLATE_DIR . '/inc/libs/tgm-plugin-activation/tgm-admin-config.php';

/**
 * Configure CMB2 Meta Box
 */
require GLIMMER_TEMPLATE_DIR . '/inc/libs/cmb2/cmb2-config.php';

/**
 * Wordpress comment seciton override 
 */
require GLIMMER_TEMPLATE_DIR . '/inc/functions/wp-comment-section-override.php';

/**
 * Query function to get post
 */
require GLIMMER_TEMPLATE_DIR . '/inc/functions/function-for-post.php';

/**
 * Popular Post functions
 */
require GLIMMER_TEMPLATE_DIR . '/inc/functions/popular-post.php';

/**
 * Include header, Hooks for template header
 */
require GLIMMER_TEMPLATE_DIR . '/inc/frontend/header.php';

/**
 * Include header, Hooks for template header
 */
require GLIMMER_TEMPLATE_DIR . '/inc/frontend/footer.php';

/**
 * Include chat-post-modify
 */
require GLIMMER_TEMPLATE_DIR . '/inc/functions/chat-post-modify.php';

/**
 * Include override functions
 */
require GLIMMER_TEMPLATE_DIR . '/inc/functions/wordpress-override.php';
