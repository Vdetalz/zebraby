<?php
/**
 * The template for displaying featured posts
 *
 * Used for index.
 *
 * @package Glimmer
 */
?>
<?php 
if ( softhopper_glimmer('featured_display') == 1 ) :
if ( !is_archive() && !is_paged() && !is_search() ) : ?>
<!-- Featured Area
================================================== -->
<?php if ( class_exists( 'GlimmerFeatured_Post' ) ) { ?>
    <div id="featured" class="feature-area">
        <div id="featured-content">
            <div class="container">
                <div class="row">       
                    <div id="featured-slider" class="owl-carousel">               
                        <?php
                            $query_args = array();

                            $max =  softhopper_glimmer('featured_per_page') !== NULL ? softhopper_glimmer('featured_per_page') : 4;

                            if( $max ) {
                                $query_args['posts_per_page'] = $max;
                            }

                            $query_args['post_status'] = array( 'publish', 'private' );
                            $query_args['has_password'] = false;
                            $query_args['ignore_sticky_posts'] = true;
                            $query_args['paged'] = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;;
                            $query_args['featured'] = 'yes';

                            $query = new WP_Query( $query_args );

                            while ( $query->have_posts() ) : $query->the_post();
                        ?>
                        <div class="item">
                            <?php
                                $post_format = get_post_format();
                                $meta = get_post_meta( $post->ID );
                                if ( false === $post_format ) {
                                    $post_format = "standard";
                                }
                            ?>
                            <div class="post-thumb">
                                <?php
                                    if( $post_format == 'video' ) {
                                        echo '<div class="post-media">';
                                        glimmer_vedio(); 
                                        echo '</div>';
                                    } else {
                                        if ( $post_format == "gallery" ) {  
                                            // show first image of gallery post
                                            if ( isset ( $meta["_glimmer_format_gallery"][0] ) ) {
                                                $images = get_post_meta( get_the_ID(), '_glimmer_format_gallery', true);
                                                if ( $images ) {
                                                  $i = 1;
                                                  foreach ( $images as $attachment_id => $img_full_url ) {
                                                   if ( $i == 2 ) continue;
                                                   $image_with_link = wp_get_attachment_link($attachment_id, 'glimmer-featured-img');
                                                        echo esc_url( $image_with_link );
                                                    $i++;
                                                  }
                                                }                          
                                            }
                                        } elseif ( $post_format == "audio" ){
                                            if( isset($meta["_glimmer_format_audio_bg_img"][0]) ) {
                                                ?>
                                                <?php
                                                echo wp_get_attachment_image( get_post_meta( get_the_ID(), '_glimmer_format_audio_bg_img_id', 1 ), 'glimmer-featured-img' );
                                                ?>
                                                <?php
                                            }
                                        } elseif ( $post_format == "quote" ){
                                            
                                        } elseif ( $post_format == "aside" ){
                                             
                                        } elseif ( $post_format == "chat" ){
                                            if ( has_post_thumbnail() ) {
                                                the_post_thumbnail('glimmer-featured-img', array( 'alt' => get_the_title()));
                                            }
                                        } elseif ( $post_format == "link" ){
                                            if( isset($meta["_glimmer_format_link_bg_img"][0]) ) {
                                                ?>
                                                <?php
                                                echo wp_get_attachment_image( get_post_meta( get_the_ID(), '_glimmer_format_link_bg_img_id', 1 ), 'glimmer-featured-img' );
                                                ?>
                                                <?php
                                            }
                                        } elseif ( $post_format == "status" ){
                                            if( isset($meta["_glimmer_format_status_bg"][0]) ) {
                                                ?>
                                                <?php
                                                echo wp_get_attachment_image( get_post_meta( get_the_ID(), '_glimmer_format_status_bg_id', 1 ), 'glimmer-featured-img' );
                                                ?>
                                                <?php
                                            }
                                        } else {
                                            if ( has_post_thumbnail() ) { ?>
                                                    <?php
                                                        the_post_thumbnail('glimmer-featured-img', array( 'alt' => get_the_title()));
                                                    ?>
                                                <?php
                                            }
                                        }
                                    }
                                ?>
                            </div>
                            <div class="post-content">
                                <?php       
                                    the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                                ?>

                                <div class="entry-meta">
                                    <span class="cat-links">
                                        <?php
                                            $categories = get_the_category(); //get all categories for this post
                                            $cat_no =  count( $categories );
                                            if ( $cat_no > 1 ) {
                                                esc_html_e('In: ','glimmer');
                                                $cat_counter = 1;
                                                foreach( $categories as $categorie ):
                                                    $cat_counter++;
                                                    ( $cat_counter == $cat_no ) ? $cat_separator = "" : $cat_separator = ", ";
                                                    echo '<a href="' . get_category_link( $categorie->term_id ) . '" ' . '>' . $categorie->name.'</a>'.$cat_separator;
                                                endforeach;  
                                            } else {
                                                esc_html_e('In: ','glimmer').the_category( ', ' );
                                            }
                                        ?>
                                    </span>
                                    <span class="devider">/</span>
                                    <span class="entry-date">
                                        <?php the_time( get_option( 'date_format' ) ); ?>
                                    </span>
                                </div> <!-- .entry-meta -->
                                <div class="more-link-wrap">
                                    <a href="<?php the_permalink(); ?>" class="more-link"><?php esc_html_e('Continue', 'glimmer'); ?></a>
                                </div>        
                            </div> <!-- /.post-content -->
                        </div> <!-- /.item --> 
                        <?php 
                            endwhile;
                            wp_reset_postdata(); 
                        ?>
                    </div> <!-- /#featured-slider  --> 
                </div>  <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- #featured-content -->

        <div class="ts-background" data-bg-parallax="scroll" data-bg-parallax-speed="3">
            <div class="ts-background-image ts-parallax-element" data-bg-image="<?php if ( softhopper_glimmer('featured_bg_img','url') !== '' ) echo esc_url(softhopper_glimmer('featured_bg_img','url') ); ?>"></div><!--  /.hg-background-image -->
        </div><!--  /.glimmer-background -->
    </div> <!-- /#featured -->
<?php } ?>
<?php endif; endif; ?>