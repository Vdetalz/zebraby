<?php
/**
 * The template for displaying chat post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
    global $softhopper_glimmer;
    (!has_post_thumbnail() ) ? $image_class = "no-image": $image_class = "";
?>
<div class="<?php echo esc_attr( glimmer_theme_check_list_grid() ); ?>">
    <article id="post-<?php the_ID(); ?>" <?php post_class($image_class); ?>>
        <?php
	        if ( has_post_thumbnail() ) {
            ?>
            <figure class="post-thumb">
                <?php 
                if ( is_single() ) {
                    $meta = get_post_meta( $post->ID );
                    if ( isset($meta["_glimmer_custom_layout"][0]) ) {
                        ( $meta["_glimmer_layout"][0] == 'full-content' ) ? $image_size = "glimmer-single-full" : $image_size = "glimmer-single-list" ;
                    } else {
                        ( $softhopper_glimmer['sidebar_layout_single'] == 'full-width' ) ? $image_size = "glimmer-single-full" : $image_size = "glimmer-single-list" ;
                    }
                    the_post_thumbnail( $image_size, array( 'class' => " img-responsive", 'alt' => get_the_title() ));
                } else {
                    ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php                                
                            glimmer_cropping_image_size();
                            global $image_size;

                            the_post_thumbnail( $image_size, array( 'class' => " img-responsive", 'alt' => get_the_title() ));
                        ?>
                    </a>
                    <?php
                }
                ?>
            </figure> <!-- /.post-thumb -->
            <?php
            }
        ?>

        <header class="entry-header">            
            <?php glimmer_entry_header(); ?>
        </header> <!-- /.entry-header -->

        <script type="text/javascript">
			jQuery( window ).load(function() {
				jQuery('.format-chat .chat-text p:contains("more-link")').parent().parent().css('display', 'none');  
			});	
	    </script>

        <div class="entry-content">
        <?php 
            ob_start(); 
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
                    'after'  => '</div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                ) );
            $post_pagination = ob_get_clean();

            if ( is_single() ) {
                the_content(); 
                echo wp_kses_post( $post_pagination );
                edit_post_link( esc_html__( '(Edit Post)', 'glimmer' ), '<span class="edit-link">', '</span>' );
                glimmer_tag_list();
            } else {
                if ( has_excerpt() ) {
                    the_excerpt();
                } else {
                    the_content();
                }
                echo '<div id="readmore-add"></div>';
                echo wp_kses_post( $post_pagination );
            }
        ?>  
        </div> <!-- .entry-content -->

        <footer class="entry-footer clearfix">                          
            <?php glimmer_entry_footer(); ?>
        </footer> <!-- .entry-footer -->
    </article> <!-- /.post-->
</div> <!-- /.col-md-12 -->
