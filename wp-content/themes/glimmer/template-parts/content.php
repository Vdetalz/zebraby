<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
 	$post_format = get_post_format();
	$meta = get_post_meta( $post->ID );
	if ( false === $post_format ) {
		$post_format = "standard";
	}
    (!has_post_thumbnail() ) ? $image_class = "no-image": $image_class = "";
?>
<div class="<?php echo esc_attr( glimmer_theme_check_list_grid() ); ?>">
    <article id="post-<?php the_ID(); ?>" <?php post_class($image_class); ?>>
        <?php
            if ( has_post_thumbnail() ) { ?>
            <figure class="post-thumb">
                <?php 
                if ( is_single() ) {
                    $meta = get_post_meta( $post->ID );
                    if ( isset($meta["_glimmer_custom_layout"][0]) ) {
                        ( $meta["_glimmer_layout"][0] == 'full-content' ) ? $image_size = "glimmer-single-full" : $image_size = "glimmer-single-list" ;
                    } else {
                        ( softhopper_glimmer('sidebar_layout_single') == 'full-width' ) ? $image_size = "glimmer-single-full" : $image_size = "glimmer-single-list" ;
                    }
                    the_post_thumbnail( $image_size, array( 'class' => " img-responsive", 'alt' => get_the_title() ));
                } else { ?>
                    <a href="<?php the_permalink(); ?>">
                        <?php                                
                            glimmer_cropping_image_size();
                            global $image_size;
                            the_post_thumbnail( $image_size, array( 'class' => " img-responsive", 'alt' => get_the_title() ));
                        ?>
                    </a>
                <?php } ?>
            </figure> <!-- /.post-thumb -->
            <?php }
        ?>

        <header class="entry-header">            
            <?php glimmer_entry_header(); ?>
        </header> <!-- /.entry-header -->

        <div class="entry-content">
            <?php 
                ob_start(); 
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
                    'after'  => '</div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                ) );
                $post_pagination = ob_get_clean();

                if ( is_single() ) {
                    the_content(); 
                    echo wp_kses_post( $post_pagination );
                    glimmer_tag_list();
                } else {
                    echo glimmer_excerpt( softhopper_glimmer('post_excerpt') );
                    echo wp_kses_post( $post_pagination );
                }
            ?> 
            
            <?php if( !is_single() ) : ?>
                <?php if( is_archive() || is_search() ) {
                    $more_link_condition = ( softhopper_glimmer('post_layout_archive') == 'list' ) ? true : false;
                } else {
                    $more_link_condition = ( softhopper_glimmer('post_layout') == "list" || is_sticky() ) ? true : false;
                } ?>
                <?php if( $more_link_condition == true  ) { ?>
                    <div class="more-link-block text-center">
                        <a href="<?php the_permalink(); ?>" class="more-link main-more-btn" title="<?php esc_attr_e('Continue Reading', 'glimmer'); ?>"><?php esc_html_e( 'Continue Reading', 'glimmer' ); ?></a>
                    </div><!-- .more-link -->
                <?php } ?>
            <?php endif; ?>
        </div> <!-- .entry-content -->
        
        <footer class="entry-footer clearfix"> 
            <?php if(is_single()) {
                glimmer_entry_footer_single();
            } else {
                glimmer_entry_footer();
            } ?>
        </footer> <!-- .entry-footer -->
    </article> <!-- /.post-->
</div> <!-- /.col-md-12 -->