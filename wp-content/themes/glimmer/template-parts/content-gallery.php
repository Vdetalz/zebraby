<?php
/**
 * The template for displaying gallery post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
global $softhopper_glimmer;
$meta = get_post_meta( $post->ID );
    if ( isset ( $meta["_glimmer_format_gallery"][0] ) ) {
        $gallery_img = $meta["_glimmer_format_gallery"][0];                            
    }  else {
        $gallery_img = '';
    } 
    ( $gallery_img == '' ) ? $image_class = "no-image" : $image_class = "";
?>
<div class="<?php echo glimmer_theme_check_list_grid(); ?>">
    <article id="post-<?php the_ID(); ?>" <?php post_class($image_class); ?>>
        
        <?php if ( !empty( $meta["_glimmer_format_gallery"][0] ) ) : ?>	
		<figure class="post-thumb">
			<?php        
				glimmer_cropping_image_size();
                global $image_size;
				 
				// if gallery style one load this script
				$meta = get_post_meta( $post->ID );
		    	( isset( $meta["_glimmer_gallery_style"][0] ) ) ? $gallery_style = $meta["_glimmer_gallery_style"][0] : $gallery_style = "" ; 
		    	if ( $gallery_style == "gallery-one"  ) {
		    	?>
		            <div class="gallery-one owl-carousel">
    	                <?php
    						// this is to get meta field image
    					    $images = get_post_meta( get_the_ID(), '_glimmer_format_gallery', true);
    				        if ( $images ) {
    				          	foreach ( $images as $attachment_id => $img_full_url ) {
    					           	$full = wp_get_attachment_image($attachment_id, $image_size);
    					           	$full_src = wp_get_attachment_image_src($attachment_id, 'full');
    					           	echo '<a class="item" href="'.esc_url( $full_src[0] ).'">';
    					            echo wp_kses_post( $full );
    					            echo '</a>';
    				            }
    				        }  
    					?> 
    	            </div> <!-- /.gallery-one -->
		    	<?php                      
		    	}  // end if;
		    ?>
		    <?php         
				// if gallery style three load this script
		    	if ( $gallery_style == "gallery-three"  ) {
		    	?>
		            <div class="glimmer-tiled-gallery">
		                <?php              
		                    if ( isset ( $meta["_glimmer_format_gallery"][0] ) ) {
		                        $imgs_urls = $meta["_glimmer_format_gallery"][0];                            
		                    }  else {
		                        $imgs_urls = '';
		                    }        
		                    $imgs_url = explode( '"', $imgs_urls );
		                    for ( $x = 0; $x < count ( $imgs_url ); $x++ ) {

		                    	// if grid layout and tiled gallery just show first image
		                    	if ( !is_single() ) {
							        if( is_archive() ) {
							            if( $softhopper_glimmer['post_layout_archive'] == 'grid' ) {
							            	if( $x == 2 ) {
					                    		break;
					                    	}
							            }
							        } else {
							            if( $softhopper_glimmer['post_layout'] == 'grid' ) {
							            	if( $x == 2 ) {
					                    		break;
					                    	}
							            }
							        }
							    }	                    	

		                    	if($x % 2 != 0) { ?>
		                       		<a class="item" href="<?php if(isset($imgs_url[$x])) echo esc_url( $imgs_url[$x] ); ?>">
						                <img src="<?php if(isset($imgs_url[$x])) echo esc_url($imgs_url[$x]); ?>" alt="<?php the_title(); ?>">
						            </a>
		                        	<?php                  		
		                        } // end if
		                    } // end for
		                ?>  
		            </div> <!-- /.gallery-one -->
		    	<?php                      
		    	}  // end if;
		    ?>
		    <?php     
		    	// if gallery style two load this script
		    	if ( $gallery_style == "gallery-two"  ) {
		    	?>
		            <div class="gallery-two">
	                    <div class="full-view owl-carousel">
		                <?php
    						// this is to get meta field image
    					    $images = get_post_meta( get_the_ID(), '_glimmer_format_gallery', true);
    				        if ( $images ) {
    				          	foreach ( $images as $attachment_id => $img_full_url ) {
    					           	$full = wp_get_attachment_image($attachment_id, $image_size);
    					           	$full_src = wp_get_attachment_image_src($attachment_id, 'full');
    					           	echo '<a class="item" href="'.esc_url($full_src[0]).'">';
    					            echo wp_kses_post( $full );
    					            echo '</a>';
    				            }
    				        }  
    					?> 
		            	</div> <!-- /.full-view -->
		    	<?php                      
		    	}  // end if;
		    ?>
		    <?php         
		    	if ( $gallery_style == "gallery-two"  ) {
		    	?>
		            <div class="list-view owl-carousel">
		                <?php
    						// this is to get meta field image
    					    $images = get_post_meta( get_the_ID(), '_glimmer_format_gallery', true);
    				        if ( $images ) {
    				          	foreach ( $images as $attachment_id => $img_full_url ) {
    					           	$full = wp_get_attachment_image($attachment_id, 'glimmer-gallery-small');
    					           	echo '<div class="item" >';
    					            echo wp_kses_post( $full );
    					            echo '</div>';
    				            }
    				        }  
    	                ?>  
		            </div>  <!-- /.list-view -->
				</div> <!-- /.gallery-two -->
		    	<?php                      
		    	}  // end if;
		    ?>
		</figure> <!-- /.post-thumb -->
		<?php
			endif;
		?>

        <header class="entry-header">            
            <?php glimmer_entry_header(); ?>
        </header> <!-- /.entry-header -->

        <div class="entry-content">
        <?php 
            ob_start(); 
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
                    'after'  => '</div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                ) );
            $post_pagination = ob_get_clean();

            if ( is_single() ) {
                the_content(); 
                echo wp_kses_post( $post_pagination );
                edit_post_link( esc_html__( '(Edit Post)', 'glimmer' ), '<span class="edit-link">', '</span>' );
                glimmer_tag_list();
            } else {
                if ( has_excerpt() ) {
                    the_excerpt();
                } else {
                    the_content();
                }
                echo wp_kses_post( $post_pagination );
            }
        ?>  
        </div> <!-- .entry-content -->

        <footer class="entry-footer clearfix">                          
            <?php glimmer_entry_footer(); ?>
        </footer> <!-- .entry-footer -->
    </article> <!-- /.post-->
</div> <!-- /.col-md-12 -->