<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
	global $softhopper_glimmer;
?>
<div class="<?php echo glimmer_theme_check_list_grid(); ?>">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php
		 	$meta = get_post_custom($post->ID);
			$soundcloud = isset( $meta["_glimmer_format_audio_soundcloud"][0] ) ? $meta["_glimmer_format_audio_soundcloud"][0] : '';
			if( !empty( $soundcloud ) ) {
				?>
				<div class="post-media">						
				<?php
					echo glimmer_soundcloud( $soundcloud );
				?>
				</div>
				<?php
			} else { ?>
				<div class="post-media">
					<div class="audio-images">
	                <?php
                    glimmer_cropping_image_size();
                    global $image_size;

                    if ( is_single() ) {
                        $meta = get_post_meta( $post->ID );
                        if ( isset($meta["_glimmer_custom_layout"][0]) ) {
                            ( $meta["_glimmer_layout"][0] == 'full-content' ) ? $image_size = "glimmer-single-full" : $image_size = "glimmer-single-list" ;
                        } else {
                            ( $softhopper_glimmer['sidebar_layout_single'] == 1 ) ? $image_size = "glimmer-single-full" : $image_size = "glimmer-single-list" ;
                        }
                        if ( isset($meta["_glimmer_format_audio_bg_img"][0]) ) {
                            echo wp_get_attachment_image( get_post_meta( get_the_ID(), '_glimmer_format_audio_bg_img_id', 1 ), $image_size );
                        }
                    } else {
                        ?>
                        <a href="<?php the_permalink(); ?>">
                            <?php 
                                if ( isset($meta["_glimmer_format_audio_bg_img"][0]) ) {
                                    echo wp_get_attachment_image( get_post_meta( get_the_ID(), '_glimmer_format_audio_bg_img_id', 1 ), $image_size );
                                }
                            ?>
                        </a>
                        <?php
                    }
                    ?>
	                </div>						
				<?php
					$audio_file = isset( $meta["_glimmer_format_audio_file"][0] ) ? $meta["_glimmer_format_audio_file"][0] : "" ;
                    $format_audio_type = wp_check_filetype($audio_file);
                    $mp3 = ( $format_audio_type['ext'] == "mp3" ) ? $meta["_glimmer_format_audio_file"][0] : '';
                    $ogg = ( $format_audio_type['ext'] == "ogg" ) ? $meta["_glimmer_format_audio_file"][0] : '';
                    $m4a = ( $format_audio_type['ext'] == "m4a" ) ? $meta["_glimmer_format_audio_file"][0] : '';
                    echo '<div class="post-audio-player">'.do_shortcode('[audio mp3="'.$mp3.'" ogg="'.$ogg.'" m4a="'.$m4a.'"]').'</div>';
				?>
				</div>
			<?php
			}
		?>

        <header class="entry-header">            
            <?php glimmer_entry_header(); ?>
        </header> <!-- /.entry-header -->

        <div class="entry-content">
        <?php 
            ob_start(); 
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
                    'after'  => '</div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                ) );
            $post_pagination = ob_get_clean();

            if ( is_single() ) {
                the_content(); 
                echo wp_kses_post( $post_pagination );
                edit_post_link( esc_html__( '(Edit Post)', 'glimmer' ), '<span class="edit-link">', '</span>' );
                glimmer_tag_list();
            } else {
                if ( has_excerpt() ) {
                    the_excerpt();
                } else {
                    the_content();
                }
                echo wp_kses_post( $post_pagination );
            }
        ?>  
        </div> <!-- .entry-content -->

        <footer class="entry-footer clearfix">                          
            <?php glimmer_entry_footer(); ?>
        </footer> <!-- .entry-footer -->
    </article> <!-- /.post-->
</div> <!-- /.col-md-12 -->