<?php
/**
 * The template for displaying page content.
 *
 * @package Glimmer
 */
 global $softhopper_glimmer;
 $meta = get_post_meta( $post->ID );
?>
<div class="col-md-12 full-width">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php 
			if ( has_post_thumbnail() ) {
	        ?>
	        <figure class="post-thumb">
				<?php
					if ( isset($meta["_glimmer_custom_layout"][0]) ) {
					    ( $meta["_glimmer_layout"][0] == 'full-content' ) ? $image_size = "glimmer-single-full" : $image_size = "glimmer-single-list" ;
					} else {
					    ( $softhopper_glimmer['sidebar_layout_page'] == 'full-width' ) ? $image_size = "glimmer-single-full" : $image_size = "glimmer-single-list" ;
					}
					the_post_thumbnail( $image_size, array( 'class' => " img-responsive", 'alt' => get_the_title() ));
		        ?>
			</figure> <!-- /.post-thumb -->
	        <?php
	        }
		?>

		<header class="entry-header">
			<?php glimmer_page_entry_header(); ?>
		</header>
		

		<div class="entry-content">
			<?php 
				the_content();
				edit_post_link( esc_html__( '(Edit Page)', 'glimmer' ), '<span class="edit-link">', '</span>' ); 
				
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
					'after'  => '</div>',
				) );
			?>
		</div> <!-- .entry-content -->	    
	</article> <!-- /.post-->
</div> <!-- /.col-md-12 -->