<?php 
/**
 * The template for displaying author page.
 *
 * @package Glimmer
 */
get_header(); ?>
<!-- Content
================================================== -->
<div id="content" class="site-content">
    <div class="container">
        <div class="row">
        <?php
            /* Show sidebar with user condition */
            $columns_grid = 8;
            $columns_offset  = '';
            if ( softhopper_glimmer('sidebar_layout') == 2 ) {
                get_sidebar();
            } elseif ( softhopper_glimmer('sidebar_layout') == 1 ) {
                $columns_grid = 10;
                $columns_offset = 'col-md-offset-1';
            }
        ?>
        <div class="<?php echo esc_attr( $columns_offset); ?> col-md-<?php echo esc_attr($columns_grid); ?>">
                <!-- Content Area -->
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">                              
                        
                        <?php get_template_part( 'template-parts/content', 'authorinfo' ); ?>

                        <?php if ( have_posts() ) : ?>

                        <?php /* Start the Loop */ ?>
                        <?php while ( have_posts() ) : the_post(); ?>

                            <?php
                                /* Include the Post-Format-specific template for the content.
                                 * If you want to override this in a child theme, then include a file
                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                 */
                                get_template_part( 'template-parts/content', get_post_format() );
                            ?>

                        <?php endwhile; ?>

                        <?php glimmer_posts_pagination_nav(); ?>

                        <?php else : ?>

                            <?php get_template_part( 'template-parts/content', 'none' ); ?>

                        <?php endif; ?>                             

                    </main> <!-- #main -->
                </div> <!-- #primary -->
            </div> <!-- /.col-md-8 -->
            <?php
                /* Show sidebar with user condition */
                if ( softhopper_glimmer('sidebar_layout') == 3 ) {
                    get_sidebar();
                }
            ?>                      
        </div> <!-- /.row -->
    </div> <!-- /.container -->     
</div><!-- #content -->
<?php get_footer(); ?>