<?php
/**
 * The template for displaying search results pages.
 *
 * @package Glimmer
 */
get_header();           
    get_template_part( 'blog-layout' );
get_footer(); 