(function($) {
    "use strict"; // use strict to start

    //RTL Check
    var check_rtl;
    if (typeof glimmer !== 'undefined') { 
        check_rtl = glimmer.check_rtl;
    }
    check_rtl = (check_rtl == 0) ?  false : true;

    var glimmerApp = {
        /* ---------------------------------------------
         Preloader
         --------------------------------------------- */
        preloader: function() {
            $(window).on('load', function() {
                $("body").imagesLoaded(function() {
                    $('.preloader').delay(500).slideUp('slow', function() {
                        $(this).remove();
                    });
                });
            });
        },
        /* ---------------------------------------------
         Value To Placeholder
         --------------------------------------------- */
        placeholder: function() {
            var $ph = $('input[type="search"], input[type="text"], input[type="email"], textarea');
            $ph.each(function() {
                var value = $(this).val();
                $(this).focus(function() {
                    if ($(this).val() === value) {
                        $(this).val('');
                    }
                });
                $(this).blur(function() {
                    if ($(this).val() === '') {
                        $(this).val(value);
                    }
                });
            });
        },
        /* ---------------------------------------------
         Menu
         --------------------------------------------- */
        menu: function() {
            var combinedmenu = $('#nav-left ul.main-menu').clone(),
                secondmenu = $('#nav-right ul.main-menu').clone();
            secondmenu.children('li').appendTo(combinedmenu);
            combinedmenu.appendTo('#mobile-menu-wrap');

            var mobile_main_menu = $('#main-menu .main-menu').clone();
            mobile_main_menu.appendTo('#mobile-menu-main-wrap');

            var items = $('#header-middle .overlapblackbg, #header-middle .slideLeft'),
                menucontent = $('#header-middle .menucontent'),
                submenu = $('#header-middle .main-menu li').has('.sub-menu'),
                menuopen = function() {
                    $(items).removeClass('menuclose').addClass('menuopen');
                },
                menuclose = function() {
                    $(items).removeClass('menuopen').addClass('menuclose');
                };

            var main_items = $('.main-menu-area .overlapblackbg, .main-menu-area .slideLeft'),
                main_menucontent = $('.main-menu-area .menucontent'),
                main_submenu = $('.main-menu-area .main-menu li').has('.sub-menu'),
                main_menuopen = function() {
                    $(main_items).removeClass('menuclose').addClass('menuopen');
                },
                main_menuclose = function() {
                    $(main_items).removeClass('menuopen').addClass('menuclose');
                };

            $('#navToggle').on('click', function() {
                if (menucontent.hasClass('menuopen')) {
                    $(menuclose);
                } else {
                    $(menuopen);
                }
            });            
            $('#navToggleMain').on('click', function() {
                if (main_menucontent.hasClass('menuopen')) {
                    $(main_menuclose);
                } else {
                    $(main_menuopen);
                }
            });
            main_menucontent.on('click', function() {
                if (main_menucontent.hasClass('menuopen')) {
                    $(main_menuclose);
                }
            });            
            menucontent.on('click', function() {
                if (menucontent.hasClass('menuopen')) {
                    $(menuclose);
                }
            });
            submenu.prepend('<span class="menu-click"><i class="menu-arrow fa fa-plus"></i></span>');
            $('.menu-mobile').on('click', function() {
                $('.main-menu').slideToggle('slow');
            });
            $('.menu-click').on('click', function() {
                $(this).siblings('.sub-menu').slideToggle('slow');
                $(this).children('.menu-arrow').toggleClass('menu-extend');
            });
        },
        /* ---------------------------------------------
         Mobile Social Area
         --------------------------------------------- */
        socialarea_mobile: function() {
            var social = $('#header-top .header-social').clone();
            social.appendTo('#mobile-menu-wrap');
            $('#mobile-menu-wrap .header-social').prepend('<span>Follow us on-</span>');
        },
        /* ---------------------------------------------
         smooth scroll
         --------------------------------------------- */
        smoothscroll: function() {
            if (typeof smoothScroll == 'object') {
                smoothScroll.init();
            }

            $("[data-bg-color], [data-bg-image], [data-bg-particles]").each(function() {
                var $this = $(this);

                if( $this.hasClass("ts-separate-bg-element") ){
                    $this.append('<div class="ts-background">');

                    // Background Color

                    if( $("[data-bg-color]") ){
                        $this.find(".ts-background").css("background-color", $this.attr("data-bg-color") );
                    }

                    // Particles
                    if( $this.attr("data-bg-particles-line-color") || $this.attr("data-bg-particles-dot-color") ){
                        $this.find(".ts-background").append('<div class="ts-background-particles">');
                        $(".ts-background-particles").each(function () {
                            var lineColor = $this.attr("data-bg-particles-line-color");
                            var dotColor = $this.attr("data-bg-particles-dot-color");
                            var parallax = $this.attr("data-bg-particles-parallax");
                            $(this).particleground({
                                density: 15000,
                                lineWidth: 0.2,
                                lineColor: lineColor,
                                dotColor: dotColor,
                                parallax: parallax,
                                proximity: 200
                            });
                        });
                    }

                    // Background Image
                    if( $this.attr("data-bg-image") !== undefined ){
                        $this.find(".ts-background").append('<div class="ts-background-image">');
                        $this.find(".ts-background-image").css("background-image", "url("+ $this.attr("data-bg-image") +")" );
                        $this.find(".ts-background-image").css("background-size", $this.attr("data-bg-size") );
                        $this.find(".ts-background-image").css("background-position", $this.attr("data-bg-position") );
                        $this.find(".ts-background-image").css("opacity", $this.attr("data-bg-image-opacity") );

                        $this.find(".ts-background-image").css("background-size", $this.attr("data-bg-size") );
                        $this.find(".ts-background-image").css("background-repeat", $this.attr("data-bg-repeat") );
                        $this.find(".ts-background-image").css("background-position", $this.attr("data-bg-position") );
                        $this.find(".ts-background-image").css("background-blend-mode", $this.attr("data-bg-blend-mode") );
                    }

                    // Parallax effect

                    if( $this.attr("data-bg-parallax") !== undefined ){
                        $this.find(".ts-background-image").addClass("ts-parallax-element");
                    }
                } else {

                    if(  $this.attr("data-bg-color") !== undefined ){
                        $this.css("background-color", $this.attr("data-bg-color") );
                        if( $this.hasClass("btn") ) {
                            $this.css("border-color", $this.attr("data-bg-color"));
                        }
                    }

                    if( $this.attr("data-bg-image") !== undefined ){
                        $this.css("background-image", "url("+ $this.attr("data-bg-image") +")" );

                        $this.css("background-size", $this.attr("data-bg-size") );
                        $this.css("background-repeat", $this.attr("data-bg-repeat") );
                        $this.css("background-position", $this.attr("data-bg-position") );
                        $this.css("background-blend-mode", $this.attr("data-bg-blend-mode") );
                    }

                }
            });

            //  Parallax Background Image
            $("[data-bg-parallax='scroll']").each(function() {
                var speed = $(this).attr("data-bg-parallax-speed");
                var $this = $(this);
                var isVisible;
                var backgroundPosition;

                $this.isInViewport(function(status) {
                    if (status === "entered") {
                        isVisible = 1;
                        var position;

                        $(window).on("scroll", function () {
                            if( isVisible === 1 ){
                                position = $(window).scrollTop() - $this.offset().top;
                                backgroundPosition = (100 - (Math.abs((-$(window).height()) - position) / ($(window).height()+$this.height()))*100);
                                if( $this.find(".ts-parallax-element").hasClass("ts-background-image") ){
                                    $this.find(".ts-background-image.ts-parallax-element").css("background-position-y", (position/speed) + "px");
                                }
                                else {
                                    $this.find(".ts-parallax-element").css("transform", "translateY(" +(position/speed)+ "px)");
                                }
                            }
                        });
                    }
                    if (status === "leaved"){
                        isVisible = 0;
                    }
                });
            });
        },
        /* ---------------------------------------------
         For Video Fit Function
         --------------------------------------------- */
        video: function() {
            $(".feature-area").fitVids();
            $(".content-area").fitVids();
        },

        /* ---------------------------------------------
         Background Fit Image
         --------------------------------------------- */
        background_fit_image: function() {
            $.fn.bgImage = function() {
                $(this).each(function() {
                    var $image = $(this).find('img');
                    var imageSource = $image.attr('src');
                    $image.css('visibility', 'hidden');
                    $(this).css('backgroundImage', 'url(' + imageSource + ')');
                    if (!$image.length) {
                        $(this).css('backgroundImage', 'none');
                    }
                });
            };
            //$('#featured-slider .post-thumb').bgImage();
            //$('.related-post-item .post-media').bgImage();
        },

        /* ---------------------------------------------
         Featured Carousel
         --------------------------------------------- */
        featured_area: function() {
            if (typeof glimmer !== 'undefined') {
                var featured_post_auto_slide = glimmer.featured_post_auto_slide;
                var featured_slide_speed = glimmer.featured_slide_speed;
                var featured_autoplay_timeout = glimmer.featured_autoplay_timeout;
            }
            if (featured_post_auto_slide == 1) {
                var featured_post_auto_slide = true;
            } else {
                var featured_post_auto_slide = false;
            }
            var item = 3;
            var item_number = $('#featured-slider .item').length;
            var is_loop = (item_number <= 3) ? false : true ;

            $('#featured-slider').owlCarousel({
                items: item,
                autoplay: featured_post_auto_slide,
                autoplayTimeout: featured_autoplay_timeout,
                autoplaySpeed: featured_slide_speed,
                navSpeed: featured_slide_speed,
                autoplayHoverPause: true,
                singleItem: false,
                loop: is_loop,
                nav: is_loop,
                margin: 30,
                rtl: check_rtl,
                dots: true,
                navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                responsive: {
                    280: {
                        items: 1
                    },
                    500: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    700: {
                        items: 2
                    },
                    768: {
                        items: 2
                    },
                    800: {
                        items: 2
                    },
                    1000: {
                        items: item
                    },
                    1200: {
                        items: item
                    },
                    1400: {
                        items: item
                    }
                },
            });

            function setOwlStageHeight(event) {
                var maxHeight = 0;
                $('.owl-item.active').each(function () { // LOOP THROUGH ACTIVE ITEMS
                    var thisHeight = parseInt( $(this).height() );
                    maxHeight=(maxHeight>=thisHeight?maxHeight:thisHeight);
                });
                $('.owl-carousel').css('height', maxHeight );
                $('.owl-stage-outer').css('height', maxHeight ); // CORRECT DRAG-AREA SO BUTTONS ARE CLICKABLE
            }
        },
        /* ---------------------------------------------
         Featured Post Height Fix
         --------------------------------------------- */
        post_height: function() {
            $.fn.fixedHeight = function() {
                var maxHeight = 0;
                $(this).each(function() {
                    var prevHeight = $(this).height();
                    var thisHeight = $(this).height('auto').height();
                    $(this).height(prevHeight);
                    maxHeight = (maxHeight > thisHeight ? maxHeight : thisHeight);
                    var video = $(this).find('.fluid-width-video-wrapper') || $(this).find('.mejs-container');
                    if (video) {
                        video.parent().height('auto');
                        video.css('height', '' + maxHeight + 'px');
                    }
                });
                $(this).height(maxHeight);
            };
            $(window).on('load', function() {
                // $('#featured-slider .post-thumb').fixedHeight();
                // $('#featured-slider .post-content').fixedHeight();
                // $('.related-post-item .post-media').fixedHeight();
            });
        },
        /* ---------------------------------------------
         Home Version Grid Masonry
         --------------------------------------------- */
        grid_masonry: function() {
            if ($('#layout-masonry').length > 0) {
                var container = $('#layout-masonry');
                container.imagesLoaded(function () {
                    container.masonry({
                        itemSelector: '#layout-masonry > [class*="col-"]',
                        columnWidth: '.grid',
                        percentPosition: true
                    });
                });
                $(window).on('resize', function() {
                    container.masonry('layout');
                });
            }
        },
        /* ---------------------------------------------
         Chat More Button fix
         --------------------------------------------- */
        chat_more_button: function() {
            if ($('.chat-text p .more-link').length) {
                $('.chat-text p .more-link').detach().appendTo('#readmore-add');
            }
        },
        /* ---------------------------------------------
         Grid Version Read more Fix
         --------------------------------------------- */
        gridmore_button: function() {
            if ($('.grid .entry-content p .more-link').length) {
                $('.grid .entry-content p .more-link').remove();
            }
        },
        /* ---------------------------------------------
         Gallery Style One Carousel
         --------------------------------------------- */
        gallary_one: function() {
            $('.gallery-one').owlCarousel({
                items: 1,
                autoplay: true,
                autoplayTimeout: 5000,
                autoplayHoverPause: true,
                singleItem: true,
                loop: true,
                rtl: check_rtl,
                nav: true,
                navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                owl2row: 'true',
                owl2rowTarget: 'item',
                responsive: {
                    1170: {
                        items: 1
                    }
                }
            });
            $('.gallery-one .item').on('click', function(e) {
                e.preventDefault();
                $('.gallery-one').magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    closeOnContentClick: false,
                    closeBtnInside: false,
                    mainClass: 'mfp-with-zoom mfp-img-mobile',
                    image: {
                        verticalFit: true,
                    },
                    gallery: {
                        enabled: true
                    },
                    zoom: {
                        enabled: true,
                        duration: 300,
                        opener: function(element) {
                            return element.find('img');
                        }
                    },
                });
            });
        },
        /* ---------------------------------------------
         Gallery Style Two Carousel
         --------------------------------------------- */
        gallary_two: function() {
            var $sync1 = $(".full-view"),
                $sync2 = $(".list-view"),
                duration = 300;

            $sync1
                .owlCarousel({
                    items: 1,
                    margin: 10,
                    nav: true,
                    rtl: check_rtl,
                    navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                    owl2row: 'true',
                    owl2rowTarget: 'item'
                })
                .on('changed.owl.carousel', function(e) {
                    var syncedPosition = syncPosition(e.item.index);
                    if (syncedPosition != "stayStill") {
                        $sync2.trigger('to.owl.carousel', [syncedPosition, duration, true]);
                    }
                });
            $('.full-view .item').on('click', function(e) {
                e.preventDefault();
                $sync1.magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    closeOnContentClick: false,
                    closeBtnInside: false,
                    mainClass: 'mfp-with-zoom mfp-img-mobile',
                    image: {
                        verticalFit: true,
                    },
                    gallery: {
                        enabled: true
                    },
                    zoom: {
                        enabled: true,
                        duration: 300,
                        opener: function(element) {
                            return element.find('img');
                        }
                    },
                });
            });
            if (typeof glimmer !== 'undefined') {
                var owl_item = glimmer.owl_item;
            }
            $sync2
                .owlCarousel({
                    margin: 5,
                    items: owl_item,
                    nav: false,
                    center: false,
                    dots: false,
                    rtl: check_rtl,
                    responsive: {
                        280: {
                            items: 2
                        },
                        500: {
                            items: 2
                        },
                        600: {
                            items: 3
                        },
                        800: {
                            items: 4
                        },
                        1000: {
                            items: 5
                        },
                        1200: {
                            items: 5
                        },
                        1400: {
                            items: 5
                        },
                    }
                })
                .on('initialized.owl.carousel', function() {
                    addClassCurrent(0);
                })
                .on('click', '.owl-item', function() {
                    $sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);

                });

            function addClassCurrent(index) {
                $sync2
                    .find(".owl-item.active")
                    .removeClass("current")
                    .eq(index)
                    .addClass("current");
            }
            addClassCurrent(0);

            function syncPosition(index) {
                addClassCurrent(index);
                var itemsNo = $sync2.find(".owl-item").length;
                var visibleItemsNo = $sync2.find(".owl-item.active").length;

                if (itemsNo === visibleItemsNo) {
                    return "stayStill";
                }
                var visibleCurrentIndex = $sync2.find(".owl-item.active").index($sync2.find(".owl-item.current"));
                if (visibleCurrentIndex == 0 && index != 0) {
                    return index - 1;
                }
                if (visibleCurrentIndex == (visibleItemsNo - 1) && index != (itemsNo - 1)) {
                    return index - visibleItemsNo + 2;
                }
                return "stayStill";
            }
        },
        /* ---------------------------------------------
         Justified Gallery
         --------------------------------------------- */
        gallery_justified: function() {
            if ($('.glimmer-tiled-gallery').length) {
                if (typeof glimmer !== 'undefined') {
                    var tiled_gallery_row_height = glimmer.tiled_gallery_row_height;
                }
                var tiledItemSpacing = 4;
                $('.glimmer-tiled-gallery').wrap('<div class="glimmer-tiled-gallery-row"></div>');
                $('.glimmer-tiled-gallery').parent().css('margin', -tiledItemSpacing);
                $('.glimmer-tiled-gallery').justifiedGallery({
                    rowHeight: tiled_gallery_row_height,
                    lastRow: 'justify',
                    maxRowHeight: '200%',
                    margins: tiledItemSpacing,
                    waitThumbnailsLoad: false
                });
            }
            $('.glimmer-tiled-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: false,
                mainClass: 'pp-gallery mfp-with-zoom mfp-img-mobile',
                image: {
                    verticalFit: true,
                },
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300,
                    opener: function(element) {
                        return element.find('img');
                    }
                },
            });
        },
        /* ---------------------------------------------
         WP Comment Form
         --------------------------------------------- */
        comment_form: function() {
            $("#contact_form > p").wrap("<div class='col-md-12'></div>");
            $("#contact_form > div").wrapAll("<div class='row'></div>");
        },
        /* ---------------------------------------------
         Maps
         --------------------------------------------- */
        maps: function() {
            if ($('#gmaps').length) {
                var map;
                if (typeof glimmer !== 'undefined') {
                    var lat = glimmer.lat;
                    var lon = glimmer.lon;
                    var map_mouse_wheel = glimmer.map_mouse_wheel;
                    var map_zoom_control = glimmer.map_zoom_control;
                    var map_point_img = glimmer.map_point_img;
                }
                map = new GMaps({
                    el: '#gmaps',
                    lat: lat,
                    lng: lon,
                    scrollwheel: map_mouse_wheel,
                    zoom: 10,
                    zoomControl: map_zoom_control,
                    panControl: false,
                    streetViewControl: false,
                    mapTypeControl: false,
                    overviewMapControl: false,
                    clickable: false
                });

                var image = map_point_img;
                map.addMarker({
                    lat: lat,
                    lng: lon,
                    icon: image,
                    animation: google.maps.Animation.DROP,
                    verticalAlign: 'bottom',
                    horizontalAlign: 'center'
                });                
            }
        },
        /* ---------------------------------------------
         Scroll top
         --------------------------------------------- */
        scroll_top: function() {
            $("body").append("<a href='#top' id='scroll-top' class='topbutton btn-hide'><span class='glyphicon glyphicon-menu-up'></span></a>");
            var $scrolltop = $('#scroll-top');
            $(window).on('scroll', function() {
                if ($(this).scrollTop() > $(this).height()) {
                    $scrolltop
                        .addClass('btn-show')
                        .removeClass('btn-hide');
                } else {
                    $scrolltop
                        .addClass('btn-hide')
                        .removeClass('btn-show');
                }
            });
            $("a[href='#top']").on('click', function() {
                $("html, body").animate({
                    scrollTop: 0
                }, "normal");
                return false;
            });
        },
        /* ---------------------------------------------
         WP Admin bar
         --------------------------------------------- */
        wp_adminbar: function() {
            // This function gets called with the user has scrolled the window.
            $(window).on('scroll', function() {
                if ($(this).scrollTop() > 0) {
                    // Add the scrolled class to those elements that you want changed
                    $("#mobile-menu .top-menu.menuopen").addClass("scroll");
                } else {
                    $("#mobile-menu .top-menu.menuopen").removeClass("scroll");
                }
            });
        },
        /* ---------------------------------------------
         Initialize All Function
         --------------------------------------------- */
        initializ: function() {
            glimmerApp.preloader();
            glimmerApp.placeholder();
            glimmerApp.menu();
            glimmerApp.socialarea_mobile();
            glimmerApp.smoothscroll();
            glimmerApp.video();
            glimmerApp.background_fit_image();
            glimmerApp.featured_area();
            glimmerApp.post_height();
            glimmerApp.grid_masonry();
            glimmerApp.chat_more_button();
            glimmerApp.gridmore_button();
            glimmerApp.gallary_one();
            glimmerApp.gallary_two();
            glimmerApp.gallery_justified();
            glimmerApp.comment_form();
            glimmerApp.maps();
            glimmerApp.scroll_top();
            glimmerApp.wp_adminbar();
        }
    };
    /* ---------------------------------------------
     Document ready function
     --------------------------------------------- */
    $(function() {
        glimmerApp.initializ();
    });
})(jQuery);