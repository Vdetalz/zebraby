<?php 
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package Glimmer
 */
get_header(); ?>
<!-- Content
================================================== -->
<div id="content" class="site-content">
    <div class="container">
        <div class="row">
            <?php
                /* Show sidebar with user condition */
                $meta = get_post_meta( $post->ID );
                $columns_grid = 8; 
                $columns_offset  = ''; 
                $content_push = '';
                if ( isset($meta["_glimmer_custom_layout"][0])) {
                    if ( $meta["_glimmer_layout"][0] == 'sidebar-content' ) {
                        $content_push = 'col-md-push-4';
                    } elseif ( $meta["_glimmer_layout"][0] == 'full-content' ) {
                        $columns_grid = glimmer_theme_full_width_column();
                        $columns_offset = 'full-width-content';
                    }
                } elseif ( softhopper_glimmer('sidebar_layout_page') == 'sidebar-left' ) {
                    $content_push = 'col-md-push-4';
                } elseif ( softhopper_glimmer('sidebar_layout_page') == 'full-width' ) {
                    $columns_grid = glimmer_theme_full_width_column();
                    $columns_offset = 'full-width-content';
                }
            ?>
            <div class="<?php echo esc_attr( $columns_offset); ?> col-md-<?php echo esc_attr($columns_grid); ?> <?php echo esc_attr($content_push); ?>">
                <!-- Content Area -->
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">
                        <div class="row">                            
                            <?php if ( have_posts() ) : ?>

                            <?php /* Start the Loop */ ?>
                            <?php while ( have_posts() ) : the_post(); ?>

                                <?php
                                    /* Include the Post-Format-specific template for the content.
                                     * If you want to override this in a child theme, then include a file
                                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                     */
                                    get_template_part( 'template-parts/content', 'page' );
                                ?>

                            <?php endwhile; ?>

                            <?php else : ?>

                                <?php get_template_part( 'template-parts/content', 'none' ); ?>

                            <?php endif; ?>
                            <div class="col-md-12">
                                <!-- Include author info template part -->
                                <?php
                                    if ( isset($meta["_glimmer_page_author_info_box"][0]) ) {
                                        $page_author_info_box = ($meta["_glimmer_page_author_info_box"][0] == 'show') ? true : false ;
                                    } else {
                                        $page_author_info_box = ( softhopper_glimmer('page_author_info_box') == true) ? true : false ;
                                    }
                                    if ($page_author_info_box == true) {
                                        get_template_part( 'template-parts/content', 'authorinfo' ); 
                                    }
                                    // If comments are open or we have at least one comment, load up the comment template
                                    if ( comments_open() || get_comments_number() ) :
                                        comments_template();
                                    endif;
                                ?>
                            </div>
                        </div>  
                    </main> <!-- #main -->
                </div> <!-- #primary -->
            </div> <!-- /.col-md-8 -->
            <?php
                /* Show sidebar with user condition */
                $meta = get_post_meta( $post->ID );
                if ( isset($meta["_glimmer_custom_layout"][0])) {
                    if ( $meta["_glimmer_layout"][0] != 'full-content') {
                        get_sidebar();
                    } 
                } elseif ( softhopper_glimmer('sidebar_layout_page') != 'full-width' ) {
                    get_sidebar();
                }
            ?>                     
        </div> <!-- /.row -->
    </div> <!-- /.container -->     
</div><!-- #content -->
<?php get_footer(); ?>