<?php get_template_part( 'template-parts/content', 'featured' ); ?> 

<!-- Content
================================================== -->
<?php
if( class_exists( 'GlimmerFeatured_Post' ) && softhopper_glimmer('featured_display') == 1  && !is_paged() && !is_archive() && !is_paged() && !is_search()) {
    $blog_home_main_class = 'blog-home-spacing-main';
} else {
    $blog_home_main_class = '';
}

if ( glimmer_theme_sidebar_layout() == "full-width" || softhopper_glimmer('sidebar_layout') == 1) {
    if ( !is_paged() ) {
        if ( glimmer_theme_sidebar_layout() == "sidebar-left" || glimmer_theme_sidebar_layout() == "sidebar-right") {
            ( softhopper_glimmer('featured_display') == 1 ) ? $full_width = 'id="blog-content"' : $full_width = '';
        } else {
            $full_width = '';
        }
    } else {
        $full_width = '';
    }
} else {
    // condition for pagination
    if ( !is_paged() ) {
        ( softhopper_glimmer('featured_display') == 1 ) ? $full_width = 'id="blog-content"' : $full_width = '';
    } else {
        $full_width = '';
    }
}
?>
<div id="content" class="site-content">
    <?php glimmer_theme_archive_search_page_title(); ?>
	<div class="container <?php echo esc_attr( $blog_home_main_class .' '. glimmer_theme_sidebar_layout() ); ?>" <?php echo esc_attr( $full_width ); ?>>
		<div class="row">
        <?php
            /* Show sidebar with user condition */
            $columns_offset  = '';
            $columns_grid = 8;
            $content_push = '';
            if ( glimmer_theme_sidebar_layout() == "sidebar-left" ) {
                $content_push = 'col-md-push-4';
            } elseif ( glimmer_theme_sidebar_layout() == "full-width" ) {
                $columns_grid = glimmer_theme_full_width_column();
                $columns_offset = 'full-width-content';
            } 
        ?>
        <div class="<?php echo esc_attr($columns_offset); ?> col-md-<?php echo esc_attr($columns_grid); ?> <?php echo esc_attr($content_push); ?>">
            <?php
                // if full-width load this
                if ( glimmer_theme_sidebar_layout() == "full-width") {
                    echo '<div class="row">';
                    if ( !is_paged() && softhopper_glimmer('featured_display') == 1 ) {
                        echo '<div class="col-md-12" id="blog-content">';
                    }
                } ?>
				<!-- Content Area -->
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
                        <?php 
                        // the query
                        $argsPosts = array(
                            'ignore_sticky_posts' => 1,
                            'meta_query' => array(
                                array(
                                    'key' => '_is_featured',
                                    'value' => 'yes',
                                )
                            ),
                        );
                        // Featured Posts
                        $ids = array();                   
                        $withoutFeatured = new WP_Query( $argsPosts );
                        if ( $withoutFeatured->have_posts() ) {
                            while ( $withoutFeatured->have_posts() ) : $withoutFeatured->the_post();
                                $ids[] = $post->ID; // building array of post ids
                            endwhile;
                        } ?>	

                        <?php if( softhopper_glimmer('post_layout') == "list" ) { ?>
                        <div class="row" > 
                            <?php 
                            // don't show featured post in index page
                            if( class_exists( 'GlimmerFeatured_Post' ) && softhopper_glimmer('featured_display_in_post') !== '1' ) {
                                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                                $featuredRemoveArgs = array( 
                                    'post__not_in'=> $ids, 
                                    'paged' => $paged,
                                    'post_status' => 'publish',
                                );
                                $featuredRemoveQuery = new WP_Query( $featuredRemoveArgs );
                                while ( $featuredRemoveQuery->have_posts() ) : $featuredRemoveQuery->the_post(); 
                                    get_template_part( 'template-parts/content' );
                                endwhile;
                            } else {
                                if ( have_posts() ) {                            
                                    while ( have_posts() ) : the_post(); 
                                        /* Include the Post-Format-specific template for the content.
                                         * If you want to override this in a child theme, then include a file
                                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                         */
                                        get_template_part( 'template-parts/content' );
                                    endwhile; 
                                } else {
                                    get_template_part( 'template-parts/content', 'none' );
                                }
                            } ?>
                        </div> <!-- /.row -->
                        <?php } else { ?>
                            <?php
                                $grid_style = "";
                                if (is_archive() || is_search()) {
                                    $grid_style = ( softhopper_glimmer('grid_style_archive') == 'grid') ? 'grid' : 'masonry';
                                } else {
                                    $grid_style = (softhopper_glimmer('grid_style') == 'grid') ? 'grid' : 'masonry';
                                }
                            ?>
                            <div class="row" id="layout-<?php echo esc_attr($grid_style); ?>"> 
                                <?php 
                                // don't show featured post in index page
                                if( class_exists( 'GlimmerFeatured_Post' ) && softhopper_glimmer('featured_display_in_post') == 1 ) {
                                    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                                    $featuredRemoveArgs = array( 
                                        'post__not_in'=> $ids, 
                                        'paged' => $paged,
                                        'post_status' => 'publish',
                                    );
                                    $featuredRemoveQuery = new WP_Query( $featuredRemoveArgs );
                                    while ( $featuredRemoveQuery->have_posts() ) : $featuredRemoveQuery->the_post(); 
                                        get_template_part( 'template-parts/post/content', get_post_format() );
                                    endwhile;
                                } else {
                                    if(  have_posts() ) {
                                        while ( have_posts() ) : the_post(); 
                                            /* Include the Post-Format-specific template for the content.
                                             * If you want to override this in a child theme, then include a file
                                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                             */
                                            get_template_part( 'template-parts/content' );
                                        endwhile; 
                                    } else {
                                        get_template_part( 'template-parts/content', 'none' );
                                    }
                                } ?>
                            </div> <!-- /.row -->
                        <?php } ?>
                    </main> <!-- #main -->

                    <?php glimmer_posts_pagination_nav(); ?>
				</div> <!-- #primary -->
			</div> <!-- /.col-md-8 -->

            <?php
                /* Show sidebar with user condition */
                if ( glimmer_theme_sidebar_layout() != "full-width" ) {
                    get_sidebar();
                } 
            ?>			

            <?php
                // if full-width load this
                if ( glimmer_theme_sidebar_layout() == "full-width" ) {
                    if ( !is_paged() && softhopper_glimmer('featured_display') == 1 ) {
                        echo '</div>';
                    }
                    echo '</div>';                    
                }
            ?>
		</div> <!-- /.row -->
	</div> <!-- /.container -->		
</div><!-- #content -->
