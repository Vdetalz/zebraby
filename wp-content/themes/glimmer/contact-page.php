<?php
/**
 * Template Name: Contact Page
 */
?>
<?php get_header(); ?>
<!-- Content
================================================== -->
<div id="content" class="site-content contact-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header-title">
                    <h2 class="section-title"><span><?php the_title(); ?></span></h2>
                </div> <!-- /.header-title --> 
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <?php if( softhopper_glimmer('google_map_api') !== '' ) { ?>
                <div class="gmaps-area">
                    <div id="gmaps"></div>                                   
                </div> <!-- /.gmaps-area -->
                <?php } else { ?>
                    <div class="gmaps-embaded-area"><?php echo ( softhopper_glimmer( 'google_map_embade_code' ) ); ?></div>
                <?php } ?>
                <div class="clear"></div>
                <div class="contact-details">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="address-main">
                                <span><?php if (  softhopper_glimmer('contact_address_main') !== '' ) echo wp_kses_post( softhopper_glimmer('contact_address_main') ); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="entry-content">
                                <p><?php if (  softhopper_glimmer('contact_description') !== '' ) echo wp_kses_post(softhopper_glimmer('contact_description') ); ?></p>
                            </div> <!-- /.entry-content -->  
                        </div>
                        <div class="col-md-4 col-md-offset-1">
                            <div id="contact-info">                                   
                                <?php if ( softhopper_glimmer('contact_address') !== '' ) echo wp_kses_post( softhopper_glimmer('contact_address') ); ?>   
                            </div>     
                        </div>
                    </div> <!-- /.row -->

                    <div class="row" id="contact-form-wrap">
                        <div class="col-md-12">
                            <div class="contact-respond" id="respond">                            
                                <?php 
                                    // show contact form by contact form 7 plugin 
                                    if ( softhopper_glimmer('contact_form7_shortcode') !== '' ) {
                                        echo do_shortcode( softhopper_glimmer('contact_form7_shortcode') ); 
                                    }
                                ?>         
                            </div><!-- #respond --> 
                        </div> <!-- /.col-md-12 -->
                    </div>
                </div> <!-- /.contact-details -->
            </div>
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- #content -->
<?php get_footer(); ?>