<?php
/**
 * The template for displaying all single posts.
 *
 * @package Glimmer
 */
get_header(); ?>
<!-- Content
================================================== -->
<div id="content" class="site-content">
    <div class="container">
        <div class="row">
            <?php
               /* Show sidebar with user condition */
               $meta = get_post_meta( $post->ID );
               $columns_grid = 8; 
               $columns_offset  = ''; 
               $content_push = '';
               if ( isset($meta["_glimmer_custom_layout"][0])) {
                   if ( $meta["_glimmer_layout"][0] == 'sidebar-content' ) {
                       $content_push = 'col-md-push-4';
                   } elseif ( $meta["_glimmer_layout"][0] == 'full-content' ) {
                       $columns_grid = glimmer_theme_full_width_column();
                       $columns_offset = 'full-width-content';
                   }
               } elseif ( softhopper_glimmer('sidebar_layout_single') == 'sidebar-left' ) {
                  $content_push = 'col-md-push-4';
               } elseif ( softhopper_glimmer('sidebar_layout_single') == 'full-width' ) {
                  $columns_grid = glimmer_theme_full_width_column();
                  $columns_offset = 'full-width-content';
               }
           ?>
           <div class="<?php echo esc_attr( $columns_offset); ?> col-md-<?php echo esc_attr($columns_grid); ?> <?php echo esc_attr($content_push); ?>">
                <!-- Content Area -->
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">
                      <div class="row">                              
                        <?php if ( have_posts() ) : ?>

                        <?php /* Start the Loop */ ?>
                        <?php while ( have_posts() ) : the_post(); ?>

                            <?php
                                /* Include the Post-Format-specific template for the content.
                                 * If you want to override this in a child theme, then include a file
                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                 */
                                get_template_part( 'template-parts/content', get_post_format() );
                            ?>

                        <?php endwhile; ?>

                        <?php else : ?>

                            <?php get_template_part( 'template-parts/content', 'none' ); ?>

                        <?php endif; ?>
                        <div class="col-md-12">
                            <?php 
                                if ( isset($meta["_glimmer_post_pagination"][0]) ) {
                                    $post_pagination = ($meta["_glimmer_post_pagination"][0] == 'show') ? true : false ;
                                } else {
                                    $post_pagination = ( softhopper_glimmer('post_pagination') == true) ? true : false ;
                                }
                                if ($post_pagination == true) {
                                    get_template_part( 'template-parts/content', 'pagination' ); 
                                }

                                if ( isset($meta["_glimmer_post_author_info_box"][0]) ) {
                                    $post_author_info_box = ($meta["_glimmer_post_author_info_box"][0] == 'show') ? true : false ;
                                } else {
                                    $post_author_info_box = ( softhopper_glimmer('post_author_info_box') == true) ? true : false ;
                                }
                                if ($post_author_info_box == true) {
                                    get_template_part( 'template-parts/content', 'authorinfo' ); 
                                }

                                if ( isset($meta["_glimmer_related_post"][0]) ) {
                                    $related_post = ($meta["_glimmer_related_post"][0] == 'show') ? true : false ;
                                } else {
                                    $related_post = ( softhopper_glimmer('related_post') == true) ? true : false ;
                                }
                                if ($related_post == true) {
                                    get_template_part( 'template-parts/content', 'relatedpost' );
                                }
                            ?>
                          <?php
                              // If comments are open or we have at least one comment, load up the comment template
                              if ( comments_open() || get_comments_number() ) :
                                  comments_template();
                              endif;
                          ?>
                        </div> <!-- /.col-md-12 -->                           
                      </div> <!-- /.row -->
                    </main> <!-- #main -->
                </div> <!-- #primary -->
            </div> <!-- /.col-md-8 -->
            <?php
                /* Show sidebar with user condition */
                $meta = get_post_meta( $post->ID );
                if ( isset($meta["_glimmer_custom_layout"][0]) ) {
                    if ( $meta["_glimmer_layout"][0] != 'full-content') {
                        get_sidebar();
                    } 
                } elseif ( softhopper_glimmer('sidebar_layout_single') != 'full-width' ) {
                    get_sidebar();
                }
            ?>                     
        </div> <!-- /.row -->
    </div> <!-- /.container -->     
</div><!-- #content -->
<?php get_footer(); ?>