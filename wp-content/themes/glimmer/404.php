<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package Glimmer
 * @since 1.0
 */
?>
<?php 
    get_header(); 
?>
<!-- Content
================================================== -->
<div id="content" class="site-content error-page">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <!-- Error Page Search Part -->
                <div class="error-search">                        
                    <div class="search">
                        <?php get_search_form(); ?>                               
                    </div> <!-- /.search -->

                    <div class="goto-button">                
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="go-to">
                            <span class="go-button"><?php esc_html_e( 'Go to Homepage', 'glimmer' ) ?></span>
                        </a>
                        <?php 
                            //get contact page template url by template slug
                            $pages = get_pages(array(
                                'meta_key' => '_wp_page_template',
                                'meta_value' => 'contact-page.php'
                            ));
                            $contact_page_url = '';
                            foreach($pages as $page){
                                $contact_page_url = $page->ID;
                            }
                            ( !empty($contact_page_url) ) ? $contact_page_url  = get_permalink( $contact_page_url ) : $contact_page_url = "#";
                        ?>
                        <a href="<?php echo esc_url( $contact_page_url ); ?>" class="go-to">
                            <span class="go-button"><?php esc_html_e( 'Contact', 'glimmer' ) ?></span>
                        </a>
                    </div> <!-- /.goto-button --> 
                </div> <!-- /.error-search -->

                <div class="error-image clearfix">
                    <img src="<?php if ( softhopper_glimmer('404_img','url')  ) echo esc_url( softhopper_glimmer('404_img','url') ); ?>" alt="<?php esc_attr_e('404', 'glimmer') ?>" class="img-responsive">
                </div> <!-- /.error-image -->
            </div>  
        </div> <!-- /.row -->

    </div> <!-- /.container -->
    
</div><!-- #content -->
<?php get_footer(); ?>