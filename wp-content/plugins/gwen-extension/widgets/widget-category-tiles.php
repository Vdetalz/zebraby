<?php
/**
 * Add function to widgets_init that'll load our widget.
 */
add_action( 'widgets_init', 'bk_register_widget_category_titles' );

function bk_register_widget_category_titles() {
	register_widget( 'bk_widget_category_titles' );
}

/**
 * This class handles everything that needs to be handled with the widget:
 * the settings, form, display, and update.  Nice!
 *
 */
class bk_widget_category_titles extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'gwen-widget gwen--category-tiles', 'description' => esc_html__('Displays Category Titles.', 'the-next-mag') );

		/* Create the widget. */
		parent::__construct( 'bk_widget_category_titles', esc_html__('[Gwen] Widget Category Tiles', 'the-next-mag'), $widget_ops);
	}
    
	/**
	 *display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );
        
        $widget_opts = array();
        $title       = $instance['title'];
        $instance['tile_description'] = strip_tags($instance['tile_description']);
        $category_ids =  explode( ',', $instance['category_ids'] );
        
        echo ($before_widget);
         
        ?>
        <div class="widget__title">
    		<h4 class="widget__title-text"><?php echo esc_html($title);?></h4>
    	</div>
        <div class="gwen-widget-categories gwen-widget widget">
            <ul class="list-unstyled list-space-sm">
                <?php
                    echo gwen_widget::get_category_tiles($category_ids, $instance['tile_description']);
                ?>
            </ul>
        </div>
        <?php
        /* After widget (defined by themes). */
		echo ($after_widget);
	}
	
	/**
	 * update widget settings
	 */
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
        $instance['title']          = $new_instance['title'];
        $instance['tile_description']  = strip_tags($new_instance['tile_description']);
        $instance['category_ids']   = $new_instance['category_ids'];
		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {
		$defaults = array('title' => 'Categories', 'tile_description' => '', 'category_ids' => '');
		$instance = wp_parse_args((array) $instance, $defaults);
	?>
        <p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><strong><?php esc_html_e('[Optional] Title:', 'the-next-mag'); ?></strong></label>
			<input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php if( !empty($instance['title']) ) echo esc_attr($instance['title']); ?>" />
		</p>
        
        <p>
		    <label for="<?php echo esc_attr($this->get_field_id( 'tile_description' )); ?>"><?php esc_attr_e('Tile Description :', 'the-next-mag'); ?></label>
		    <select class="widefat" id="<?php echo esc_attr($this->get_field_id( 'tile_description' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'tile_description' )); ?>" >
			    <option value="description" <?php if( !empty($instance['tile_description']) && $instance['tile_description'] == 'description' ) echo 'selected="selected"'; else echo ""; ?>><?php esc_attr_e('Category description', 'the-next-mag'); ?></option>
			    <option value="post-count" <?php if( !empty($instance['tile_description']) && $instance['tile_description'] == 'post-count' ) echo 'selected="selected"'; else echo ""; ?>><?php esc_attr_e('Post Count', 'the-next-mag'); ?></option>
			    <option value="disable" <?php if( !empty($instance['tile_description']) && $instance['tile_description'] == 'disable' ) echo 'selected="selected"'; else echo ""; ?>><?php esc_attr_e('Disable', 'the-next-mag'); ?></option>
			 </select>
	    </p>
        
        <p>
		    <label for="<?php echo esc_attr($this->get_field_id( 'category_ids' )); ?>"><?php esc_attr_e('Categories: (Separate category ids by the comma. e.g. 1,2):','the-next-mag') ?></label>
		    <input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id( 'category_ids' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'category_ids' )); ?>" value="<?php if( !empty($instance['category_ids']) ) echo esc_attr($instance['category_ids']); ?>" />
	    </p>
<?php
	}
}
?>
