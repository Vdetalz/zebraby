<?php
/**
 * Add function to widgets_init that'll load our widget.
 */
add_action('widgets_init', 'gwen_register_instagram_widget');
function gwen_register_instagram_widget(){
	register_widget('gwen_instagram');
}
class gwen_instagram extends WP_Widget {
    
/**
 * Widget setup.
 */
	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'gwen-widget gwen--instagram', 'description' => esc_html__('Displays Instagram Gallery.', 'the-next-mag') );

		/* Create the widget. */
		parent::__construct( 'gwen_instagram', esc_html__('[Gwen]: Instagram', 'the-next-mag'), $widget_ops);
	}
    function widget( $args, $instance ) {
		extract($args);
        $title = $instance['title'];
        $userid = apply_filters('userid', $instance['userid']);
    	$amount = apply_filters('instagram_image_amount', $instance['image_amount']);
        echo $before_widget; 
        
		// Pulls and parses data.
        
        $photos_arr = array();
            
		$search_for['username'] = $userid;
    	$photos_arr = gwen_widget::gwen_get_instagram( $search_for, $amount, $amount, false );
        
		?>
        <div class="widget-about__inner">
            <div class="widget__title">
        		<h4 class="widget__title-text"><?php echo esc_html($title);?></h4>
        	</div>
    		<div class="widget__content">
                <ul class="list-unstyled clearfix">
                    <?php
            			foreach($photos_arr as $photo)
            			{
            		?>
            			<li class="instagram-item"><a target="_blank" href="<?php echo esc_url($photo['link']); ?>"><img src="<?php echo esc_url($photo['large']); ?>" alt="<?php echo esc_attr($photo['description']); ?>" /></a></li>
            		<?php
            			}
            		?>
                </ul>
            </div>
        </div>
        								
        <?php echo $after_widget; ?>
        			 
        <?php }	

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {	
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
      /* Set up some default widget settings. */
      $defaults = array( 'title' => '', 'userid' => '', 'image_amount' => '');
      $instance = wp_parse_args( (array) $instance, $defaults );	

      $title = esc_attr($instance['title']);
			$userid = esc_attr($instance['userid']);
			$amount = esc_attr($instance['image_amount']);	
    ?>
    <p>
		<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><strong><?php esc_html_e('[Optional] Title:', 'the-next-mag'); ?></strong></label>
		<input type="text" class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php if( !empty($instance['title']) ) echo esc_attr($instance['title']); ?>" />
	</p>
    <p><label for="<?php echo $this->get_field_id('userid'); ?>"><?php esc_html_e( 'Instagram user ID:', 'the-next-mag'); ?> <input class="widefat" id="<?php echo $this->get_field_id('userid'); ?>" name="<?php echo $this->get_field_name('userid'); ?>" type="text" value="<?php echo $userid; ?>" /></label></p>
    <p><label for="<?php echo $this->get_field_id('image_amount'); ?>"><?php esc_html_e( 'Images count:', 'the-next-mag'); ?> <input class="widefat" id="<?php echo $this->get_field_id('image_amount'); ?>" name="<?php echo $this->get_field_name('image_amount'); ?>" type="text" value="<?php echo $amount; ?>" /></label></p>	

<?php }

}
?>