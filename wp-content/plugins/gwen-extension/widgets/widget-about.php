<?php
add_action( 'widgets_init', 'gwen_about_load_widget' );

function gwen_about_load_widget() {
	register_widget( 'gwen_about_widget' );
}

class gwen_about_widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'gwen--about', 'description' => esc_html__('About us/me information.', 'gwen') );

		/* Create the widget. */
		parent::__construct( 'gwen_about_widget', esc_html__('[Gwen] Widget About', 'gwen'), $widget_ops);
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$authorID = $instance['author_id'];
        if($authorID == '') {
            return;
        }
		
		/* Before widget (defined by themes). */
		echo $before_widget;

		$bk_author_bio = get_the_author_meta('description', $authorID);
        $bk_author_email = get_the_author_meta('publicemail', $authorID);
        $bk_author_name = get_the_author_meta('display_name', $authorID);
        $bk_author_tw = get_the_author_meta('twitter', $authorID);
        $bk_author_go = get_the_author_meta('googleplus', $authorID);
        $bk_author_fb = get_the_author_meta('facebook', $authorID);
        $bk_author_yo = get_the_author_meta('youtube', $authorID);
        $bk_author_www = get_the_author_meta('url', $authorID);

		?>
			<div class="widget-about__inner text-center">
                <div class="widget__title">
            		<h4 class="widget__title-text"><?php echo esc_html($title);?></h4>
            	</div>
    			<div class="widget__content">
                    <div class="author__avatar">
                        <?php echo get_avatar( $authorID, $size = '150', '', $bk_author_name, array('class' => 'entry-author__avatar') );?>     
                    </div>
                    <div class="author__name">
                        <a class="entry-author__name" rel="author" title="<?php echo esc_attr($bk_author_name);?>" href="<?php echo esc_url(get_author_posts_url($authorID));?>"><?php echo esc_html($bk_author_name);?></a>                    </div>
                    <div class="author__text">
    					<span>
    						<?php 
                                $bk_allow_html = array(
                                    'a' => array(
                                        'href' => array(),
                                        'title' => array()
                                    ),
                                    'br' => array(),
                                    'em' => array(),
                                    'strong' => array(),
                                    'p' => array(),
                                );
                                echo wp_kses($bk_author_bio, $bk_allow_html);
                            ?>
                        </span>
    				</div>
                    <div class="author__social">
    					<ul class="social-list social-list--sm list-horizontal">
    					   <?php
                            if (($bk_author_email != NULL) || ($bk_author_www != NULL) || ($bk_author_go != NULL) || ($bk_author_tw != NULL) || ($bk_author_fb != NULL) ||($bk_author_yo != NULL)) {
                                if ($bk_author_email != NULL) { echo '<li><a href="mailto:'. esc_attr($bk_author_email) .'"><i class="mdicon mdicon-mail_outline"></i><span class="sr-only">e-mail</span></a></li>'; } 
                                if ($bk_author_www != NULL) { echo ' <li><a href="'. esc_url($bk_author_www) .'" target="_blank"><i class="mdicon mdicon-public"></i><span class="sr-only">Website</span></a></li>'; } 
                                if ($bk_author_tw != NULL) { echo ' <li><a href="'. esc_url($bk_author_tw).'" target="_blank" ><i class="mdicon mdicon-twitter"></i><span class="sr-only">Twitter</span></a></li>'; } 
                                if ($bk_author_go != NULL) { echo ' <li><a href="'. esc_url($bk_author_go) .'" rel="publisher" target="_blank"><i class="mdicon mdicon-google-plus"></i><span class="sr-only">Google+</span></a></li>'; }
                                if ($bk_author_fb != NULL) { echo ' <li><a href="'. esc_url($bk_author_fb) . '" target="_blank" ><i class="mdicon mdicon-facebook"></i><span class="sr-only">Facebook</span></a></li>'; }
                                if ($bk_author_yo != NULL) { echo ' <li><a href="http://www.youtube.com/user/'. esc_attr($bk_author_yo) . '" target="_blank" ><span class="sr-only">Youtube</span></a></li>'; }
                            }   
                           ?>	
                        </ul>
    				</div>
                </div>
            </div>
			
		<?php

		/* After widget (defined by themes). */
		echo $after_widget;
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['author_id'] = strip_tags( $new_instance['author_id'] );

		return $instance;
	}


	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'About Me', 'author_id' => 0);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:96%;" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'author_id' ); ?>">Author ID:</label>
			<input id="<?php echo $this->get_field_id( 'author_id' ); ?>" name="<?php echo $this->get_field_name( 'author_id' ); ?>" value="<?php echo $instance['author_id']; ?>" style="width:96%;" />
		</p>


	<?php
	}
}

?>