<?php 

class Glimmer_Login_Form extends WP_Widget {

    function __construct() {
        $params = array (
            'description' => esc_html__('Glimmer : Customized Login Form', 'glimmer'),
            'name' => esc_html__('Glimmer : Login Form', 'glimmer')
        );
        parent::__construct('Glimmer_Login_Form','',$params);
    }   

    function widget( $args, $instance ) {
        extract( $args );
        /* User-selected settings. */
        $title = apply_filters('widget_title', $instance['title'] );

        /* Before widget (defined by themes). */
        echo $before_widget;

        /* Title of widget (before and after defined by themes). */
        if ( $title ) {
            echo $before_title . $title . $after_title;
        }

        ?>
        <div class="glimmer-login-form">

        <?php 

            if ( ! is_user_logged_in() ) {

                wp_login_form( $args );
                echo'<p class="lost-pass"><a href="'. wp_lostpassword_url().'" title="Lost Password">Lost Your Password?</a></p>';
                wp_register( "", "", true );

            } else {
                ?>
                    <div class="glimmer-login-board">
                       <div class="left-thumb">
                           <?php echo get_avatar( get_the_author_meta('ID')); ?>
                       </div> <!-- / . left-thumb -->
                       <div class="right-details">
                           <p><?php esc_html_e( "Welcome:", "glimmer" );?> <strong><?php echo get_the_author_meta('display_name');?></strong></p>
                           <div class="dashboard-link">
                                <i class="fa fa-tachometer"></i><a href="<?php echo home_url() ; ?>/wp-admin"><?php esc_html_e( "Dashboard","glimmer" );?></a>
                           </div>
                           <div class="your-profile">
                                <i class="fa fa-user"></i><a href="<?php echo get_edit_user_link( get_the_author_meta('ID') ) ?>"><?php esc_html_e( "Profile", "glimmer" );?></a>
                           </div>
                           <div class="logout-link">
                                <i class="fa fa-power-off"></i><a href="<?php echo wp_logout_url( home_url() ); ?>"><?php esc_html_e( "Logout", "glimmer" );?></a>
                           </div>
                       </div> <!-- / . right-details -->
                   </div> <!-- / .glimmer-login-board -->
                <?php
            }

       echo "</div> <!-- / .glimmer-login-form -->";

        /* After widget (defined by themes). */
        echo $after_widget;
    }
    
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        /* Strip tags (if needed) and update the widget settings. */
        $instance['title'] = strip_tags( $new_instance['title'] );
        return $instance;
    }
    
     /** @see WP_Widget::form */
    function form( $instance ) {

            /* Set up some default widget settings. */
            $defaults = array(
                'title' => esc_html__('Login Form', 'glimmer'),
                );
            $instance = wp_parse_args( (array) $instance, $defaults ); ?>
        
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'glimmer') ?></label>
                <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php if( isset($instance['title']) ) echo esc_attr($instance['title']); ?>" />
            </p>
        
       <?php 
    }
} //end class