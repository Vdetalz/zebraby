<?php
class Glimmer_Popular_Posts extends WP_Widget {

	function __construct() {
		$params = array (
			'description' => esc_html__('Glimmer : Popular Posts', 'glimmer'),
			'name' => esc_html__('Glimmer : Popular Posts', 'glimmer')
		);
		parent::__construct('Glimmer_Popular_Posts','',$params);
	}

	public function form( $instance) {
		extract($instance);
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:','glimmer'); ?></label>
			<input
				class="widefat"
				type="text"
				id="<?php echo $this->get_field_id('title'); ?>"
				name="<?php echo $this->get_field_name('title'); ?>"
				value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('glimmer_popular_post_limit'); ?>"><?php esc_html_e('Number of posts to show:','glimmer'); ?></label>
			<input 
				id="<?php echo $this->get_field_id('glimmer_popular_post_limit'); ?>" 
				type="text" 
				name="<?php echo $this->get_field_name('glimmer_popular_post_limit'); ?>"
				value="<?php if( isset($glimmer_popular_post_limit) ) echo esc_attr($glimmer_popular_post_limit); ?>"
				size="3" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('glimmer_popular_post_last_x_days'); ?>"><?php esc_html_e('Show last x days popular post:','glimmer'); ?></label>
			<input 
				id="<?php echo $this->get_field_id('glimmer_popular_post_last_x_days'); ?>" 
				type="text" 
				name="<?php echo $this->get_field_name('glimmer_popular_post_last_x_days'); ?>"
				value="<?php if( isset($glimmer_popular_post_last_x_days) ) echo esc_attr($glimmer_popular_post_last_x_days); ?>"
				size="3" />
		</p>
		<?php
	} // end form function

	function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['glimmer_popular_post_limit'] = intval( $new_instance['glimmer_popular_post_limit'] );
        $instance['glimmer_popular_post_last_x_days'] = intval( $new_instance['glimmer_popular_post_last_x_days'] );
     
        return $instance;
    }

	public function widget($args, $instance) {
		extract($args);
		extract($instance);
		$title = apply_filters('widget_title',$title);
		$glimmer_popular_post_limit = apply_filters('widget_glimmer_popular_post_limit',$glimmer_popular_post_limit);
		$glimmer_popular_post_last_x_days = apply_filters('widget_glimmer_popular_post_last_x_days',$glimmer_popular_post_last_x_days);

		if ( $glimmer_popular_post_limit == 0 ) $glimmer_popular_post_limit = 5;
		if ( $glimmer_popular_post_last_x_days == 0 ) $glimmer_popular_post_last_x_days = 30;

		echo $before_widget;
			if ( !empty( $title ) ) {
				echo $before_title . $title . $after_title;
			}
			?>			
				<?php 
					$glimmer_popular_post = new WP_Query( 
						array( 'posts_per_page' => $glimmer_popular_post_limit,
						   'meta_key' => 'softhopper_post_views_count',
						   'ignore_sticky_posts' => true,
						   'orderby' => 'meta_value_num',
						   'order' => 'DESC',
						   'date_query' => array(
                                array(
                                    'after' => "$glimmer_popular_post_last_x_days day ago"
                                )
                            )
						) 
					);
				?>
				<div class="widget-feed">
                    <ul>
						<?php while ( $glimmer_popular_post->have_posts() ) : $glimmer_popular_post->the_post(); ?>
                    	<li class="feed-wrapper">
                            <div class="content">                                 
                                <div class="image-area">
                                    <?php get_template_part( 'template-parts/content', 'image-small' ); ?>
                                </div> <!-- /.image-area -->

                                <div class="item-text">
                                    <h5><a href="<?php the_permalink(); ?>"><?php echo glimmer_custom_post_excerpt( get_the_title(), 70, '&hellip;'); ?></a></h5>
                                    <span class="item-meta"><?php the_time( 'j M, Y' ); ?></span>,
                                    <span class="item-meta"><?php echo softhopper_get_post_views(get_the_ID()); ?></span>
                                </div> <!-- /.item-text -->
                            </div>  <!-- /.content --> 
                        </li>

		                <?php
							endwhile;
						?>
					</ul> 
                </div> <!-- /.widget-feed --> 
			<?php
		echo $after_widget;
	}
}