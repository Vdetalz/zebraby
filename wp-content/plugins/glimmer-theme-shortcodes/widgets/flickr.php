<?php
class Glimmer_Flickr_Gallery extends WP_Widget {

    function __construct() {
        $params = array (
            'description' => esc_html__('Glimmer : Flickr gallery widget', 'glimmer'),
            'name' => esc_html__('Glimmer : Flickr Gallery', 'glimmer')
        );
        parent::__construct('Glimmer_Flickr_Gallery','',$params);
    }

    public function form( $instance) {
        /* Set up some default widget settings. */
            $defaults = array(
                'title' => esc_html__('Flickr Gallery', 'glimmer'),
                'flickr_count' => "6",
                'flickr_display_type' => "latest",
                'flickr_image_size' => "s",
                'flickr_user_id' => "138254744@N08",
                'flickr_tag' => "",
                
                );
            $instance = wp_parse_args( (array) $instance, $defaults );
            extract($instance);
        ?>   
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:','glimmer'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('title'); ?>"
                name="<?php echo $this->get_field_name('title'); ?>"
                value="<?php if( isset($title) ) echo esc_attr($title); ?>"
            />
        </p>   
        <p>
            <label for="<?php echo $this->get_field_id('flickr_count'); ?>"><?php esc_html_e('How many picture show:','glimmer'); ?></label>
            <input
                class="widefat"
                type="number"
                id="<?php echo $this->get_field_id('flickr_count'); ?>"
                name="<?php echo $this->get_field_name('flickr_count'); ?>"
                value="<?php if( isset($flickr_count) ) echo esc_attr($flickr_count); ?>"
            />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('flickr_display_type'); ?>"><?php esc_html_e('Picture Display:','glimmer'); ?></label>
            <select id="<?php echo $this->get_field_id( 'flickr_display_type' ); ?>" name="<?php echo $this->get_field_name( 'flickr_display_type' ); ?>" class="widefat" >
            <?php
            	$flickr_display = array(
            		'latest' => 'Latest', 
            		'random' => 'Random', 
            	);

            	foreach ($flickr_display as $key => $value) {
            		( $flickr_display_type == $key ) ?  $selected = "selected='selected'"  : $selected = "" ;
            		echo "<option $selected value='$key'>$value</option>";
            	}
            ?>
            </select>
        </p> 
        <p>
            <label for="<?php echo $this->get_field_id('flickr_image_size'); ?>"><?php esc_html_e('Picture size:','glimmer'); ?></label>
            <select id="<?php echo $this->get_field_id( 'flickr_image_size' ); ?>" name="<?php echo $this->get_field_name( 'flickr_image_size' ); ?>" class="widefat" >
            <?php
            	$flickr_images_size = array(
            		's' => 'Small Square', 
            		't' => 'Thumbnail',
            		'm' => 'Medium',  
            	);

            	foreach ($flickr_images_size as $key => $value) {
            		( $instance['flickr_image_size'] == $key ) ?  $selected = "selected='selected'"  : $selected = "" ;
            		echo "<option $selected value='$key'>$value</option>";
            	}
            ?>
            </select>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('flickr_user_id'); ?>"><?php esc_html_e('Flickr ID:','glimmer'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('flickr_user_id'); ?>"
                name="<?php echo $this->get_field_name('flickr_user_id'); ?>"
                value="<?php if( isset($flickr_user_id) ) echo esc_attr($flickr_user_id); ?>"
            />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('flickr_tag'); ?>"><?php esc_html_e('Tag ( Optional ):','glimmer'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('flickr_tag'); ?>"
                name="<?php echo $this->get_field_name('flickr_tag'); ?>"
                value="<?php if( isset($flickr_tag) ) echo esc_attr($flickr_tag); ?>"
            />
        </p>
        
        <?php
    } // end form function

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        foreach ($new_instance as $key => $value) {           
            $instance[$key] = strip_tags( $new_instance[$key] );
        } // end for each
        return $instance;
    }

    public function widget($args, $instance) {
        extract($args);
        extract($instance);
        $title = apply_filters( 'widget_title', $title );       
   
        echo $before_widget;
            if ( !empty( $title ) ) {
                echo $before_title . $title . $after_title;
            }
            ?>
            <div id="glimmer_flickr_badge_wrapper">
                <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=<?php if( isset($flickr_count) ) echo esc_attr($flickr_count); ?>&amp;display=<?php if( isset($flickr_display_type) ) echo esc_attr($flickr_display_type); ?>&amp;size=<?php if( isset($flickr_image_size) ) echo esc_attr($flickr_image_size); ?>&amp;layout=x&amp;source=user&amp;user=<?php if( isset($flickr_user_id) ) echo esc_attr($flickr_user_id); ?>&amp;tag=<?php if( isset($flickr_tag) ) echo esc_attr($flickr_tag); ?>"></script>
            </div>                
            <?php
        echo $after_widget;
    }
}