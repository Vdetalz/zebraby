<?php 


class Glimmer_Comments extends WP_Widget {

	function __construct() {
	    $params = array (
	        'name' => esc_html__('Glimmer : Recent Comments', 'glimmer'),
	        'description' => esc_html__('Glimmer : Custom Recent comments', 'glimmer')
	    );
	    parent::__construct('Glimmer_Comments','',$params);
	}
		
	function widget( $args, $instance ) {
		extract( $args );
		/* User-selected settings. */
		$title = apply_filters('widget_title', $instance['title'] );

		//$comment_count = "";
		$comment_count = $instance['comment_count'];
		$comment_date = $instance['comment_date'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Title of widget (before and after defined by themes). */
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		?>
		<div class="glimmer-recent-comments" >
			<div class="widget-feed">
			    <ul> 

		        <?php 
		        	$no_comments = 4; 
		        	$comment_len = 55; 
		        	$avatar_size = 80;
		        	$comments_query = new WP_Comment_Query();
					$comments = $comments_query->query( array( 'number' => $no_comments ) );

					$comm = '';
					if ( $comments ) : foreach ( $comments as $comment ) :
						($comment_date == "on") ? $comm_time_date = '<span class="item-meta">'.get_comment_date( "j-F-Y", $comment->comment_ID ).'&nbsp;at&nbsp;'.get_comment_date( "g:i a", $comment->comment_ID ).'</span>' : $comm_time_date = "" ;
						$comm .= '<li class="feed-wrapper">
								    <div class="content">                                 
								        <div class="image-area">
								            <a rel="bookmark" href="' . get_permalink( $comment->comment_post_ID ) . '#comment-' . $comment->comment_ID . '">
								                <figure class="fit-img">
								                    '.get_avatar( $comment->comment_author_email, $avatar_size ).'
								                </figure>
								            </a>
								        </div> <!-- /.comments-image -->
								        <div class="item-text">
								            <h5><a rel="bookmark" href="' . get_permalink( $comment->comment_post_ID ) . '#comment-' . $comment->comment_ID . '">' . strip_tags( glimmer_custom_post_excerpt( apply_filters( 'get_comment_text', $comment->comment_content ), $comment_len, '&hellip;') ) . '</a></h5>'.$comm_time_date.'
								        </div> <!-- /.comments-item-text -->
								    </div>  <!-- /.comments-item --> 
								</li>';
					endforeach; else :
						$comm .= 'No comments.';
					endif;
					echo $comm;	
		        ?>
			    </ul> <!-- /.comments-newsfeed -->
			</div> <!-- /.comments-widget --> 
		</div><!-- / .glimmer-recent-comments -->
		<?php 

		/* After widget (defined by themes). */
		echo $after_widget;
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['comment_count'] = intval( $new_instance['comment_count'] );
		$instance['comment_date'] = $new_instance['comment_date'];
		return $instance;
	}
	
	function form( $instance ) {

			/* Set up some default widget settings. */
			$defaults = array(
				'title' => __('Recent Comments','glimmer'),
				'comment_count' => 5,
				'comment_date' => "on"
	 			);
			$instance = wp_parse_args( (array) $instance, $defaults ); ?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e('Title:', 'glimmer') ?></label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
			</p>
	    	<p>
				<label for="<?php echo $this->get_field_id( 'comment_count' ); ?>"><?php esc_html_e('Comment to show :', 'glimmer') ?></label>
				<input type="number" id="<?php echo $this->get_field_id( 'comment_count' ); ?>" name="<?php echo $this->get_field_name( 'comment_count' ); ?>" value="<?php echo $instance['comment_count']; ?>"  class="widefat" />
			</p>
	    	<p>
				<input class="checkbox" type="checkbox" <?php checked( $instance['comment_date'], 'on' ); ?> id="<?php echo $this->get_field_id( 'comment_date' ); ?>" name="<?php echo $this->get_field_name( 'comment_date' ); ?>" />
				<label for="<?php echo $this->get_field_id( 'comment_date' ); ?>"><?php esc_html_e('Show date', 'glimmer'); ?></label>
			</p>
	   <?php 
	}
} //end class