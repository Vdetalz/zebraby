<?php 
/*
Plugin Name: Glimmer Core
Plugin URI: http://www.softhopper.net
Description: This plugin will include Glimmer theme shortcode
Author: SoftHopper
Author URI: http://softhopper.net
Version: 1.0
*/

/* don't call the file directly */
if ( !defined( 'ABSPATH' ) ) exit;

// include shortcode file to generate shortcode from code editor
include( plugin_dir_path( __FILE__ ) . 'shortcodes/shortcodes.php' );

// Include essential functions
include( plugin_dir_path( __FILE__ ) . 'inc/functions.php' );

// Include essential functions
include( plugin_dir_path( __FILE__ ) . 'widgets/widgets.php' );

// Featured Posts
include( plugin_dir_path( __FILE__ ) . 'inc/featured-post.php' );

// Instagram Posts
include( plugin_dir_path( __FILE__ ) . 'inc/instagram-block.php' );