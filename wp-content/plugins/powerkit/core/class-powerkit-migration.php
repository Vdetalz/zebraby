<?php
/**
 * The admin-specific functionality of the module.
 *
 * @package    Powerkit
 * @subpackage Core
 */

/**
 * The admin-specific functionality of the module.
 */
class Powerkit_Migration {

	/**
	 * The data to upgrade.
	 *
	 * @var array $data The data to upgrade.
	 */
	public $data = array();

	/**
	 * The title to migration.
	 *
	 * @var array $title The data to migration.
	 */
	public $title;

	/**
	 * The description to migration.
	 *
	 * @var array $description The data to migration.
	 */
	public $description;

	/**
	 * The steps to migration.
	 *
	 * @var array $steps The data to migration.
	 */
	public $steps = array();

	/**
	 * Construct
	 */
	public function __construct() {
		$this->title       = esc_html__( 'Update is Required', 'powerkit' );
		$this->description = esc_html__( 'Please click the button below to start migration to Powerkit.', 'powerkit' );

		add_action( 'init', array( $this, 'init' ) );
		add_action( 'powerkit_plugin_upgrade', array( $this, 'upgrade' ), 10, 2 );
		add_action( 'wp_ajax_powerkit_migration', array( $this, 'init_powerkit_migration' ) );
		add_action( 'admin_notices', array( $this, 'show_migration' ) );
		add_action( 'admin_notices', array( $this, 'show_notices' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
	}

	/**
	 * Upgrade
	 *
	 * @param string $current Current version.
	 * @param string $new     New version.
	 */
	public function upgrade( $current, $new ) {
		// Update migration data.
		update_option( 'powerkit_migration_data', array(
			'current' => $current,
			'new'     => $new,
		), true );
	}

	/**
	 * Initialization
	 */
	public function init() {
		$migration_data = get_option( 'powerkit_migration_data' );
		if ( $migration_data ) {
			$this->data['current'] = $migration_data['current'];
			$this->data['new']     = $migration_data['new'];

			do_action( 'powerkit_plugin_migration', $this );
		}
	}

	/**
	 * Add new step.
	 *
	 * @param array $settings The settings.
	 */
	public function add_step( $settings ) {
		$action = isset( $settings['action'] ) ? $settings['action'] : null;

		// If action is posts.
		if ( 'posts' === $action ) {
			$args = isset( $settings['args'] ) ? $settings['args'] : array(
				'posts_per_page' => -1,
				'post_type'      => array( 'post', 'page' ),
				'post_status'    => array( 'publish', 'draft', 'future' ),
				'fields'         => 'ids',
			);

			// WP Query.
			$query = new WP_Query( $args );

			$max = $query->post_count;

			// Set additional settings.
			$settings['name'] .= sprintf( ' (<span class="counter">0</span>/%s) ', $max );
			$settings['args']  = $args;
			$settings['max']   = $max;
			$settings['min']   = 0;
		}

		// Generate key.
		$key = uniqid();

		// Add step.
		$this->steps[ $key ] = $settings;
	}

	/**
	 * Init migration.
	 */
	public function init_powerkit_migration() {
		check_ajax_referer( 'nonce', 'nonce' );

		$step   = false;
		$offset = 0;

		if ( isset( $_POST['step'] ) ) { // Input var ok.
			$step = sanitize_text_field( wp_unslash( $_POST['step'] ) ); // Input var ok.
		}

		if ( isset( $_POST['offset'] ) ) { // Input var ok.
			$offset = (int) $_POST['offset']; // Input var ok.
		}

		// Get steps list.
		$steps = get_option( 'powerkit_migration_steps', array() );

		// Check exists step.
		if ( ! isset( $steps[ $step ] ) ) {
			return;
		}

		// Set data of step.
		$data = $steps[ $step ];

		/*
		 * Deactivate plugins
		 * ---------------------------------
		 */
		if ( 'deactivate_plugins' === $data['action'] ) {
			foreach ( $data['plugins'] as $plugin ) {
				if ( is_plugin_active( $plugin ) ) {
					deactivate_plugins( $plugin );
				}
			}
		}

		/*
		 * Last iteration
		 * ---------------------------------
		 */
		if ( 'done' === $data['action'] ) {
			update_option( 'powerkit_migration_notice', true, true );
			delete_option( 'powerkit_migration_data' );
			delete_option( 'powerkit_migration_steps' );
		}

		wp_die();
	}

	/**
	 * Notices.
	 *
	 * @since 1.0.0
	 */
	public function show_notices() {
		if ( ! get_option( 'powerkit_migration_notice' ) ) {
			return;
		}
		update_option( 'powerkit_migration_notice', false, true );
		?>
			<div id="message" class="notice notice-success is-dismissible">
				<p><strong><?php esc_html_e( 'Migration successful.', 'powerkit' ); ?></strong></p>
			</div>
		<?php
	}

	/**
	 * Build admin page
	 *
	 * @since 1.0.0
	 */
	public function show_migration() {
		if ( ! $this->steps ) {
			return;
		}

		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( esc_html__( 'You do not have sufficient rights to view this page.', 'powerkit' ) );
		}
		?>
			<div class="notice notice-warning powerkit-migration">
				<?php if ( $this->title ) { ?>
					<p><h2><?php echo wp_kses( $this->title, 'post' ); ?></h2></p>
				<?php } ?>

				<?php if ( $this->description ) { ?>
					<p><?php echo wp_kses( $this->description, 'post' ); ?></p>
				<?php } ?>

				<p>
					<button class="button button-primary">
						<?php esc_html_e( 'Start Migration', 'powerkit' ); ?>
					</button>
				</p>

				<div class="migrate-step-list">
					<table class="form-table">
						<?php
						if ( $this->steps ) {
							// Get all titles.
							$titles = array_filter( $this->steps, function( $item ) {
								return isset( $item['title'] );
							} );

							// Loop steps.
							foreach ( $this->steps as $key => $step ) {
								if ( isset( $step['title'] ) ) {
									?>
									<tr class="<?php echo count( $titles ) > 1 ? 'title' : 'hide'; ?>" style="display: none;">
										<th colspan="2">
											<h2 style="margin: 0;"><?php echo wp_kses( $step['title'], 'post' ); ?></h2>
										</th>
									</tr>
									<?php
								} else {
								?>
									<tr class="row" style="display: none;">
										<th style="padding-bottom: 10px;">
											<div class="step step-hook" data-step="<?php echo esc_attr( $key ); ?>"
												data-min="<?php echo esc_attr( isset( $step['min'] ) ? (int) $step['min'] : 'false' ); ?>"
												data-max="<?php echo esc_attr( isset( $step['max'] ) ? (int) $step['max'] : 'false' ); ?>">

												— <?php echo wp_kses( $step['name'], 'post' ); ?>
											</div>
										</th>
										<td class="status">
											<span style="float: none;" class="spinner is-active"></span>
										</td>
									</tr>
								<?php
								}
							}
						}
						?>
					</table>
				</div>
			</div>
		<?php
	}

	/**
	 * Register the stylesheets and JavaScript for the admin area.
	 *
	 * @param string $page Current page.
	 */
	public function admin_enqueue_scripts( $page ) {
		if ( ! $this->steps ) {
			return;
		}

		// Localise script.
		wp_localize_script( 'jquery-core', 'powerkit_migration', array(
			'url'   => admin_url( 'admin-ajax.php' ),
			'nonce' => wp_create_nonce( 'nonce' ),
		) );

		// Filters steps.
		$steps = array_filter( $this->steps, function( $el ) {
			return ! isset( $el['title'] );
		} );

		// Set temporary variable.
		update_option( 'powerkit_migration_steps', $steps, true );

		ob_start();
		?>
		<script>
		( function( $ ) {
			function powerkitMigrationStep( step ) {
				var step = $( '.migrate-step-list .step-hook' ).first();

				if ( $( step ).length ) {
					var min = false;

					$( step ).closest( '.row' ).prev( '.title' ).show();

					$( step ).closest( '.row' ).show();

					if ( false !== $( step ).data( 'min' ) ) {
						var min  = parseInt( $( step ).data( 'min' ) );
						var max  = parseInt( $( step ).data( 'max' ) );

						if ( min < max ) {
							$( step ).data( 'min', min + 1 );
						}
					}

					var data = {
						action: 'powerkit_migration',
						offset: min,
						nonce: powerkit_migration.nonce,
						step: $( step ).data( 'step' ),
					};

					jQuery.post( powerkit_migration.url, data, function( response ) {

						if ( min >= max ) {
							min = false;
						}

						if ( false === min ) {
							$( step ).removeClass( 'step-hook' );

							$( step ).closest( '.row' ).find( '.status' ).html( '<code>success</code>' );
						} else {
							$( step ).find( '.counter' ).html( min + 1  );
						}

						powerkitMigrationStep();
					});
				} else {
					setTimeout( function() {
						location.reload();
					}, 25 );
				}
			}

			// Start Migration.
			$( document ).on( 'click', '.powerkit-migration .button-primary', function() {
				$( this ).parent().remove();

				powerkitMigrationStep();

				return false;
			});

		} )( jQuery );
		</script>
		<?php
		wp_add_inline_script( 'jquery-core', str_replace( array( '<script>', '</script>' ), '', ob_get_clean() ) );
	}
}

new Powerkit_Migration();
