<?php
/**
 * Integration of the Gutenberg into the plugin.
 *
 * @package    Powerkit
 * @subpackage Core
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Integration Gutenberg class.
 */
class Powerkit_Gutenberg {

	/**
	 * __construct
	 *
	 * This function will initialize the initialize
	 */
	public function __construct() {

		// Define the functionality of the gutenberg.
		$this->initialize();
	}

	/**
	 * Initialize
	 *
	 * This function will initialize the gutenberg.
	 */
	public function initialize() {
		add_action( 'block_categories', array( $this, 'block_categories' ), 10, 2 );
	}

	/**
	 * Filter the default array of block categories.
	 *
	 * @param array  $categories Array of block categories.
	 * @param object $post       Post being loaded.
	 */
	public function block_categories( $categories, $post ) {
		return array_merge(
			$categories,
			array(
				array(
					'slug'  => 'powerkit',
					'title' => esc_html__( 'Powerkit', 'powerkit' ),
				),
			)
		);
	}
}

new Powerkit_Gutenberg();
