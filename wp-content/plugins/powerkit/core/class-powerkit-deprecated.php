<?php
/**
 * Deprecated features and migration functions
 *
 * @package    Powerkit
 * @subpackage Core
 */

/**
 *
 * Check for compatible plugins.
 */
function powerkit_check_compatible_plugins() {
	$plugins = array();
	if ( is_plugin_active( 'gridable/gridable.php' ) ) {
		$plugins[] = esc_html__( 'Gridable', 'powerkit' );
	}

	if ( $plugins ) {
		// Translators: name of plugins.
		$msg = esc_html__( 'Required to deactivate %s for compatibility with Powerkit.', 'powerkit' );
		?>
		<div id="message" class="notice notice-warning is-dismissible">
			<p><?php echo sprintf( esc_html( $msg ), esc_html( join( ', ', $plugins ) ) ); ?></p>
		</div>
		<?php
	}
}
add_action( 'admin_notices', 'powerkit_check_compatible_plugins' );

/**
 * Migration to version 1.6.0
 *
 * @param object $object Object migration.
 */
add_action( 'powerkit_plugin_migration', function ( $object ) {

	if ( isset( $object->data['current'] ) && version_compare( $object->data['current'], '1.6.0', '<' ) ) {
		// Title of Migration.
		$object->add_step( array(
			'title' => esc_html__( 'Migration to version 1.6.0', 'powerkit' ),
		) );

		// Migration Options.
		$galleries = get_option( 'powerkit_lightbox_gallery_selectors' );
		if ( $galleries && false === strpos( $galleries, '.wp-block-gallery' ) ) {
			$value = sprintf( '.wp-block-gallery, %s', $galleries );

			update_option( 'powerkit_lightbox_gallery_selectors', $value, true );
		}

		// Deactivate Plugins.
		$object->add_step( array(
			'name'    => esc_html__( 'Deactivate Plugins', 'powerkit' ),
			'action'  => 'deactivate_plugins',
			'plugins' => array(
				'gridable/gridable.php',
			),
		) );

		// Done.
		$object->add_step( array(
			'name'   => esc_html__( 'Done', 'powerkit' ),
			'action' => 'done',
		) );
	}
} );
