var el = wp.element.createElement,
	registerBlockType = wp.blocks.registerBlockType,
	InspectorControls = wp.editor.InspectorControls,
	ServerSideRender  = wp.components.ServerSideRender,
	TextControl       = wp.components.TextControl,
	RangeControl      = wp.components.RangeControl,
	ToggleControl     = wp.components.ToggleControl;
	SelectControl     = wp.components.SelectControl;

registerBlockType( 'powerkit/instagram', {

	title: wp.i18n.__( 'Instagram' ),

	description: wp.i18n.__( 'The block allows you to display images from your instagram account.' ),

	icon: 'images-alt',

	category: 'powerkit',

	edit: function( props ) {

		var userID   = props.attributes.userID,
			header   = props.attributes.header,
			button   = props.attributes.button,
			number   = props.attributes.number,
			columns  = props.attributes.columns,
			size     = props.attributes.size,
			target   = props.attributes.target,
			template = props.attributes.template;

		var templates = pk_instagram_localize.templates;

		// Set template options.
		var templateOptions = [];

		for ( key in templates ) {
			templateOptions.push( { label: templates[key], value: key } );
		}

		return [
			// Inspector Controls.
			el( InspectorControls, { key: 'inspector' },
				el( wp.components.PanelBody, {
						title: wp.i18n.__( 'Settings' ),
						className: 'block-settings',
						initialOpen: true
					},
					el( TextControl, {
						label: wp.i18n.__( 'User ID' ),
						value: userID,
						onChange: function( val ) {
							props.setAttributes( { userID: val } )
						}
					} ),
					el( ToggleControl, {
						label: wp.i18n.__( 'Display header' ),
						checked: header,
						onChange: function( val ) {
							props.setAttributes( { header: val } )
						}
					} ),
					el( ToggleControl, {
						label: wp.i18n.__( 'Display follow button' ),
						checked: button,
						onChange: function( val ) {
							props.setAttributes( { button: val } )
						}
					} ),
					el( RangeControl, {
						label: wp.i18n.__( 'Number of images (maximum 12)' ),
						value: number,
						max: 12,
						onChange: function( val ) {
							props.setAttributes( { number: val } )
						}
					} ),
					el( SelectControl, {
						label: wp.i18n.__( 'Number of columns' ),
						value: columns,
						options: [
							{ label: '1', value: '1' },
							{ label: '2', value: '2' },
							{ label: '3', value: '3' },
							{ label: '4', value: '4' },
							{ label: '5', value: '5' },
							{ label: '6', value: '6' },
							{ label: '7', value: '7' },
						],
						onChange: function( val ) {
							props.setAttributes( { columns: val } )
						}
					} ),
					el( SelectControl, {
						label: wp.i18n.__( 'Photo size' ),
						value: size,
						options: [
							{ label: wp.i18n.__( 'Thumbnail' ), value: 'thumbnail' },
							{ label: wp.i18n.__( 'Small' ), value: 'small' },
							{ label: wp.i18n.__( 'Large' ), value: 'large' },
						],
						onChange: function( val ) {
							props.setAttributes( { size: val } )
						}
					} ),
					el( SelectControl, {
						label: wp.i18n.__( 'Open links in' ),
						value: target,
						options: [
							{ label: wp.i18n.__( 'New window (_blank)' ), value: '_blank' },
							{ label: wp.i18n.__( 'Current window (_self)' ), value: '_self' },
						],
						onChange: function( val ) {
							props.setAttributes( { target: val } )
						}
					} ),
					templateOptions.length > 1 ? el( SelectControl, {
						label: wp.i18n.__( 'Template' ),
						value: template,
						options: templateOptions,
						onChange: function( val ) {
							props.setAttributes( { template: val } )
						}
					} ) : [],
				)
			),

			// Rendering.
			el( ServerSideRender, {
				block: 'powerkit/instagram',
				attributes: props.attributes
			} )
		]
	},

	save: function() {
		// Rendering in PHP
		return null;
	},
} );
