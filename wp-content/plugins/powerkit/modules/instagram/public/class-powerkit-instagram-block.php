<?php
/**
 * The Gutenberg Block.
 *
 * @link       https://codesupply.co
 * @since      1.0.0
 *
 * @package    Powerkit
 * @subpackage Modules/public
 */

/**
 * The initialize block.
 */
class Powerkit_Instagram_Block {

	/**
	 * Initialize
	 */
	public function __construct() {
		if ( ! function_exists( 'register_block_type' ) ) {
			return;
		}

		add_action( 'init', array( $this, 'block' ) );
	}

	/**
	 * Render Block
	 *
	 * @param array $attributes The attributes of block.
	 */
	public function render( $attributes ) {

		$params = array(
			'user_id'    => $attributes['userID'],
			'header'     => $attributes['header'],
			'button'     => $attributes['button'],
			'number'     => $attributes['number'],
			'columns'    => $attributes['columns'],
			'size'       => $attributes['size'],
			'target'     => $attributes['target'],
			'template'   => $attributes['template'],
			'cache_time' => apply_filters( 'powerkit_instagram_cache_time', 60 ),
		);

		ob_start();

		// Instagram output.
		powerkit_instagram_get_recent( $params );

		return ob_get_clean();
	}

	/**
	 * Enqueue the block's assets for the editor.
	 */
	public function block() {
		// Scripts.
		wp_register_script(
			'powerkit-instagram-block-script',
			plugins_url( 'block/block.js', __FILE__ ),
			array( 'wp-blocks', 'wp-components', 'wp-element', 'wp-i18n', 'wp-editor' ),
			filemtime( plugin_dir_path( __FILE__ ) . 'block/block.js' ),
			true
		);

		// Add additional data to scripts.
		wp_localize_script( 'powerkit-instagram-block-script', 'pk_instagram_localize', array(
			'templates' => powerkit_instagram_get_templates_options(),
		) );

		// Styles.
		wp_register_style(
			'powerkit-instagram-block-editor-style',
			plugins_url( 'css/public-powerkit-instagram.css', __FILE__ ),
			array( 'wp-edit-blocks' ),
			filemtime( plugin_dir_path( __FILE__ ) . 'css/public-powerkit-instagram.css' )
		);

		wp_style_add_data( 'powerkit-instagram-block-editor-style', 'rtl', 'replace' );

		// Here we actually register the block with WP, again using our namespacing.
		// We also specify the editor script to be used in the Gutenberg interface.
		register_block_type( 'powerkit/instagram', array(
			'editor_script'   => 'powerkit-instagram-block-script',
			'editor_style'    => 'powerkit-instagram-block-editor-style',
			'attributes'      => array(
				'userID'   => array(
					'type'    => 'string',
					'default' => null,
				),
				'header'   => array(
					'type'    => 'boolean',
					'default' => true,
				),
				'button'   => array(
					'type'    => 'boolean',
					'default' => true,
				),
				'number'   => array(
					'type'    => 'number',
					'default' => 4,
				),
				'columns'  => array(
					'type'    => 'select',
					'default' => '1',
				),
				'size'     => array(
					'type'    => 'select',
					'default' => 'thumbnail',
				),
				'target'   => array(
					'type'    => 'select',
					'default' => '_blank',
				),
				'template' => array(
					'type'    => 'select',
					'default' => 'default',
				),
			),
			'render_callback' => array( $this, 'render' ),
		) );
	}
}

new Powerkit_Instagram_Block();
