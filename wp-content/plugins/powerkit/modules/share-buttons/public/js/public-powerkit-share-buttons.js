/**
 * Share buttons.
 */

( function( $ ) {

	$( document ).ready( function() {

		// Blockquote
		let blockquoteLength = $( '.pk-share-buttons-blockquote' ).length;

		if ( blockquoteLength ) {
			$( '.entry-content' ).find( 'blockquote' ).each( function( index, item ) {

				var text;

				text = $( this ).find( 'p' ).text();

				if ( ! text ) {
					text = $( this ).text();
				}

				let container = $( '.pk-share-buttons-blockquote' ).not( '.pk-share-buttons-blockquote-clone' ).clone().appendTo( item );

				container.addClass( 'pk-share-buttons-blockquote-clone' );

				container.find( '.pk-share-buttons-link' ).each( function( index, item ) {

					let url = $( this ).attr( 'href' ).replace( '%text%', encodeURIComponent( text ) );

					$( this ).attr( 'href', url );
				} );
			} );
		}

		// Highlight and Share
		let highlightLength = $( '.pk-share-buttons-highlight-text' ).length;

		if ( highlightLength ) {

			/*
			 * Events
			 */

			$( 'body' ).on( 'mouseup', function( e ) {
				if ( $( e.target ).closest( '.pk-share-buttons-wrap' ).length ) {
					return;
				}

				highlightRemove();
			} );

			$( 'body' ).on( 'mouseup', '.entry-content', function( e ) {
				e.stopPropagation();

				highlightRemove();

				let selection = window.getSelection();
				let text      = selection.toString();

				// Current title.
				this.title = '';

				// Check exists text.
				if ( '' == text ) {
					return;
				}

				highlightDisplay( text, e );
			} );

			/*
			 * Remove highlight container
			 */
			var highlightRemove = function() {
				$( '.pk-share-buttons-highlight-clone' ).remove();
			};

			/*
			 * Show highlight container
			 */
			var highlightDisplay = function( text, e ) {
				highlightRemove();

				let container = $( '.pk-share-buttons-highlight-text' ).not( '.pk-share-buttons-highlight-clone' ).clone().appendTo( 'body' );

				let wrapper_x = e.pageX + 10;
				let wrapper_y = e.pageY + 10;

				container.addClass( 'pk-share-buttons-highlight-clone' );

				container.css( { left: wrapper_x, top: wrapper_y } );

				container.find( '.pk-share-buttons-link' ).each( function( index, item ) {

					let url = $( this ).attr( 'href' ).replace( '%text%', encodeURIComponent( text ) );

					$( this ).attr( 'href', url );
				} );
			}
		}
	} );

} )( jQuery );